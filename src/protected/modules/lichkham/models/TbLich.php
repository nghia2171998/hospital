<?php

/**
 * TbLich là lớp chính của bảng "tb_lich".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 04/12/2021 16:01:01
 * @version 1.0
 *
 */
class TbLich extends TbLichBase {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Danh sách các cấu hình của model
     * @return array
     */
    public static function configArray() {
        $cnf = parent::configArray();
        return $cnf;
    }

    /**
     * Hàm gọi cấu hình theo model
     * @param string $item Tên của cấu hình
     * @return mix Giá trị của cấu hình tùy theo từng kiểu cấu hình mà có kiểu khác nhau
     */
    public static function getConf($item) {
        $option = Setting::getItem($item, __CLASS__);
        return $option ? $option : self::getDefaultConfig($item);
    }

    public static function SearchAdminKH() {
        $timkiem = new CDbCriteria();
        $timkiem->addSearchCondition('ten_kh', $_REQUEST['tukhoa1']);
        $bs = TbLich::model()->findAll($timkiem);
        return $bs;
    }

    public static function SearchAsDoctor() {
        $search = new CDbCriteria();
        if($_REQUEST['order']!=0){
            $search->addSearchCondition('bac_si', $_REQUEST['order']);
        }
        if($_REQUEST['orderfr']==$_REQUEST['orderto']){
            $search->compare('ngay_dat', $_REQUEST['orderfr']);
        }else{
            $search->addBetweenCondition('ngay_dat', $_REQUEST['orderfr'], $_REQUEST['orderto'], 'AND');
        }
        $bs = TbLich::model()->findAll($search);
        return $bs;
    }

}
