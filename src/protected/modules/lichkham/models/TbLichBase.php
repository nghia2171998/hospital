<?php

/**
 * Đây là lớp model của bảng "tb_lich".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 18/03/2022 08:49:18
 * @version 1.0
 *
 * Dưới đây là các cột của bảng 'tb_lich':
 * @property integer $id
 * @property string $ten_kh
 * @property string $sdt
 * @property string $dia_chi
 * @property string $tuoi
 * @property integer $chuyenkhoa
 * @property integer $weekday
 * @property string $time
 * @property integer $hour
 * @property integer $minute
 * @property string $time_slot
 * @property integer $deadtime
 * @property string $lydo
 * @property string $ngay_dat
 * @property string $ngay_kham
 * @property integer $bac_si
 * @property integer $trang_thai
 * @property integer $in_trash
 * @property integer $created_at
 * @property integer $update_at
 */
class TbLichBase extends MyModel
{

    public $modelName = 'TbLich';

    /**
     * @return string trả về tên của bảng
     */
    public function tableName()
    {
        return 'tb_lich';
    }

    /**
     * @return array Các quy định về thuộc tính của lớp.
     */
    public function rules()
    {
        // LƯU Ý: bạn chỉ nên định nghĩa các quy định cho các trường
        // mà người dùng sẽ thể nhập liệu    
        return array(
            array('ten_kh, sdt, dia_chi, tuoi, chuyenkhoa, weekday, time, hour, minute, time_slot, deadtime, lydo, ngay_dat, ngay_kham, bac_si, trang_thai, in_trash, created_at', 'required'),
            array('chuyenkhoa, weekday, hour, minute, deadtime, bac_si, trang_thai, in_trash, created_at, update_at', 'numerical', 'integerOnly'=>true),
            array('ten_kh', 'length', 'max' => 100),
            array('dia_chi', 'length', 'max' => 500),
            array('time_slot', 'length', 'max' => 30),
            // Những thuộc tính dưới đây để phục vụ cho hàm search().            
            // @todo Hãy xóa đi những trường mà bạn không muốn cho người dùng tìm kiếm
            array('id, ten_kh, sdt, dia_chi, tuoi, chuyenkhoa, weekday, time, hour, minute, time_slot, deadtime, lydo, ngay_dat, ngay_kham, bac_si, trang_thai, in_trash, created_at, update_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array mối quan hệ với các bảng khác.
     */
    public function relations()
    {
        // LƯU Ý: Bạn có thể cần phải điều chỉnh tên các quan hệ 
        // và các lớp liên quan được tự động sinh ra bên dưới đây        
        $rtn = parent::relations();
        return $rtn;
    }

    /**
     * @return array Tùy chỉnh tên các thuộc tính (name=>label)
     */
    public function attributeLabels()
    {
        $rtn = parent::attributeLabels();
        $rtn['id'] = 'ID';
        $rtn['ten_kh'] = 'Ten Kh';
        $rtn['sdt'] = 'Sdt';
        $rtn['dia_chi'] = 'Dia Chi';
        $rtn['tuoi'] = 'Tuoi';
        $rtn['chuyenkhoa'] = 'Chuyenkhoa';
        $rtn['weekday'] = 'Weekday';
        $rtn['time'] = 'Time';
        $rtn['hour'] = 'Hour';
        $rtn['minute'] = 'Minute';
        $rtn['time_slot'] = 'Time Slot';
        $rtn['deadtime'] = 'Deadtime';
        $rtn['lydo'] = 'Lydo';
        $rtn['ngay_dat'] = 'Ngay Dat';
        $rtn['ngay_kham'] = 'Ngay Kham';
        $rtn['bac_si'] = 'Bac Si';
        $rtn['trang_thai'] = 'Trang Thai';
        $rtn['in_trash'] = 'In Trash';
        $rtn['created_at'] = 'Created At';
        $rtn['update_at'] = 'Update At';
        return $rtn;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TbLich the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}