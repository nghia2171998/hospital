<div id='lich' style='width:100%;hieght:auto;display:block'>
    <div class='title'>
        <nav class="navbar navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand"></a>
                <p>Lịch khám</p>
                <div class="d-flex">
                    <input name='tukhoa' class="form-control me-2" v-model='tukhoa' v-on:keyup.enter="fetchAllData()" type="search" placeholder="Bệnh nhân"
                           aria-label="Search">
                </div>
            </div>
        </nav>
        <br />

        <div id='order' class="form">
            <label>Bác Sĩ: </label>
            <select class="js-example-basic-single" v-model='select' change='fetchAllData()' id='select' >
                <option value="" selected>chọn bác sĩ</option>
                <?php
                $bs = TbBacsi::model()->findAll();
                foreach ($bs as $bs) {
                    if ($bs['trang_thai'] == 1 && $bs['in_trash'] == 0) {
                        ?>
                        <option value="<?= $bs['id'] ?>"><?= $bs['ten'] ?></option>
                        <?php
                    }
                }
                ?>
            </select>
            <br/>
            <label>Chuyên khoa: </label>
            <select class="js-example-basic-single" v-model='select1' id="select1">
                <option value="" selected>chọn chuyên khoa</option>
                <?php
                $ck = TbChuyenkhoa::model()->findAll();
                foreach ($ck as $ck) {
                    if ($ck['trang_thai'] == 1 && $ck['in_trash'] == 0) {
                        ?>
                        <option value="<?= $ck['id'] ?>"><?= $ck['ten_khoa'] ?></option>
                        <?php
                    }
                }
                ?>
            </select>
<!--            <br />
            <label>Từ Ngày:</label>
            <input type="date" id="input" v-model="orderfr" @change="fetchAllData()">
            <br />
            <label>Đến Ngày: </label>
            <input type="date" id="input" v-model="orderto" @change="fetchAllData()">-->
            <br />
            <a href="<?php echo Yii::app()->baseUrl ?>/lichkham/admin">Tất cả lịch</a>
        </div>
        <div v-if='myModel'>
            <div class="modal-mask">
                <div class="modal-wrappe">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Chi tiết</h5>
                                <button type="button" class="btn-close" @click="myModel=false"></button>
                            </div>
                            <div class="modal-body row g-3">
                                <div class="col-md-6">
                                    <label for="inputCity" class="form-label">Tên khách hàng</label>
                                    <input type="text" class="form-control" name='ten' id="ten" v-model='ten' required
                                           disabled />
                                </div>
                                <div class="col-md-4">
                                    <label for="inputState" class="form-label">Số điện thoại</label>
                                    <input type="text" class="form-control" name='sdt' id="sdt" v-model='sdt' required
                                           disabled />
                                </div>
                                <div class="col-md-2">
                                    <label for="inputZip" class="form-label">Tuổi</label>
                                    <input type="number" class="form-control" name='tuoi' id="tuoi" v-model='tuoi'
                                           required disabled />
                                </div>
                                <div class="col-md-6">
                                    <label for="inputEmail4" class="form-label">Ngày đặt</label>
                                    <input type="date" class="form-control" name='ngaydat' id="ngaydat"
                                           v-model='ngaydat' required disabled />
                                </div>
                                <div class="col-md-6">
                                    <label for="inputPassword4" class="form-label">Ngày khám</label>
                                    <input type="date" class="form-control" name='ngaykham' id="ngaykham"
                                           @change="fetchSchedule()" id='ngaykham' v-model='ngaykham' required />
                                </div>
                                <div class="col-md-4">
                                    <label for="inputEmail4" class="form-label">Lịch khám: {{time}} ngày:{{ngaykham}}</label>
                                    <select class="form-select" v-model='lich_kham' id='lich_kham'
                                            aria-label="Default select example">
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="inputEmail4" class="form-label">Chuyên khoa</label>
                                    <select class="form-select" v-model='chuyen_khoa' id='chuyen_khoa'
                                            aria-label="Default select example">

                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="inputPassword4" class="form-label">Trạng thái</label>
                                    <select class="form-select" v-model='trangthai' aria-label="Default select example">
                                        <option value="0">Mới</option>
                                        <option value="1">Chấp nhận</option>
                                        <option value="2">Đã khám</option>
                                        <option value="3">Không đến</option>
                                        <option value="4">Từ chối</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="f1" class="form-label">Bác sĩ khám</label>
                                    <input type="text" class="form-control" name='bacsi' id="bacsi" v-model='bacsi'
                                           required disabled />
                                </div>
                                <div class="mb-3">
                                    <label for="exampleFormControlTextarea1" class="form-label">Lý do khám</label>
                                    <textarea class="form-control" name='lydo' v-model='lydo' id="lydo" rows="3"
                                              disabled></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" v-model="hiddenId" />
                                <button type="button" class="btn btn-secondary" @click='myModel=false'>Thoát</button>
                                <button type="button" class="btn btn-primary" @click='submitData'>Lưu</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr class="table-primary">
                    <th scope="col">STT</th>
                    <th scope="col">Tên Khách Hàng</th>
                    <th scope="col">Số điện thoại</th>
                    <th scope="col">Tuổi</th>
                    <th scope="col">Lịch khám</th>
                    <th scope="col">Ngày khám</th>
                    <th scope="col">Bác sĩ</th>
                    <th scope="col">Trạng Thái</th>
                    <th scope="col">Chi tiết</th>
                </tr>
            </thead>
            <tbody v-for='row, index in allData'>
                <tr>
                    <th scope="row">{{index+1}}</th>
                    <td>{{row.ten}}</td>
                    <td>{{row.sdt}}</td>
                    <td>{{row.tuoi}}</td>
                    <td>{{row.time_slot}}</td>
                    <td>{{row.ngaykham}}</td>
                    <td>{{row.bacsi}}</td>
                    <td>{{row.trang_thai | trangthai}}</td>
                    <td>
                        <button type="button" class="btn btn-primary" @click="fetchData(row.id)">Xem</button>
                    </td>
                </tr>
                <tr class="table table-borderless comment-lich" v-for="rows in comment[row.id]" style='font-size: 14px'>
                    <td colspan="1">{{rows.day}}</td>
                    <td colspan="1">{{rows.name}} </td>
                    <td colspan="7">{{rows.content}}</td>
                </tr>
                <tr>
                    <td colspan="9">
                        <p>
                            <a  data-bs-toggle="collapse" v-bind:href=" '#comment'+row.id" role="button" aria-expanded="false" aria-controls="collapseExample">
                                Comment
                            </a>
                        </p>
                        <div class="collapse" v-bind:id=" 'comment'+row.id">
                            <div class=" card-body">
                                <div class="row g-2">
                                    <div class="col-5">
                                        <textarea class="form-control" v-model="noidung" style='font-size:12px' rows="2"></textarea>
                                    </div>
                                    <div class="col-auto">
                                        <button type="button" @click='submitComment(row.id)' class="btn btn-primary">Lưu</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
    var application = new Vue({
        el: '#lich',
        data: {
            allChuyenkhoa: '',
            allData: '',
            comment: '',
            dynamicTitle: '',
            actionButton: 'Insert',
            myModel: false,
            tukhoa: '',
            bacsi: '',
            chuyenkhoa: '',
            in_trash: 0,
            pageList: [],
            limit: 5,
            current_page: 1,
            id_bs: '',
            day: '',
            time: '',
            noidung: '',
            select: '',
            select1:'',
            orderfr:'',
            orderto:'',
            lich_kham:'',
        },
        methods: {
            fetchAllData: function (page = 1) {
                application.in_trash = 0;
                if (page) {
                    application.current_page = page;
                }
                $.ajax({
                    url: "<?php echo Yii::app()->createUrl('lichkham/admin/GetList') ?>",
                    type: 'GET',
                    data: {
                        in_trash: 0,
                        limit: 5,
                        page: application.current_page,
                        bacsi: application.select,
                        chuyenkhoa: application.select1,
                        tukhoa: application.tukhoa,
                        tu_ngay:application.orderfr,
                        den_ngay:application.orderto,
                    },
                    success: function (res) {
                        application.comment = res.data.comment;
                        application.allData = res.data.data;
                        var page_number = Math.ceil(res.data.total / application.limit);
                        application.pageList = [];
                        for (var i = 1; i <= page_number; i++) {
                            application.pageList.push(i);
                        }
                        $('.js-example-basic-single').select2();
                        $('#select').on('select2:select', function (e) {
                            var data = e.params.data['id'];
                            application.select=data;
                            application.fetchAllData();
                        });
                        $('#select1').on('select2:select', function (e) {
                            var data = e.params.data['id'];
                            application.select1=data;
                            application.fetchAllData();
                        });
                    },
                });
            },
            openModel: function () {
                application.ten = '';
                application.sdt = '';
                application.bacsi = '';
                application.tuoi = '';
                application.ngaydat = '';
                application.ngaykham = '';
                application.lich_kham = '';
                application.lydo = '';
                application.trangthai = 0;
                application.hiddenId = '';
                application.actionButton = "up";
                application.dynamicTitle = "Sửa thông tin";
                application.myModel = true;
            },
            fetchData: function (id) {

                $.ajax({
                    url: "<?php echo Yii::app()->createUrl('lichkham/admin/getOneList') ?>",
                    type: 'GET',
                    data: {
                        id: id,
                    },
                }).then(function (res) {
                    application.id_bs = res.data.id_bs;
                    application.hiddenId = res.data.id;
                    application.ten = res.data.ten;
                    application.sdt = res.data.sdt;
                    application.tuoi = res.data.tuoi;
                    application.lydo = res.data.lydo;
                    application.ngaydat = res.data.ngaydat;
                    application.ngaykham = res.data.ngaykham;
                    application.fetchSchedule();
                    application.fetchChuyenkhoa(res.data.id_bs);
                    application.bacsi = res.data.bacsi;
                    application.trangthai = res.data.trang_thai;
                    application.myModel = true;
                    application.actionButton = 'up';
                    application.dynamicTitle = 'Sửa thông tin';
                    application.id_lk = res.data.id_time;
                    application.day = res.data.ngaykham;
                    application.time = res.data.time;
                    application.chuyen_khoa = res.data.chuyenkhoa;
                });
            },
            fetchChuyenkhoa: function (id) {
                $.ajax({
                    url: "<?php echo Yii::app()->createUrl('lichkham/admin/getChuyenkhoa') ?>",
                    type: "GET",
                    data: {
                        id: id,
                    },
                }).then(function (res) {
                    $('#chuyen_khoa').empty();
                    res.data.map(e => $('#chuyen_khoa').append(
                                `<option value="${e.id}">${e.name}</option>`));
                    document.getElementById("chuyen_khoa").value = application.chuyen_khoa;
                });
            },
            fetchSchedule: function () {
                $.ajax({
                    url: "<?php echo Yii::app()->createUrl('lichkham/admin/getSchedule') ?>",
                    type: "GET",
                    data: {
                        id: application.hiddenId,
                        date: application.ngaykham,
                    },
                }).then(function (res) {
                    $('#lich_kham').empty();
                    if (!res.data) {
                        $('#lich_kham').append(
                                `<option value="">Không có lịch khám</option>`);
                    } else {
                        $('#lich_kham').append(
                                `<option value="" selected>Thay đổi</option>`);
                        res.data.map(e => $('#lich_kham').append(
                                    `<option value="${e.id}">${e.name}</option>`));
                        document.getElementById("lich_kham").value = '';
                    }
                });
            },
            submitData: function () {
                if (application.actionButton == 'up') {
                    $.ajax({
                        url: "<?php echo Yii::app()->createUrl('lichkham/admin/Upschedule') ?>",
                        type: 'POST',
                        data: {
                            id: application.hiddenId,
                            ngaykham: application.ngaykham,
                            lich: application.lich_kham,
                            trangthai: application.trangthai,
                            chuyenkhoa:application.chuyen_khoa,
                        },
                    }).then(function (res) {
                        application.myModel = false;
                        application.fetchAllData();
                    });
                }
            },
            submitComment: function (id) {
                $.ajax({
                    url: "<?php echo Yii::app()->createUrl('lichkham/admin/addcomment') ?>",
                    type: 'POST',
                    data: {
                        id: id,
                        content: application.noidung,
                    },
                }).then(function (res) {
                    application.noidung = '';
                    application.fetchAllData();
                });
            },
        },
        filters: {
            trangthai: function (id) {
                if (id == 0) {
                    return 'Mới';
                } else if (id == 1) {
                    return 'Chấp nhận';
                } else if (id == 2) {
                    return 'Đã khám';
                } else if (id == 3) {
                    return 'Không đến';
                } else if (id == 4) {
                    return 'Từ chối';
                }
            }
        },
    });

    application.fetchAllData();
</script>