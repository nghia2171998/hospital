<?php
/**
 * Admin Controller of lichkham module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 04/12/2021 16:00:46
 * @version 1.0
 */
class DefaultController extends FrontController 
{
    
    function init()
    {
        parent::init();
        $this->pageTitle = '';
    }

    function actionIndex()
    {
        $this->render("index");
    }
    
}