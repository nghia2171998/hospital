<?php

/**
 * Admin Controller of lichkham module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 04/12/2021 16:00:46
 * @version 1.0
 */
class AdminController extends BackController {

    public $modelName = 'Lichkham';

    function init() {
        parent::init();
        $this->pageTitle = $this->modelName;
    }

    function getFormAttr() {
        return array(
            '' => array(
                array(
                    'field' => 'name'
                ),
                array(
                    'field' => 'status',
                    'type' => 'select'
                )
            ),
        );
    }

    /**
     * Danh sách các trường dùng cho danh sách
     * @return array
     */
    public function getListAttr() {
        return array(
            'name',
            'status'
        );
    }

    /**
     * Danh sách các trường dùng cho lọc
     * @return array
     */
    public function getFilterAttr() {
        return array(
            array(
                'field' => 'name',
                'type' => 'search'
            ),
            array(
                'field' => 'status',
                'type' => 'select'
            ),
        );
    }

    public function getAttrData() {
        $rtn = parent::getAttrData();
        return $rtn;
    }

    function actionIndex() {
        $lich = new CDbCriteria();
        $lich->order = 'id DESC';
        $count = TbLich::model()->count($lich);
        $pages = new CPagination($count);
        $pages->pageSize = 8;
        $pages->applyLimit($lich);
        $models = TbLich::model()->findAll($lich);

        $this->render('index', array(
            'lich' => $models,
            'pages' => $pages,
            'count' => $count
        ));
    }

    function actionGetlist() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $offset = ($page - 1) * $limit;
        $in_trash = $_GET['in_trash'];
        $bs = new CDbCriteria();
        $bs->order = 'id DESC';
        $bs->compare('in_trash', $in_trash);
        if ($_GET['bacsi'] != "") {
            $bs->compare('bac_si', $_GET['bacsi']);
        }
        if (isset($_GET['select']) != '') {
            $bs->compare('trang_thai', $_GET['select']);
        }
        if (isset($_GET['chuyenkhoa']) != '') {
            $bs->compare('chuyenkhoa', $_GET['chuyenkhoa']);
        }
        if ((isset($_GET['tu_ngay']) != ' ') && (isset($_GET['den_ngay']) != ' ')) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $bs->addBetweenCondition('ngay_kham', strtotime($_GET['tu_ngay']), strtotime($_GET['den_ngay']), 'AND');
        }
        if (isset($_GET['tukhoa'])) {
            $bs->addSearchCondition('ten_kh', $_GET['tukhoa']);
        }
        $bs->limit = $limit;
        $bs->offset = $offset;
        $bacsi = TbLich::model()->findAll($bs);
        $numRec = TbLich::model()->count($bs);
        $numPage = ($numRec % $limit) ? ($numRec / $limit + 1) : $numRec / $limit;
        $rtn = array();
        $con = array();
        foreach ($bacsi as $item) {
            $b = TbBacsi::model()->findByPk($item['bac_si']);
            $c = new CDbCriteria();
            $c->compare('id_schedule', $item->id);
            $comment = TbComment::model()->findAll($c);
            foreach ($comment as $co) {
                $con[$item->id][] = array(
                    'name' => $co->name,
                    'content' => $co->content,
                    'day' => date('H:i:s d/m/Y', $co->create_at),
                );
            }
            $rtn[] = array(
                'id' => $item->id,
                'ten' => $item->ten_kh,
                'sdt' => $item->sdt,
                'ngaykham' => date('d/m/Y', $item->ngay_kham),
                'tuoi' => $item->tuoi,
                'time_slot' => $item->time_slot,
                'bacsi' => $b->ten,
                'trang_thai' => $item->trang_thai,
            );
        }
        $value = array('data' => $rtn, 'comment' => $con, 'page' => $page, 'offset' => $offset, 'numPage' => $numPage, 'total' => $numRec);
        $this->renderJson("200", "thanh cong", $value);
    }

    function actiongetOneList() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $id = $_GET['id'];
        $data = TbLich::model()->findByPk($id);
        $bs = TbBacsi::model()->findByPk($data->bac_si);
        $rtn = array();
        $rtn['id'] = $data->id;
        $rtn['chuyenkhoa'] = $data->chuyenkhoa;
        $rtn['ten'] = $data->ten_kh;
        $rtn['sdt'] = $data->sdt;
        $rtn['diachi'] = $data->dia_chi;
        $rtn['tuoi'] = $data->tuoi;
        $rtn['lydo'] = $data->lydo;
        $rtn['time'] = $data->time_slot;
        $rtn['id_bs'] = $data->bac_si;
        $rtn['bacsi'] = $bs->ten;
        $rtn['ngaydat'] = date('Y-m-d', $data->ngay_dat);
        $rtn['ngaykham'] = date('Y-m-d', $data->ngay_kham);
        $rtn['trang_thai'] = $data->trang_thai;

        $this->renderJson("200", "thanh cong", $rtn);
    }

    function actiongetSchedule() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $lich = TbLich::model()->findByPk($_GET['id']);
        $s = ['s', 'c', 't', 'd'];
        $rtn = array();
        $date = explode('-', $_GET['date']);
        $dayofweek = date('w', strtotime($_GET['date'])) - 1;
        $nk = strtotime($_GET['date']);
        if ($dayofweek == -1) {
            $dayofweek = 6;
        }
        foreach ($s as $s) {
            $time = new CDbCriteria();
            $time->compare('day', $date[2]);
            $time->compare('month', $date[1]);
            $time->compare('year', $date[0]);
            $time->compare('id_bs', $lich->bac_si);
            $time->compare('session_day', $s);
            $time->compare('trang_thai', 0);
            $count = TbSettime::model()->count($time);
            if ($count >= 1) {
                $lk = TbSettime::model()->findAll($time);
            } else {
                $k = new CDbCriteria();
                $k->compare('id_bs', $lich->bac_si);
                $k->compare('weekday', $dayofweek);
                $k->compare('black_day', 0);
                $k->compare('session_day', $s);
                $k->compare('trang_thai', 0);
                $lk = TbSettime::model()->findAll($k);
            }
            foreach ($lk as $item) {
                if ($dayofweek >= $item['han_thu']) {
                    $day = $dayofweek - $item['han_thu'];
                } elseif ($dayofweek < $item['han_thu']) {
                    $day = 7 - $item['han_thu'] + $dayofweek;
                }
                $newdate = date('m/d/Y', strtotime('-' . $day . ' day', $nk));
                $schedule_day = strtotime($item['han_gio'] . ":" . $item['han_phut'] . ":00 " . $newdate);
                $t = new CDbCriteria();
                $t->compare('bac_si', $lich->bac_si);
                $t->compare('weekday', $dayofweek);
                $t->compare('ngay_kham', $nk);
                $t->addSearchCondition('time_slot', $item['name']);
                $count_schedule = TbLich::model()->count($t);
                if ($count_schedule == 0 && strtotime("now") < $schedule_day) {
                    $rtn[] = array(
                        'id' => $item->id,
                        'name' => $item->name,
                    );
                }
            }
        }
        $this->renderJson("200", "thanh cong", $rtn);
    }

    function actiongetChuyenkhoa() {
        $bs = TbBacsi::model()->findByPk($_GET['id']);
        $ck = json_decode($bs['chuc_vu']);
        $rtn = array();
        foreach ($ck as $ck) {
            $item = TbChuyenkhoa::model()->findByPk($ck);
            if ($item->trang_thai == 1 && $item->in_trash == 0) {
                $rtn[] = array(
                    'name' => $item->ten_khoa,
                    'id' => $item->id,
                );
            }
        }
        $this->renderJson("200", "thanh cong", $rtn);
    }

    function actionUpschedule() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $lich = TbLich::model()->findByPk($_POST['id']);
        if (isset($_POST['lich']) && $_POST['lich'] != '') {
            $time = TbSettime::model()->findByPk($_POST['lich']);
            $lich->time_slot = $time->name;
            $lich->time = $time->time;
            $lich->hour = $time->hour;
            $lich->minute = $time->minute;
            $lich->ngay_kham = strtotime($_POST['ngaykham']);
        }
        $lich->chuyenkhoa = $_POST['chuyenkhoa'];
        $lich->trang_thai = $_POST['trangthai'];
        $lich->update_at = time();
        $lich->save();
    }

    function actionaddcomment() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        echo $_SESSION['User'];
        $c = new TbComment();
        $c->id_schedule = $_POST['id'];
        $c->name = $_SESSION['User'];
        $c->content = $_POST['content'];
        $c->create_at = time();
        $c->save();
    }

}
