<?php
/**
 * lichkham module
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 04/12/2021 16:00:46
 * @version 1.0
 */
class LichkhamModule extends MyModule
{

    public function init()
    {        
        /**
        * $this->setImport(array(
        *    'lichkham.models.*',
        *    'lichkham.components.*',
        * ));
        */
    }
    
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {     
            return true;
        }
        else
        {
            return false;
        }    
    }

}
