<silde-show>
    <div class='khung'>
        <div class='detail' style='margin-top: 20px;'>
            <div>
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo Yii::app()->baseUrl ?>/">Trang chủ</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Thông Tin Truyền Thông</li>
                    </ol>
                </nav>
                <h1>Thông Tin Truyền Thông</h1>
            </div>
        </div>
        <div class='more'>
            <ul>
                <?php
                    foreach($tt as $row)
                    {
                    ?>
                <li>
                    <a href="<?php echo $row['url'] ?>" target="_blank">
                        <div class="img">
                            <img src='<?php echo Yii::app()->baseUrl ?>/file/<?=$row['image']?>' />
                        </div>
                        <p><?=$row['content']?></p>
                    </a>
                </li>
                <?php
                    }
                    ?>
            </ul>
        </div>
    </div>
    <div class='p_trang'>
        <?php
        $config = [
            'total' => $count,
            'limit' => $pages->pageSize,
            'full' => false,
            'querystring' => 'page',
            'url'=>'Truyen-thong'
        ];
        
        $page = new Pagination($config);
        
        $h= ceil($count/$pages->pageSize);
        if($h>1)
        {
            echo $page->getPagination();
        }
    ?>
    </div>

</silde-show>