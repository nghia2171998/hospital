<?php

/**
 * Admin Controller of truyenthong module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 03/11/2021 03:36:23
 * @version 1.0
 */
class AdminController extends BackController {

    public $modelName = 'Truyenthong';

    function init() {
        parent::init();
        $this->pageTitle = $this->modelName;
    }

    function getFormAttr() {
        return array(
            '' => array(
                array(
                    'field' => 'name'
                ),
                array(
                    'field' => 'status',
                    'type' => 'select'
                )
            ),
        );
    }

    /**
     * Danh sách các trường dùng cho danh sách
     * @return array
     */
    public function getListAttr() {
        return array(
            'name',
            'status'
        );
    }

    /**
     * Danh sách các trường dùng cho lọc
     * @return array
     */
    public function getFilterAttr() {
        return array(
            array(
                'field' => 'name',
                'type' => 'search'
            ),
            array(
                'field' => 'status',
                'type' => 'select'
            ),
        );
    }

    function getListAttr1() {
        return array(
            array(
                'field' => 'ten',
                'name' => 'Tên bài',
                'type' => 'text'
            ),
            array(
                'field' => 'image',
                'name' => 'Ảnh',
                'type' => 'img',
            ),
            array(
                'field' => 'trang_thai',
                'name' => 'Trạng thái',
                'type' => 'text'
            ),
        );
    }

    function getFormAttr1() {
        return array(
            array(
                'field' => 'ten',
                'name' => 'Tên bài viết',
                'type' => 'text',
                'value' => ''
            ),
            array(
                'field' => 'image',
                'name' => 'Ảnh',
                'type' => 'img',
                'value' => '',
            ),
            array(
                'field' => 'url',
                'name' => 'Đường dẫn',
                'type' => 'text',
                'value' => '',
            ),
            array(
                'field' => 'uu_tien',
                'name' => "Thứ tự ưu tiên",
                'type' => 'text',
                'value' => ''
            ),
            array(
                'field' => 'trang_thai',
                'name' => 'trạng thái',
                'type' => 'radio',
                'value' => ''
            ),
        );
    }

    function getTitle() {
        return array(
            'Title' => 'Truyền thông',
            'addbutton'=>'1',
        );
    }


    public function getAttrData() {
        $rtn = parent::getAttrData();
        return $rtn;
    }

    function actionGetList() {
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $offset = ($page - 1) * $limit;
        $in_trash = $_GET['in_trash'];

        $tt = new CDbCriteria();
        $tt->order = 'id DESC';
        $tt->compare('in_trash', $in_trash);
        if (isset($_GET['tukhoa'])) {
            $tt->addSearchCondition('content', $_GET['tukhoa']);
        }
        if (isset($_GET['select']) != '') {
            $tt->compare('trang_thai', $_GET['select']);
        }
        $tt->limit = $limit;
        $tt->offset = $offset;

        $data = TbTruyenthong::model()->findAll($tt);
        $numRec = TbTruyenthong::model()->count($tt);
        $numPage = ($numRec % $limit) ? ($numRec / $limit + 1) : $numRec / $limit;
        $rtn = array();
        foreach ($data as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'url' => $item->image,
                'ten' => $item->content,
                'image' => $item->image,
                'trang_thai' => $item->trang_thai,
            );
        }
        $value = array('data' => $rtn, 'page' => $page, 'offset' => $offset, 'numPage' => $numPage, 'total' => $numRec);
        $this->renderJson('200', 'successful', $value);
    }

    function actionGetSearch() {
        $tt = new CDbCriteria();
        $in_trash = $_GET['in_trash'];
        if (isset($_GET['keyword'])) {
            $tt->addSearchCondition('content', $_GET['keyword']);
        }
        if (isset($_GET['select']) != '') {
            $tt->compare('trang_thai', $_GET['select']);
        }
        $tt->compare('in_trash', $in_trash);
        $tt->order = 'id DESC';
        $models = TbTruyenthong::model()->findAll($tt);
        $rtn = array();
        foreach ($models as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'url' => $item->image,
                'content' => $item->content,
                'image' => $item->image,
                'trang_thai' => $item->trang_thai,
            );
        }
        $this->renderJson("200", "thanh cong", $rtn);
    }

    function actiongetOneList() {
        $id = $_GET['id'];
        $data = TbTruyenthong::model()->findByPk($id);
        $rtn = array();
        $rtn['id'] = $data->id;
        $rtn['ten'] = $data->content;
        $rtn['anh'] = $data->image;
        $rtn['url'] = $data->url;
        $rtn['uu_tien'] = $data->uu_tien;
        $rtn['trang_thai'] = $data->trang_thai;
        $this->renderJson('200', 'successfull', $rtn);
    }

    function actionAddList() {
        $tt = new TbTruyenthong();
        $tt->content = $_POST['ten'];
        $tt->url = $_POST['url'];
        $tt->image = $_POST['anh'];
        $tt->uu_tien = $_POST['uu_tien'];
        $tt->trang_thai = (integer) $_POST['rd1'];
        $tt->in_trash = 0;
        $tt->created_at = time();
        $tt->save();
    }

    function actionUpList() {
        $id = $_POST['hiddenId'];
        $tt = TbTruyenthong::model()->findByPk($id);
        $tt->image = $_POST['anh'];
        $tt->content = $_POST['ten'];
        $tt->url = $_POST['url'];
        $tt->uu_tien = $_POST['uu_tien'];
        $tt->trang_thai = (integer) $_POST['rd1'];
        $tt->update_at = time();
        $tt->save();
    }

    function actionHidden() {
        $id = $_POST['id'];
        $tt = TbTruyenthong::model()->findByPk($id);
        $tt->in_trash = 1;
        $tt->save();
    }

    function actionUnHidden() {
        $id = $_POST['id'];
        $ck = TbTruyenthong::model()->findByPk($id);
        $ck->in_trash = 0;
        $ck->save();
    }

    
}