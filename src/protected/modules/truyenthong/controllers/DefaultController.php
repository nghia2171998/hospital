<?php
/**
 * Admin Controller of truyenthong module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 03/11/2021 03:36:23
 * @version 1.0
 */
class DefaultController extends FrontController 
{
    
    function init()
    {
        parent::init();
        $this->pageTitle = '';
    }

    function actionIndex()
    {
        $tt=new CDbCriteria();
        $tt->compare('trang_thai', 1);
        $tt -> compare('in_trash', 0);
        $tt -> order = 'uu_tien';
        $count=TbTruyenthong::model()->count($tt);
        $pages=new CPagination($count);
        $pages->pageSize=5;
        $pages->applyLimit($tt);
        $models=TbTruyenthong::model()->findAll($tt);
        $this->render('index', array(
        'tt' => $models,
            'pages' => $pages,
            'count'=>$count
    ));
    }

    
    
}