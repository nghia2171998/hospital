<?php
/**
 * truyenthong module
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 03/11/2021 03:36:23
 * @version 1.0
 */
class TruyenthongModule extends MyModule
{

    public function init()
    {        
        /**
        * $this->setImport(array(
        *    'truyenthong.models.*',
        *    'truyenthong.components.*',
        * ));
        */
    }
    
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {     
            return true;
        }
        else
        {
            return false;
        }    
    }

}
