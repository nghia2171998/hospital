<?php

/**
 * Đây là lớp model của bảng "tb_truyenthong".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 04/03/2022 03:47:31
 * @version 1.0
 *
 * Dưới đây là các cột của bảng 'tb_truyenthong':
 * @property integer $id
 * @property string $url
 * @property integer $trang_thai
 * @property string $content
 * @property string $image
 * @property integer $in_trash
 * @property integer $uu_tien
 * @property integer $created_at
 * @property integer $update_at
 */
class TbTruyenthongBase extends MyModel
{

    public $modelName = 'TbTruyenthong';

    /**
     * @return string trả về tên của bảng
     */
    public function tableName()
    {
        return 'tb_truyenthong';
    }

    /**
     * @return array Các quy định về thuộc tính của lớp.
     */
    public function rules()
    {
        // LƯU Ý: bạn chỉ nên định nghĩa các quy định cho các trường
        // mà người dùng sẽ thể nhập liệu    
        return array(
            array('url, trang_thai, content, in_trash, uu_tien, created_at', 'required'),
            array('trang_thai, in_trash, uu_tien, created_at, update_at', 'numerical', 'integerOnly'=>true),
            array('url', 'length', 'max' => 200),
            array('content', 'length', 'max' => 500),
            array('image', 'length', 'max' => 100),
            // Những thuộc tính dưới đây để phục vụ cho hàm search().            
            // @todo Hãy xóa đi những trường mà bạn không muốn cho người dùng tìm kiếm
            array('id, url, trang_thai, content, image, in_trash, uu_tien, created_at, update_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array mối quan hệ với các bảng khác.
     */
    public function relations()
    {
        // LƯU Ý: Bạn có thể cần phải điều chỉnh tên các quan hệ 
        // và các lớp liên quan được tự động sinh ra bên dưới đây        
        $rtn = parent::relations();
        return $rtn;
    }

    /**
     * @return array Tùy chỉnh tên các thuộc tính (name=>label)
     */
    public function attributeLabels()
    {
        $rtn = parent::attributeLabels();
        $rtn['id'] = 'ID';
        $rtn['url'] = 'Url';
        $rtn['trang_thai'] = 'Trang Thai';
        $rtn['content'] = 'Content';
        $rtn['image'] = 'Image';
        $rtn['in_trash'] = 'In Trash';
        $rtn['uu_tien'] = 'Uu Tien';
        $rtn['created_at'] = 'Created At';
        $rtn['update_at'] = 'Update At';
        return $rtn;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TbTruyenthong the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}