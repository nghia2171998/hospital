<?php

/**
 * TbTruyenthong là lớp chính của bảng "tb_truyenthong".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 04/03/2022 02:31:10
 * @version 1.0
 *
 */
class TbTruyenthong extends TbTruyenthongBase
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Danh sách các cấu hình của model
     * @return array
     */
    public static function configArray()
    {
        $cnf = parent::configArray();
        return $cnf;
    }

    /**
     * Hàm gọi cấu hình theo model
     * @param string $item Tên của cấu hình
     * @return mix Giá trị của cấu hình tùy theo từng kiểu cấu hình mà có kiểu khác nhau
     */
    public static function getConf($item)
    {
        $option = Setting::getItem($item, __CLASS__);
        return $option ? $option : self::getDefaultConfig($item);
    }
    public static function Sort()
    {
        $cond = new CDbCriteria();
        $cond->compare('trang_thai', 1);
        $cond->compare('in_trash', 0);
        $cond->order = 'uu_tien';
        $tt = TbTruyenthong::model()->findAll($cond);
        return $tt;
    } 
}