<?php
/**
 * Admin Controller of hoidap module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 08/11/2021 10:22:25
 * @version 1.0
 */
class DefaultController extends FrontController 
{
    
    function init()
    {
        parent::init();
        $this->pageTitle = '';
    }

    function actionIndex()
    {
        $hd=new CDbCriteria();
        $hd -> compare('trang_thai', 1);
        $hd -> compare('in_trash', 0);
        $count=TbHoidap::model()->count($hd);
        $pages=new CPagination($count);
        $pages->pageSize=3;
        $pages->applyLimit($hd);
        $models=TbHoidap::model()->findAll($hd);
        
        $this->render('index', array(
        'hd' => $models,
            'pages' => $pages,
            'count'=>$count
    ));

    }

    public function actionAdd()
    {
        $h = new TbHoidap();
        $h->name=$_REQUEST['name'];
        $h->Phonenumber=$_REQUEST['phone'];
        $h->questions=$_REQUEST['questions'];
        $h->anserwers="";
        $h->trang_thai=0;
        $h->status=0;
        $h->in_trash=0;
        $h->created_at = time();
        $h->save();
        return $this->redirect(['index']);
    }
    
}