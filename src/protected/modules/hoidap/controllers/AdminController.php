<?php

/**
 * Admin Controller of hoidap module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 08/11/2021 10:22:25
 * @version 1.0
 */
class AdminController extends BackController {

    public $modelName = 'Hoidap';

    function init() {
        parent::init();
        $this->pageTitle = $this->modelName;
    }

    function getFormAttr() {
        return array(
            '' => array(
                array(
                    'field' => 'name'
                ),
                array(
                    'field' => 'status',
                    'type' => 'select'
                )
            ),
        );
    }

    /**
     * Danh sách các trường dùng cho danh sách
     * @return array
     */
    public function getListAttr() {
        return array(
            'name',
            'status'
        );
    }

    /**
     * Danh sách các trường dùng cho lọc
     * @return array
     */
    public function getFilterAttr() {
        return array(
            array(
                'field' => 'name',
                'type' => 'search'
            ),
            array(
                'field' => 'status',
                'type' => 'select'
            ),
        );
    }

    function getListAttr1() {
        return array(
            array(
                'field' => 'ten',
                'name' => 'Tên khách hàng',
                'type' => 'text'
            ),
            array(
                'field' => 'Phonenumber',
                'name' => 'Số điện thoại',
                'type' => 'text'
            ),
            array(
                'field' => 'trang_thai',
                'name' => 'Trạng thái',
                'type' => 'text'
            ),
        );
    }

    function getFormAttr1() {
        return array(
            array(
                'field' => 'questions',
                'name' => 'Câu hỏi',
                'type' => 'text',
                'value' => '',
            ),
            array(
                'field' => 'quesrecap',
                'name' => 'Câu hỏi rút gọn',
                'type' => 'text',
                'value' => '',
            ),
            array(
                'field' => 'title',
                'name' => "câu trả lời",
                'type' => 'textarea',
                'value' => ''
            ),
            array(
                'field' => 'trang_thai',
                'name' => 'trạng thái',
                'type' => 'radio',
                'value' => ''
            ),
        );
    }

    function getTitle() {
        return array(
            'Title' => 'hỏi đáp',
            'select' => '1',
        );
    }

    public function getAttrData() {
        $rtn = parent::getAttrData();
        return $rtn;
    }

    function actionGetList() {
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $offset = ($page - 1) * $limit;
        $in_trash = $_GET['in_trash'];

        $hd = new CDbCriteria();
        $hd->order = 'id DESC';
        $hd->compare('in_trash', $in_trash);
        if (isset($_GET['select']) != '') {
            $hd->compare('trang_thai', $_GET['select']);
        }
        if (isset($_GET['select1']) != '') {
            if ($_GET['select1'] == '0') {
                $hd->compare('status', 1);
            }
            if ($_GET['select1'] == '1') {
                $hd->compare('status', 0);
            }
        }
        if (isset($_GET['tukhoa'])) {
            $hd->addSearchCondition('name', $_GET['tukhoa']);
        }
        $hd->limit = $limit;
        $hd->offset = $offset;
        $hoidap = TbHoidap::model()->findAll($hd);
        $numRec = TbHoidap::model()->count($hd);
        $numPage = ($numRec % $limit) ? ($numRec / $limit + 1) : $numRec / $limit;
        $rtn = array();
        foreach ($hoidap as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'ten' => $item->name,
                'Phonenumber' => $item->Phonenumber,
                'questions' => $item->questions,
                'quesrecap' => $item->quesrecap,
                'anserwers' => $item->anserwers,
                'trang_thai' => $item->trang_thai
            );
        }
        $value = array('data' => $rtn, 'page' => $page, 'offset' => $offset, 'numPage' => $numPage, 'total' => $numRec);
        $this->renderJson("200", "thanh cong", $value);
    }

    function actionGetOneList() {
        $id = $_GET['id'];
        $data = TbHoidap::model()->findByPk($id);
        $rtn[] = array();
        $rtn['id'] = $data->id;
        $rtn['questions'] = $data->questions;
        $rtn['quesrecap'] = $data->quesrecap;
        $rtn['title'] = $data->anserwers;
        $rtn['trang_thai'] = $data->trang_thai;
        $this->renderJson("200", "thanh cong", $rtn);
    }

    function actionUpList() {
        $id = $_POST['hiddenId'];
        $hd = TbHoidap::model()->findByPk($id);
        $hd->questions = $_POST['questions'];
        $hd->quesrecap = $_POST['quesrecap'];
        $hd->anserwers = $_POST['title'];
        $hd->trang_thai = $_POST['rd1'];
        $hd->status = 1;
        $hd->update_at = time();
        $hd->save();
    }

    function actionHidden() {
        $id = $_POST['id'];
        $hd = TbHoidap::model()->findByPk($id);
        $hd->in_trash = 1;
        $hd->update_at = time();
        $hd->save();
    }

    function actionUnHidden() {
        $id = $_POST['id'];
        $hd = TbHoidap::model()->findByPk($id);
        $hd->in_trash = 0;
        $hd->update_at = time();
        $hd->save();
    }

}