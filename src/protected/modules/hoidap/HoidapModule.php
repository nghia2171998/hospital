<?php
/**
 * hoidap module
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 08/11/2021 10:22:25
 * @version 1.0
 */
class HoidapModule extends MyModule
{

    public function init()
    {        
        /**
        * $this->setImport(array(
        *    'hoidap.models.*',
        *    'hoidap.components.*',
        * ));
        */
    }
    
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {     
            return true;
        }
        else
        {
            return false;
        }    
    }

}
