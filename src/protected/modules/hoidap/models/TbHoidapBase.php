<?php

/**
 * Đây là lớp model của bảng "tb_hoidap".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 02/03/2022 07:53:28
 * @version 1.0
 *
 * Dưới đây là các cột của bảng 'tb_hoidap':
 * @property integer $id
 * @property string $name
 * @property string $Phonenumber
 * @property string $questions
 * @property string $quesrecap
 * @property string $anserwers
 * @property integer $trang_thai
 * @property integer $status
 * @property integer $in_trash
 * @property integer $created_at
 * @property integer $update_at
 */
class TbHoidapBase extends MyModel
{

    public $modelName = 'TbHoidap';

    /**
     * @return string trả về tên của bảng
     */
    public function tableName()
    {
        return 'tb_hoidap';
    }

    /**
     * @return array Các quy định về thuộc tính của lớp.
     */
    public function rules()
    {
        // LƯU Ý: bạn chỉ nên định nghĩa các quy định cho các trường
        // mà người dùng sẽ thể nhập liệu    
        return array(
            array('name, Phonenumber, questions, trang_thai, status, in_trash, created_at', 'required'),
            array('trang_thai, status, in_trash, created_at, update_at', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max' => 250),
            array('quesrecap, anserwers', 'safe'),
            // Những thuộc tính dưới đây để phục vụ cho hàm search().            
            // @todo Hãy xóa đi những trường mà bạn không muốn cho người dùng tìm kiếm
            array('id, name, Phonenumber, questions, quesrecap, anserwers, trang_thai, status, in_trash, created_at, update_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array mối quan hệ với các bảng khác.
     */
    public function relations()
    {
        // LƯU Ý: Bạn có thể cần phải điều chỉnh tên các quan hệ 
        // và các lớp liên quan được tự động sinh ra bên dưới đây        
        $rtn = parent::relations();
        return $rtn;
    }

    /**
     * @return array Tùy chỉnh tên các thuộc tính (name=>label)
     */
    public function attributeLabels()
    {
        $rtn = parent::attributeLabels();
        $rtn['id'] = 'ID';
        $rtn['name'] = 'Name';
        $rtn['Phonenumber'] = 'Phonenumber';
        $rtn['questions'] = 'Questions';
        $rtn['quesrecap'] = 'Quesrecap';
        $rtn['anserwers'] = 'Anserwers';
        $rtn['trang_thai'] = 'Trang Thai';
        $rtn['status'] = 'Status';
        $rtn['in_trash'] = 'In Trash';
        $rtn['created_at'] = 'Created At';
        $rtn['update_at'] = 'Update At';
        return $rtn;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TbHoidap the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}