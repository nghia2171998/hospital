<silde-show>
    <div class='khung'>
        <div class='detail' style='margin-top: 20px;'>
            <div>
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo Yii::app()->baseUrl ?>/">Trang chủ</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Hỏi đáp</li>
                    </ol>
                </nav>
                <hr />
                <h1 style='margin-top: 50px; text-decoration: underline'> HỎI ĐÁP </h1>
            </div>

            <div class="viewover">

                <div class="view">
                    <div class='views'>
                        <?php foreach ($hd as $hd){ ?>
                        <h4><?php echo $hd['quesrecap']?></h4>
                        <div class="viewhid">
                            <p style="color: red; font-weight: bold">Trả lời: </p>
                            <p><?php echo $hd['anserwers']?></p>
                        </div>
                        <div class="modal fade" id="exampleModalToggle" aria-hidden="true"
                            aria-labelledby="exampleModalToggleLabel" tabindex="-1">
                            <div style="max-width: 1000px;" class="modal-dialog modal-xl">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalToggleLabel">Câu hỏi:
                                            <?php echo $hd['questions']?></h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <h6>Câu trả lời:</h6>
                                        <p><?php echo$hd['anserwers']?></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Đóng</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a style="color: #0000FF; text-decoration: underline " data-bs-toggle="modal"
                            href="#exampleModalToggle" role="button">Xem
                            chi tiết >>></a>
                        <hr>
                        <?php } ?>
                    </div>
                </div>

                <div class="formhd">
                    <h5 style='text-align: center;'>Đặt câu hỏi</h5>
                    <div class="showform">
                        <form action='<?php echo yii::app()->createUrl('hoi-dap/add')?>' method='post'
                            enctype="multipart/form-data" onclick="return validateTel()">
                            <div class="mb-3">
                                <label for="exampleFormControlInput1" class="form-label">Họ và tên</label>
                                <input type="text" class="form-control" id="a1" name='name'
                                    required>
                            </div>
                            <div id="a5" style="color:red;"></div>
                            <div class="mb-3">
                                <label for="exampleFormControlInput1" class="form-label">Số điện thoại</label>
                                <input type="tel" class="form-control" id="a2" name='phone'>
                            </div>
                            <div id="a6" style="color:red;"></div>
                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Câu hỏi</label>
                                <textarea class="form-control" id="a3" name='questions' rows="3" required></textarea>
                                <div class="col-12">
                                    <button class="btn btn-primary" type="submit">Gửi câu hỏi</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class='p_trang'>
        <?php
        $config = [
            'total' => $count,
            'limit' => $pages->pageSize,
            'full' => false,
            'querystring' => 'page',
            'url'=>''
        ];
        
        $page = new Pagination($config);
        
        $h= ceil($count/$pages->pageSize);
        if($h>1)
        {
            echo $page->getPagination();
        }
    ?>
    </div>
</silde-show>
<script>
function validateTel() {
    var regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
    var txt = document.getElementById("a2");
    if (!regex.test(txt.value)) {
        document.getElementById("a6").innerHTML = "Nhập lại số điện thoại";
        return false;
    } else {
        document.getElementById("a6").innerHTML = "OK";
        return true;
    }
}
</script>