<silde-show>
    <div class='khung'>
        <div class='detail' style='margin-top: 20px;'>
            <div>
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo Yii::app()->baseUrl ?>/">Trang chủ</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo Yii::app()->baseUrl ?>/Chuyen-khoa">Dịch vụ và chuyên khoa</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?= $ck['ten_khoa'] ?></li>
                    </ol>
                </nav>
                <h1>Chuyên Khoa <?= $ck['ten_khoa'] ?></h1>
            </div>
            <div class='chi_tiet'>
                <img src="<?php echo Yii::app()->baseUrl ?>/file/<?= $ck['image'] ?>" />
                <p id='show'><?php echo $ck->content ?></p>
            </div>
        </div>

        <div class="bac_si">
            <h4>Bác sĩ</h4>
            <?php
            $g = MyUtil::checkBS($ck['id']);
            foreach ($g as $bs) {
                $row = TbBacsi::model()->findByPk($bs);
                if ($row['trang_thai'] == 1 && $row['in_trash'] == 0) {
                    ?>
                    <div style='width:80%;height:auto;margin:auto;margin-top:20px'>
                        <div style='width:100%;height:135px;border-bottom: 1px solid #eee;margin-bottom:5px'>
                            <a href='<?php echo Yii::app()->createUrl('bacsi/default', array('id' => $row['id'], 'url' => $row['url'])) ?>'
                               style='display:flex;color:black;text-decoration:none'>
                                <img width=160px; height=100px
                                     src='<?php echo Yii::app()->baseUrl ?>/file/<?= $row['image'] ?>' />
                                <h4 style='margin:0 0 0 10px; font-size: medium; overflow:hidden'><?= $row['ten'] ?></h4>
                            </a>
                        </div>
                    </div>
        <?php
    }
}
?>
        </div>
    </div>
</silde-show>