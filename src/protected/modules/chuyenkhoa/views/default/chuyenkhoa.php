<silde-show>
    <div class='khung'>
        <div class='detail' style='margin-top: 20px;'>
            <div>
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo Yii::app()->baseUrl ?>/">Trang chủ</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dịch vụ và chuyên khoa</li>
                    </ol>
                </nav>
                <h1>Dịch vụ và chuyên khoa</h1>
            </div>
        </div>
    </div>
    <div class="more">
        <ul>
            <?php
                    foreach($ck as $row)
                    {
                    ?>
            <li>
                <a style="display: flex"
                    href='<?php echo Yii::app()->createUrl('chuyenkhoa/default', array('id' => $row['id'], 'url' => $row['url'])) ?>'>
                    <img style="width:160px ; height:100px"
                        src='<?php echo Yii::app()->baseUrl ?>/file/<?=$row['image']?>' />
                    <h4 style='margin-left:20px;font-size: medium'><?=$row['ten_khoa']?></h4>
                </a>
            </li>
            <?php
                    }
                    ?>
        </ul>
    </div>
    <div class='p_trang'>
        <?php
        $config = [
            'total' => $count,
            'limit' => $pages->pageSize,
            'full' => false,
            'querystring' => 'page',
            'url'=>'Chuyen-khoa'
        ];
        
        $page = new Pagination($config);
        $h= ceil($count/$pages->pageSize);
        if($h>1)
        {
            echo $page->getPagination();
        }
        
    ?>
    </div>
</silde-show>