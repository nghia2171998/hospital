<?php
/**
 * chuyenkhoa module
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 25/10/2021 04:47:53
 * @version 1.0
 */
class ChuyenkhoaModule extends MyModule
{

    public function init()
    {        
        /**
        * $this->setImport(array(
        *    'chuyenkhoa.models.*',
        *    'chuyenkhoa.components.*',
        * ));
        */
    }
    
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {     
            return true;
        }
        else
        {
            return false;
        }    
    }

}
