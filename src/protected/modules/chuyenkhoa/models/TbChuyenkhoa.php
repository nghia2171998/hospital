<?php

/**
 * TbChuyenkhoa là lớp chính của bảng "tb_chuyenkhoa".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 06/01/2022 07:54:24
 * @version 1.0
 *
 */
class TbChuyenkhoa extends TbChuyenkhoaBase
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Danh sách các cấu hình của model
     * @return array
     */
    public static function configArray()
    {
        $cnf = parent::configArray();
        return $cnf;
    }

    /**
     * Hàm gọi cấu hình theo model
     * @param string $item Tên của cấu hình
     * @return mix Giá trị của cấu hình tùy theo từng kiểu cấu hình mà có kiểu khác nhau
     */
    public static function getConf($item)
    {
        $option = Setting::getItem($item, __CLASS__);
        return $option ? $option : self::getDefaultConfig($item);
    }
    static function Getchuyenkhoa() {
        $ck = new CDbCriteria();
        $ck->order = 'id DESC';
        $ck->compare('trang_thai', 1);
        $chuyenkhoa = TbChuyenkhoa::model()->findAll($ck);
        $rtn = array();
        foreach ($chuyenkhoa as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'text' => $item->ten_khoa,
            );
        }
        return $rtn;
    }
    public static function SearchCK()
    {
        $timkiem = new CDbCriteria();
        $timkiem->addSearchCondition('ten_khoa', $_REQUEST['tukhoa']);
        $timkiem->compare('trang_thai', 1);
        $timkiem->compare('in_trash', 0);
        $bs = TbChuyenkhoa::model()->findAll($timkiem);
        return $bs;
    }
    public static function Sort()
    {
        $cond = new CDbCriteria();
        $cond->compare('trang_thai', 1);
        $cond->compare('in_trash', 0);
        $cond->order = 'uu_tien';
        $ck = TbChuyenkhoa::model()->findAll($cond);
        return $ck;
    }

}