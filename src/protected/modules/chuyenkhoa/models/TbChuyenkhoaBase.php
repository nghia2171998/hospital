<?php

/**
 * Đây là lớp model của bảng "tb_chuyenkhoa".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 04/03/2022 04:01:00
 * @version 1.0
 *
 * Dưới đây là các cột của bảng 'tb_chuyenkhoa':
 * @property integer $id
 * @property string $ten_khoa
 * @property string $image
 * @property string $content
 * @property string $url
 * @property integer $uu_tien
 * @property integer $trang_thai
 * @property integer $in_trash
 * @property integer $created_at
 * @property integer $update_at
 */
class TbChuyenkhoaBase extends MyModel
{

    public $modelName = 'TbChuyenkhoa';

    /**
     * @return string trả về tên của bảng
     */
    public function tableName()
    {
        return 'tb_chuyenkhoa';
    }

    /**
     * @return array Các quy định về thuộc tính của lớp.
     */
    public function rules()
    {
        // LƯU Ý: bạn chỉ nên định nghĩa các quy định cho các trường
        // mà người dùng sẽ thể nhập liệu    
        return array(
            array('ten_khoa, content, url, uu_tien, trang_thai, in_trash, created_at', 'required'),
            array('uu_tien, trang_thai, in_trash, created_at, update_at', 'numerical', 'integerOnly'=>true),
            array('ten_khoa, url', 'length', 'max' => 200),
            array('image', 'length', 'max' => 100),
            // Những thuộc tính dưới đây để phục vụ cho hàm search().            
            // @todo Hãy xóa đi những trường mà bạn không muốn cho người dùng tìm kiếm
            array('id, ten_khoa, image, content, url, uu_tien, trang_thai, in_trash, created_at, update_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array mối quan hệ với các bảng khác.
     */
    public function relations()
    {
        // LƯU Ý: Bạn có thể cần phải điều chỉnh tên các quan hệ 
        // và các lớp liên quan được tự động sinh ra bên dưới đây        
        $rtn = parent::relations();
        return $rtn;
    }

    /**
     * @return array Tùy chỉnh tên các thuộc tính (name=>label)
     */
    public function attributeLabels()
    {
        $rtn = parent::attributeLabels();
        $rtn['id'] = 'ID';
        $rtn['ten_khoa'] = 'Ten Khoa';
        $rtn['image'] = 'Image';
        $rtn['content'] = 'Content';
        $rtn['url'] = 'Url';
        $rtn['uu_tien'] = 'Uu Tien';
        $rtn['trang_thai'] = 'Trang Thai';
        $rtn['in_trash'] = 'In Trash';
        $rtn['created_at'] = 'Created At';
        $rtn['update_at'] = 'Update At';
        return $rtn;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TbChuyenkhoa the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}