<?php
/**
 * Admin Controller of chuyenkhoa module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 25/10/2021 04:47:53
 * @version 1.0
 */
class DefaultController extends FrontController 
{
    
    function init()
    {
        parent::init();
        $this->pageTitle = '';
    }

    function actionIndex($id)
    {
        $ck = TbChuyenkhoa::model()->findByPk($id);
        $this->render("index", array('ck'=>$ck));
    }
    function actionChuyenkhoa()
    {
        $ck=new CDbCriteria();
        $ck->compare('trang_thai', 1);
        $ck->compare('in_trash', 0);
        $ck -> order = 'uu_tien';
        $count=TbChuyenkhoa::model()->count($ck);
        $pages=new CPagination($count);
        $pages->pageSize=5;
        $pages->applyLimit($ck);
        $models=TbChuyenkhoa::model()->findAll($ck);

        $this->render('chuyenkhoa', array(
        'ck' => $models,
            'pages' => $pages,
            'count'=>$count
            
    ));
    }
    
}