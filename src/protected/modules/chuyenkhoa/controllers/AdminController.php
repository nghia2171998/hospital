<?php

/**
 * Admin Controller of chuyenkhoa module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 25/10/2021 04:47:53
 * @version 1.0
 */
class AdminController extends BackController {

    public $modelName = 'Chuyenkhoa';

    function init() {
        parent::init();
        $this->pageTitle = $this->modelName;
    }

    function getFormAttr() {
        return array(
            '' => array(
                array(
                    'field' => 'name'
                ),
                array(
                    'field' => 'status',
                    'type' => 'select'
                )
            ),
        );
    }

    /**
     * Danh sách các trường dùng cho danh sách
     * @return array
     */
    public function getListAttr() {
        return array(
            'name',
            'status'
        );
    }

    /**
     * Danh sách các trường dùng cho lọc
     * @return array
     */
    public function getFilterAttr() {
        return array(
            array(
                'field' => 'name',
                'type' => 'search'
            ),
            array(
                'field' => 'status',
                'type' => 'select'
            ),
        );
    }

    public function getAttrData() {
        $rtn = parent::getAttrData();
        return $rtn;
    }

    function getListAttr1() {
        return array(
            array(
                'field' => 'ten_khoa',
                'name' => 'Tên khoa',
                'type' => 'text'
            ),
            array(
                'field' => 'image',
                'name' => 'Ảnh',
                'type' => 'img',
            ),
            array(
                'field' => 'trang_thai',
                'name' => 'Trạng thái',
                'type' => 'text'
            ),
        );
    }

    function getFormAttr1() {
        return array(
            array(
                'field' => 'ten_khoa',
                'name' => 'Tên',
                'type' => 'text',
                'value' => ''
            ),
            array(
                'field' => 'image',
                'name' => 'Ảnh',
                'type' => 'img',
                'value' => '',
            ),
            array(
                'field' => 'title',
                'name' => "Nội dung",
                'type' => 'textarea',
                'value' => ''
            ),
            array(
                'field' => 'uu_tien',
                'name' => "Thứ tự ưu tiên",
                'type' => 'text',
                'value' => ''
            ),
            array(
                'field' => 'trang_thai',
                'name' => 'trạng thái',
                'type' => 'radio',
                'value' => ''
            ),
        );
    }
    function getTitle(){
        return array(
            'Title'=>'Chuyên khoa',
            'addbutton'=>'1',
        );
    }

    function actionGetList() {
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $offset = ($page - 1) * $limit;
        $in_trash = $_GET['in_trash'];

        $ck = new CDbCriteria();
        $ck->order = 'id DESC';
        $ck->compare('in_trash', $in_trash);
        if (isset($_GET['tukhoa'])) {
            $ck->addSearchCondition('ten_khoa', $_GET['tukhoa']);
        }
        if (isset($_GET['select']) != '') {
            $ck->compare('trang_thai', $_GET['select']);
        }
        $ck->limit = $limit;
        $ck->offset = $offset;

        $data = TbChuyenkhoa::model()->findAll($ck);
        $numRec = TbChuyenkhoa::model()->count($ck);
        $numPage = ($numRec % $limit) ? ($numRec / $limit + 1) : $numRec / $limit;
        $rtn = array();
        foreach ($data as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'image' => $item->image,
                'ten_khoa' => $item->ten_khoa,
                'content' => $item->content,
                'url' => $item->url,
                'trang_thai' => $item->trang_thai,
            );
        }
        $value = array('data' => $rtn, 'page' => $page, 'offset' => $offset, 'numPage' => $numPage, 'total' => $numRec);
        $this->renderJson('200', 'successful', $value);
    }

    function actionAddList() {
        $ck = new TbChuyenkhoa();
        $ck->ten_khoa = $_POST['ten_khoa'];
        $ck->image = $_POST['anh'];
        $ck->content = $_POST['title'];
        $ck->url = MyUtil::to_slug($_POST['ten_khoa']);
        $ck->trang_thai = $_POST['rd1'];
        $ck->uu_tien = $_POST['uu_tien'];
        $ck->in_trash = 0;
        $ck->created_at = time();
        $ck->save();
    }

    function actionUpList() {
        $id = $_POST['hiddenId'];
        $ck = TbChuyenkhoa::model()->findByPk($id);
        $ck->ten_khoa = $_POST['ten_khoa'];
        $ck->image = $_POST['anh'];
        $ck->content = $_POST['title'];
        $ck->url = MyUtil::to_slug($_POST['ten_khoa']);
        $ck->trang_thai = (integer) $_POST['rd1'];
        $ck->uu_tien = $_POST['uu_tien'];
        $ck->update_at = time();
        $ck->save();
    }

    function actionGetOneList() {
        $id = $_GET['id'];
        $data = TbChuyenkhoa::model()->findByPk($id);
        $rtn = array();
        $rtn['id'] = $data->id;
        $rtn['ten_khoa'] = $data->ten_khoa;
        $rtn['anh'] = $data->image;
        $rtn['title'] = $data->content;
        $rtn['uu_tien'] = $data->uu_tien;
        $rtn['trang_thai'] = $data->trang_thai;
        $this->renderJson('200', 'successfull', $rtn);
    }

    function actionHidden() {
        $id=$_POST['id'];
        $ck = TbChuyenkhoa::model()->findByPk($id);
        $ck->in_trash = 1;
        $ck->save();
    }

    function actionUnHidden() {
        $id=$_POST['id'];
        $ck = TbChuyenkhoa::model()->findByPk($id);
        $ck->in_trash = 0;
        $ck->save();
    }

    function actionGetList1() {
        $ck = new CDbCriteria();
        $ck->order = 'id DESC';
        $ck->compare('trang_thai', 1);
        $chuyenkhoa = TbChuyenkhoa::model()->findAll($ck);
        $rtn = array();
        foreach ($chuyenkhoa as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'text' => $item->ten_khoa,
            );
        }
        $this->renderJson("200", "thanh cong", $rtn);
    }

}