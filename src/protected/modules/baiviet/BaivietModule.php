<?php
/**
 * baiviet module
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 15/11/2021 08:11:09
 * @version 1.0
 */
class BaivietModule extends MyModule
{

    public function init()
    {        
        /**
        * $this->setImport(array(
        *    'baiviet.models.*',
        *    'baiviet.components.*',
        * ));
        */
    }
    
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {     
            return true;
        }
        else
        {
            return false;
        }    
    }

}
