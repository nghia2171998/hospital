<silde-show>
    <div class='khung'>
        <div class='detail' style='margin-top: 20px;'>
            <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo Yii::app()->baseUrl ?>/">Trang chủ</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><a
                            href="<?php echo Yii::app()->baseUrl?>/danhmuc">Cẩm nang</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?=$danhmuc['name']?></li>
                </ol>
            </nav>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <h1 class="h1s"><?=$bv['ten']?></h1>
                        <div style='margin:20px 0 30px 0'>
                            <b><?=$bv['tomtat']?></b>
                        </div>
                        <img style='width:80%;display: block;margin: auto;'
                            src='<?php echo Yii::app()->baseUrl ?>/file/<?=$bv['image']?>' />
                        <div style='text-align: center;background-color:#f9f9f9;margin-bottom:50px'>
                            <i><?=$bv['mota_image']?></i>
                        </div>
                        <div class='baiviet'><?=$bv['noidung']?></div>
                    </div>

                    <div id='' class="col-md-4 hhh">
                        <div id="dinamicMenu">
                            <h5 style="padding: 5px">Nội dung chính</h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="viewdm">
                <h4>Bài Viết Liên Quan</h4>
                <?php
                $g=MyUtil::checkArr($_REQUEST['id']);
                $result = array_unique($g);
                foreach($result as $result)
                {
                    if($result != $_REQUEST['id'])
                    {
                        $article = TbBaiviet::model()->findByPk($result); 
                ?>

                <div style='width:80%;height:auto;margin:auto;margin-top:40px'>
                    <div style='width:100%;height:135px;border-bottom: 1px solid #eee;margin-bottom:5px'>
                        <a href='<?php echo Yii::app()->createUrl('baiviet/default', array('id' => $article['id'], 'url' => $article['url'])) ?>'
                            style='display:flex;color:black;text-decoration:none'>
                            <img width=200px; height=120px
                                src='<?php echo Yii::app()->baseUrl ?>/file/<?=$article['image']?>' />
                            <h4 style='margin:0 0 0 10px'><?=$article['ten']?></h4>
                        </a>
                    </div>
                </div>

                <?php 
                    }
                }
                ?>
            </div>
            <div class="viewdm">
                <h4>Danh Mục Cẩm Nang</h4>
                <div class='htdm'>

                    <ul>
                        <?php 
                $dm=TbDanhmuc::model()->findAll();
                foreach ($dm as $dm): ?>
                        <li>
                            <a
                                href="<?php echo Yii::app()->createUrl('danhmuc/default', array('id' => $dm['id'], 'url' => $dm['url'])) ?>">
                                <?php echo $dm['name']?>
                            </a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
</silde-show>


<script src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>
<script src="https://www.codehim.com/demo/jquery-dynamic-content-menu/js/jquery-dynamic-content-menu.js">
</script>
<script>
$(function() {
    if (!window.jQuery) {
        alert('jQuery not included!');
    }
    //Include plugin
    $("#dinamicMenu").dynamicContentMenu({
        // 'theme' : "material",
        'selectors': "h1, h2, h3, h4 .h1s",
        'extendPage': false, // do not increase page height
    });
});
window.onscroll = function() {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 250 || document.documentElement.scrollTop > 250) {
        document.getElementById("dinamicMenu").style.top = "50px";

    } else {
        document.getElementById("dinamicMenu").style.top = "390px";
    }
}
</script>