<?php

/**
 * Đây là lớp model của bảng "tb_baiviet".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 04/03/2022 05:25:57
 * @version 1.0
 *
 * Dưới đây là các cột của bảng 'tb_baiviet':
 * @property integer $id
 * @property string $ten
 * @property string $noidung
 * @property string $tomtat
 * @property string $danhmuc
 * @property string $image
 * @property string $mota_image
 * @property string $alt_image
 * @property string $url
 * @property integer $uu_tien
 * @property integer $trang_thai
 * @property integer $in_trash
 * @property integer $created_at
 * @property integer $update_at
 */
class TbBaivietBase extends MyModel
{

    public $modelName = 'TbBaiviet';

    /**
     * @return string trả về tên của bảng
     */
    public function tableName()
    {
        return 'tb_baiviet';
    }

    /**
     * @return array Các quy định về thuộc tính của lớp.
     */
    public function rules()
    {
        // LƯU Ý: bạn chỉ nên định nghĩa các quy định cho các trường
        // mà người dùng sẽ thể nhập liệu    
        return array(
            array('uu_tien, in_trash, created_at', 'required'),
            array('uu_tien, trang_thai, in_trash, created_at, update_at', 'numerical', 'integerOnly'=>true),
            array('ten, tomtat', 'length', 'max' => 500),
            array('danhmuc, image, mota_image, alt_image, url', 'length', 'max' => 250),
            array('noidung', 'safe'),
            // Những thuộc tính dưới đây để phục vụ cho hàm search().            
            // @todo Hãy xóa đi những trường mà bạn không muốn cho người dùng tìm kiếm
            array('id, ten, noidung, tomtat, danhmuc, image, mota_image, alt_image, url, uu_tien, trang_thai, in_trash, created_at, update_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array mối quan hệ với các bảng khác.
     */
    public function relations()
    {
        // LƯU Ý: Bạn có thể cần phải điều chỉnh tên các quan hệ 
        // và các lớp liên quan được tự động sinh ra bên dưới đây        
        $rtn = parent::relations();
        return $rtn;
    }

    /**
     * @return array Tùy chỉnh tên các thuộc tính (name=>label)
     */
    public function attributeLabels()
    {
        $rtn = parent::attributeLabels();
        $rtn['id'] = 'ID';
        $rtn['ten'] = 'Ten';
        $rtn['noidung'] = 'Noidung';
        $rtn['tomtat'] = 'Tomtat';
        $rtn['danhmuc'] = 'Danhmuc';
        $rtn['image'] = 'Image';
        $rtn['mota_image'] = 'Mota Image';
        $rtn['alt_image'] = 'Alt Image';
        $rtn['url'] = 'Url';
        $rtn['uu_tien'] = 'Uu Tien';
        $rtn['trang_thai'] = 'Trang Thai';
        $rtn['in_trash'] = 'In Trash';
        $rtn['created_at'] = 'Created At';
        $rtn['update_at'] = 'Update At';
        return $rtn;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TbBaiviet the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}