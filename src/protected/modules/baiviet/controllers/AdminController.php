<?php

/**
 * Admin Controller of baiviet module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 15/11/2021 08:11:09
 * @version 1.0
 */
class AdminController extends BackController {

    public $modelName = 'Baiviet';

    function init() {
        parent::init();
        $this->pageTitle = $this->modelName;
    }

    function getFormAttr() {
        return array(
            '' => array(
                array(
                    'field' => 'name'
                ),
                array(
                    'field' => 'status',
                    'type' => 'select'
                )
            ),
        );
    }

    /**
     * Danh sách các trường dùng cho danh sách
     * @return array
     */
    public function getListAttr() {
        return array(
            'name',
            'status'
        );
    }

    /**
     * Danh sách các trường dùng cho lọc
     * @return array
     */
    public function getFilterAttr() {
        return array(
            array(
                'field' => 'name',
                'type' => 'search'
            ),
            array(
                'field' => 'status',
                'type' => 'select'
            ),
        );
    }

    public function getAttrData() {
        $rtn = parent::getAttrData();
        return $rtn;
    }

    function getListAttr1() {
        return array(
            array(
                'field' => 'ten',
                'name' => 'Tên bài viết',
                'type' => 'text'
            ),
            array(
                'field' => 'image',
                'name' => 'Ảnh',
                'type' => 'img',
            ),
            array(
                'field' => 'trang_thai',
                'name' => 'Trạng thái',
                'type' => 'text'
            ),
        );
    }

    function getFormAttr1() {
        return array(
            array(
                'field' => 'ten',
                'name' => 'Tên bài viết',
                'type' => 'text',
                'value' => ''
            ),
            array(
                'field' => 'danhmuc',
                'name' => 'Danh mục',
                'type' => 'select2',
                'value' => '',
                'data' => TbDanhmuc::Getdanhmuc(),
            ),
            array(
                'field' => 'image',
                'name' => 'Ảnh',
                'type' => 'img',
                'value' => '',
            ),
            array(
                'field' => 'mota_image',
                'name' => 'Mô tả ảnh',
                'type' => 'text',
                'value' => '',
            ),
            array(
                'field' => 'tomtat',
                'name' => 'Tóm tắt',
                'type' => 'ckeditor',
                'value' => '',
            ),
            array(
                'field' => 'title',
                'name' => "Nội dung",
                'type' => 'textarea',
                'value' => ''
            ),
            array(
                'field' => 'uu_tien',
                'name' => "Thứ tự ưu tiên",
                'type' => 'text',
                'value' => ''
            ),
            array(
                'field' => 'trang_thai',
                'name' => 'trạng thái',
                'type' => 'radio',
                'value' => ''
            ),
        );
    }

    function getTitle() {
        return array(
            'Title' => 'Bài viết',
            'addbutton' => '1',
        );
    }

    public function actionGetList() {
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $offset = ($page - 1) * $limit;
        $in_trash = $_GET['in_trash'];

        $bv = new CDbCriteria();
        $bv->order = 'id DESC';
        $bv->compare('in_trash', $in_trash);
        if (isset($_GET['tukhoa'])) {
            $bv->addSearchCondition('ten', $_GET['tukhoa']);
        }
        if (isset($_GET['select']) != '') {
            $bv->compare('trang_thai', $_GET['select']);
        }
        $bv->limit = $limit;
        $bv->offset = $offset;
        $data = TbBaiviet::model()->findAll($bv);
        $numRec = TbBaiviet::model()->count($bv);
        $numPage = ($numRec % $limit) ? ($numRec / $limit + 1) : $numRec / $limit;
        $rtn = array();
        foreach ($data as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'ten' => $item->ten,
                'noidung' => $item->noidung,
                'tomtat' => $item->tomtat,
                'danhmuc' => $item->danhmuc,
                'image' => $item->image,
                'mota_image' => $item->mota_image,
                'alt_image' => $item->alt_image,
                'url' => $item->url,
                'trang_thai' => $item->trang_thai,
            );
        }
        $value = array('data' => $rtn, 'page' => $page, 'offset' => $offset, 'numPage' => $numPage, 'total' => $numRec);
        $this->renderJson('200', 'successful', $value);
    }

    function actionGetSearch() {
        $bv = new CDbCriteria();
        $in_trash = $_GET['in_trash'];
        if (isset($_GET['keyword'])) {
            $bv->addSearchCondition('ten', $_GET['keyword']);
        }
        if (isset($_GET['select']) != '') {
            $bv->compare('trang_thai', $_GET['select']);
        }
        $bv->compare('in_trash', $in_trash);
        $bv->order = 'id DESC';
        $models = TbBaiviet::model()->findAll($bv);
        $rtn = array();
        foreach ($models as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'ten' => $item->ten,
                'noidung' => $item->noidung,
                'tomtat' => $item->tomtat,
                'danhmuc' => $item->danhmuc,
                'image' => $item->image,
                'mota_image' => $item->mota_image,
                'alt_image' => $item->alt_image,
                'url' => $item->url,
                'trang_thai' => $item->trang_thai,
            );
        }
        $this->renderJson("200", "thanh cong", $rtn);
    }

    function actionAddList() {
        $bv = new TbBaiviet();
        $bv->ten = $_POST['ten'];
        $bv->noidung = $_POST['title'];
        $bv->tomtat = $_POST['tomtat'];
        $bv->image = $_POST['anh'];
        $bv->danhmuc = $_POST['danhmuc'];
        $bv->mota_image = $_POST['mota_image'];
        $bv->url = MyUtil::to_slug($_POST['ten']);
        $bv->uu_tien = $_POST['uu_tien'];
        $bv->trang_thai = (integer) $_POST['rd1'];
        $bv->in_trash = 0;
        $bv->created_at = time();
        $bv->save();
    }

    function actionGetOneList() {
        $id = $_GET['id'];
        $data = TbBaiviet::model()->findByPk($id);
        $rtn = array();
        $rtn['id'] = $data->id;
        $rtn['ten'] = $data->ten;
        $rtn['tomtat'] = $data->tomtat;
        $rtn['title'] = $data->noidung;
        $rtn['anh'] = $data->image;
        $rtn['danhmuc'] = json_decode($data->danhmuc);
        $rtn['mota_image'] = $data->mota_image;
        $rtn['uu_tien'] = $data->uu_tien;
        $rtn['trang_thai'] = $data->trang_thai;
        $this->renderJson("200", "thanh cong", $rtn);
    }

    function actionUpList() {
        $id = $_POST['hiddenId'];
        $bv = TbBaiviet::model()->findByPk($id);
        $bv->ten = $_POST['ten'];
        $bv->noidung = $_POST['title'];
        $bv->tomtat = $_POST['tomtat'];
        $bv->image = $_POST['anh'];
        $bv->danhmuc = $_POST['danhmuc'];
        $bv->mota_image = $_POST['mota_image'];
        $bv->url = MyUtil::to_slug($_POST['ten']);
        $bv->uu_tien = $_POST['uu_tien'];
        $bv->trang_thai = (integer) $_POST['rd1'];
        $bv->in_trash = 0;
        $bv->update_at = time();
        $bv->save();
    }

    function actionHidden() {
        $id = $_POST['id'];
        $bv = TbBaiviet::model()->findByPk($id);
        $bv->in_trash = 1;
        $bv->save();
    }

    function actionUnHidden() {
        $id = $_POST['id'];
        $bv = TbBaiviet::model()->findByPk($id);
        $bv->in_trash = 0;
        $bv->save();
    }

}