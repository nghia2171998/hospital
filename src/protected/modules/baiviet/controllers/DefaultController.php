<?php
/**
 * Admin Controller of baiviet module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 15/11/2021 08:11:09
 * @version 1.0
 */
class DefaultController extends FrontController 
{
    
    function init()
    {
        parent::init();
        $this->pageTitle = '';
    }

    function actionIndex($id)
    {
        $bv = TbBaiviet::model()->findByPk($id);
        $a=json_decode($bv['danhmuc']);
        $dm = TbDanhmuc::model()->findByPk($a[0]);
        $this->render("index", array('bv'=>$bv,'danhmuc'=>$dm));
    }
    
}