<silde-show>
    <div class='khung'>
        <div class='detail' style='margin-top: 20px;'>
            <div>
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo Yii::app()->baseUrl ?>/">Trang chủ</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo Yii::app()->baseUrl ?>/Bac-si">Bác sĩ</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?= $bs['ten'] ?></li>
                    </ol>
                </nav>
                <hr />
            </div>
            <div class='chitiet'>
                <div class='img_bs'>
                    <img src='<?php echo Yii::app()->baseUrl ?>/file/<?= $bs['image'] ?>' />
                </div>
                <div class='text'>
                    <h4><?= $bs['ten'] ?></h4>
                    <p><?= $bs['noidung'] ?></p>
                </div>
            </div>
            <div class='lich' style='display: flex'>
                <div class='chon_ngay'>
                    <select class="form-select" aria-label="Default select example" name='date' id='form_date'>

                        <?php
                        date_default_timezone_set('Asia/Ho_Chi_Minh');
                        $day = MyUtil::get_current_weekday();
                        $t2 = 0;
                        for ($i = $day; $i <= $day + 6; $i++) {
                            $t = $i - $day;
                            if ($i <= 6) {
                                $t1 = $i;
                            } else {
                                $t1 = $t2;
                                $t2++;
                            }
                            ?>
                            <option value="<?php echo date('Y-m-d', strtotime(' + ' . $t . ' days')) . "-" . $t1; ?>"><?php echo date('d-m-Y', strtotime(' + ' . $t . ' days')); ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <form action='<?php echo Yii::app()->createUrl('bacsi/default/datlich', array('id' => $bs['id'])) ?>' method='post'>
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Đặt lịch</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="row g-3">
                                        <div class="mb-3">
                                            <label for="exampleFormControlInput1" class="form-label">Họ tên</label>
                                            <input type="text" name='ten' class="form-control" id="exampleFormControlInput1" required>
                                        </div>
                                        <div class="col-md-9">
                                            <label for="inputEmail4" class="form-label">Số điện thoại</label>
                                            <input type="text" name='sdt' class="form-control" id="inputEmail4" required>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="inputZip" class="form-label">Tuổi</label>
                                            <input type="number" name='age' class="form-control" id="inputZip" min="0" max="120" required>
                                        </div>
                                        <div class="col-12">
                                            <label for="inputAddress" class="form-label">Địa chỉ</label>
                                            <input type="text" name='dc' class="form-control" id="inputAddress" required>
                                        </div>
                                        <div class="mb-3">
                                            <label for="exampleFormControlTextarea1" class="form-label">Lý do khám</label>
                                            <textarea class="form-control" name='lydo' id="exampleFormControlTextarea1" rows="3" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type='hidden' name='id_date' id='date_id'>
                                    <input type='hidden' name='time_slot' id='time_slot'>
                                    <input type='hidden' name='hour' id='hour'>
                                    <input type='hidden' name='minute' id='minute'>
                                    <input type='hidden' name='time' id='time'>
                                    <input type='hidden' name='deadtime' id='deadtime'>
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Đặt lịch</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class='show_time'>
                <div class="container">
                    <div class="row row-cols-2" id='chon'>
                    </div>
                </div>
            </div>
            <div class='mota'>
                <?= $bs['chitiet'] ?>
            </div>
        </div>
    </div>
</silde-show>
<script>
    $(document).ready(function (e) {
        $("[name='date']").change(function () {
            var url = "<?php echo Yii::app()->createUrl('bacsi/default/gettime') ?>";
            $.ajax({
                type: "GET",
                url: url,
                data: {date: $("#form_date").val(),
                    id: '<?= $_REQUEST['id'] ?>',
                },
                success: function (data) {
                    $('#chon').empty();
                    if (!data.data) {
                        $('#chon').append(`<div class="col-3 list">
                            <button type="button" class="btn btn-warning">Chưa có lịch khám</button>
                        </div>`);
                    } else {
                        document.getElementById('date_id').value = $("#form_date").val();
                        data.data.map(e => $('#chon').append(`<div class="col-3 list">
                            <input class="btn btn-primary" id='timeline' data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap" onclick='GetTime(id_time="${e.id}",time="${e.time}",time_slot="${e.name}",deadtime="${e.deadtime}",hour="${e.hour}",minute="${e.minute}")' type="button" value='${e.name}'>
                        </div>`));
                    }
                }

            });
        })
                .trigger("change");
    });
    function GetTime() {
        document.getElementById('time').value = time;
        document.getElementById('deadtime').value = deadtime;
        document.getElementById('hour').value = hour;
        document.getElementById('minute').value = minute;
        document.getElementById('time_slot').value = time_slot;
    }
</script>