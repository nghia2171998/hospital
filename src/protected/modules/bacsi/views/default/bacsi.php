<silde-show>
    <div class='khung'>
        <div class='detail' style='margin-top: 20px;'>
            <div>
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo Yii::app()->baseUrl ?>/">Trang chủ</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Bác sĩ</li>
                    </ol>
                </nav>
                <h1>Bác sĩ</h1>
            </div>
            <div class='bac_si'>
                <ul>
                    <?php
                    foreach($bs as $row)
                    {
                    ?>
                    <li>
                        <div style='width:80%;height:auto;margin:auto;margin:10px 0 10px 0'>
                            <a href='<?php echo Yii::app()->createUrl('bacsi/default', array('id' => $row['id'], 'url' => $row['url'])) ?>'
                                style='display:flex;color:black;text-decoration:none; height:auto'>
                                <img width=160px; height=100px
                                    src='<?php echo Yii::app()->baseUrl ?>/file/<?=$row['image']?>' />
                                <div style='display: block'>
                                    <h4 style='margin:0 0 0 10px; font-size: medium; overflow:hidden'>
                                        <?=$row['ten']?>
                                    </h4>
                                    <p style='margin-top:10px; text-overflow: ellipsis; '>
                                        <?php
									$x=json_decode( $row['chuc_vu']);
									foreach($x as $y){
										$ck = TbChuyenkhoa::model()->findByPk($y);
										echo $ck['ten_khoa'].", ";
									}
									?>
                                    </p>
                                </div>
                            </a>
                        </div>
                        <hr />
                    </li>

                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div class='p_trang'>
        <?php
        $config = [
            'total' => $count,
            'limit' => $pages->pageSize,
            'full' => false,
            'querystring' => 'page',
            'url'=>'Bac-si'
        ];
        
        $page = new Pagination($config);
        
        $h= ceil($count/$pages->pageSize);
        if($h>1)
        {
            echo $page->getPagination();
        }
    ?>
    </div>
</silde-show>