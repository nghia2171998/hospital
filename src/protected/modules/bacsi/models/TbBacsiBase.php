<?php

/**
 * Đây là lớp model của bảng "tb_bacsi".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 23/02/2022 09:22:05
 * @version 1.0
 *
 * Dưới đây là các cột của bảng 'tb_bacsi':
 * @property integer $id
 * @property string $ten
 * @property string $image
 * @property string $chuc_vu
 * @property string $noidung
 * @property string $chitiet
 * @property string $url
 * @property string $khung_gio
 * @property string $lich_kham
 * @property integer $trang_thai
 * @property integer $in_trash
 * @property integer $created_at
 * @property integer $update_at
 * @property integer $han_dat
 */
class TbBacsiBase extends MyModel
{

    public $modelName = 'TbBacsi';

    /**
     * @return string trả về tên của bảng
     */
    public function tableName()
    {
        return 'tb_bacsi';
    }

    /**
     * @return array Các quy định về thuộc tính của lớp.
     */
    public function rules()
    {
        // LƯU Ý: bạn chỉ nên định nghĩa các quy định cho các trường
        // mà người dùng sẽ thể nhập liệu    
        return array(
            array('ten, image, chuc_vu, noidung, chitiet, url, trang_thai, in_trash, created_at, han_dat', 'required'),
            array('trang_thai, in_trash, created_at, update_at, han_dat', 'numerical', 'integerOnly'=>true),
            array('ten', 'length', 'max' => 200),
            array('image, chuc_vu', 'length', 'max' => 300),
            array('noidung', 'length', 'max' => 2000),
            array('url', 'length', 'max' => 100),
            array('khung_gio, lich_kham', 'safe'),
            // Những thuộc tính dưới đây để phục vụ cho hàm search().            
            // @todo Hãy xóa đi những trường mà bạn không muốn cho người dùng tìm kiếm
            array('id, ten, image, chuc_vu, noidung, chitiet, url, khung_gio, lich_kham, trang_thai, in_trash, created_at, update_at, han_dat', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array mối quan hệ với các bảng khác.
     */
    public function relations()
    {
        // LƯU Ý: Bạn có thể cần phải điều chỉnh tên các quan hệ 
        // và các lớp liên quan được tự động sinh ra bên dưới đây        
        $rtn = parent::relations();
        return $rtn;
    }

    /**
     * @return array Tùy chỉnh tên các thuộc tính (name=>label)
     */
    public function attributeLabels()
    {
        $rtn = parent::attributeLabels();
        $rtn['id'] = 'ID';
        $rtn['ten'] = 'Ten';
        $rtn['image'] = 'Image';
        $rtn['chuc_vu'] = 'Chuc Vu';
        $rtn['noidung'] = 'Noidung';
        $rtn['chitiet'] = 'Chitiet';
        $rtn['url'] = 'Url';
        $rtn['khung_gio'] = 'Khung Gio';
        $rtn['lich_kham'] = 'Lich Kham';
        $rtn['trang_thai'] = 'Trang Thai';
        $rtn['in_trash'] = 'In Trash';
        $rtn['created_at'] = 'Created At';
        $rtn['update_at'] = 'Update At';
        $rtn['han_dat'] = 'Han Dat';
        return $rtn;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TbBacsi the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}