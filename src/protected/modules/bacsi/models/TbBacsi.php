<?php

/**
 * TbBacsi là lớp chính của bảng "tb_bacsi".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 05/12/2021 04:19:28
 * @version 1.0
 *
 */
class TbBacsi extends TbBacsiBase
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Danh sách các cấu hình của model
     * @return array
     */
    public static function configArray()
    {
        $cnf = parent::configArray();
        return $cnf;
    }

    /**
     * Hàm gọi cấu hình theo model
     * @param string $item Tên của cấu hình
     * @return mix Giá trị của cấu hình tùy theo từng kiểu cấu hình mà có kiểu khác nhau
     */
    public static function getConf($item)
    {
        $option = Setting::getItem($item, __CLASS__);
        return $option ? $option : self::getDefaultConfig($item);
    }
    public static function SearchBS()
    {
        $timkiem = new CDbCriteria();
        $timkiem->addSearchCondition('ten', $_REQUEST['tukhoa']);
        $timkiem->compare('trang_thai', 1);
        $timkiem->compare('in_trash', 0);
        $bs = TbBacsi::model()->findAll($timkiem);
        return $bs;
    }
    public static function SearchAdminBS()
    {
        $timkiem = new CDbCriteria();
        $timkiem->addSearchCondition('ten', $_REQUEST['tukhoa']);
        $bs = TbBacsi::model()->findAll($timkiem);
        return $bs;
    }
    public static function Sort()
    {
        $sort = new CDbCriteria();
        $sort -> order = 'uu_tien';
        $sort -> compare('trang_thai', 1);
        $sort -> compare('in_trash', 0);
        $bs  = TbBacsi::model()->findAll($sort);
        return $bs;
    }
}