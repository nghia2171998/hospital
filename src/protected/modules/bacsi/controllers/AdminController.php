<?php

/**
 * Admin Controller of bacsi module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 31/10/2021 10:22:38
 * @version 1.0
 */
class AdminController extends BackController {

    public $modelName = 'Bacsi';

    function init() {
        parent::init();
        $this->pageTitle = $this->modelName;
    }

    function getFormAttr() {
        return array(
            '' => array(
                array(
                    'field' => 'name'
                ),
                array(
                    'field' => 'status',
                    'type' => 'select'
                )
            ),
        );
    }

    /**
     * Danh sách các trường dùng cho danh sách
     * @return array
     */
    public function getListAttr() {
        return array(
            'name',
            'status'
        );
    }

    /**
     * Danh sách các trường dùng cho lọc
     * @return array
     */
    public function getFilterAttr() {
        return array(
            array(
                'field' => 'name',
                'type' => 'search'
            ),
            array(
                'field' => 'status',
                'type' => 'select'
            ),
        );
    }

    public function getAttrData() {
        $rtn = parent::getAttrData();
        return $rtn;
    }

    function getListAttr1() {
        return array(
            array(
                'field' => 'ten',
                'name' => 'Tên bác sĩ',
                'type' => 'text'
            ),
            array(
                'field' => 'image',
                'name' => 'Ảnh',
                'type' => 'img',
            ),
            array(
                'field' => 'trang_thai',
                'name' => 'Trạng thái',
                'type' => 'text'
            ),
        );
    }

    function getFormAttr1() {
        return array(
            array(
                'field' => 'ten',
                'name' => 'Tên bác sĩ',
                'type' => 'text',
                'value' => ''
            ),
            array(
                'field' => 'chuc_vu',
                'name' => 'Chuyên khoa',
                'type' => 'select2',
                'value' => '',
                'data' => TbChuyenkhoa::Getchuyenkhoa(),
            ),
            array(
                'field' => 'anh',
                'name' => 'Ảnh',
                'type' => 'img',
                'value' => '',
            ),
            array(
                'field' => 'title',
                'name' => "Nội dung",
                'type' => 'textarea',
                'value' => ''
            ),
            array(
                'field' => 'tomtat',
                'name' => 'Chi tiết',
                'type' => 'ckeditor',
                'value' => '',
            ),
            array(
                'field' => 'uu_tien',
                'name' => "Thứ tự ưu tiên",
                'type' => 'text',
                'value' => ''
            ),
            array(
                'field' => 'trang_thai',
                'name' => 'trạng thái',
                'type' => 'radio',
                'value' => ''
            ),
        );
    }

    function getTitle() {
        return array(
            'Title' => 'Bác sĩ',
            'addbutton' => '1',
        );
    }

    function actionGetList() {
        $title = 'CK';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $offset = ($page - 1) * $limit;
        $in_trash = $_GET['in_trash'];
        $bs = new CDbCriteria();
        $bs->order = 'id DESC';
        $bs->compare('in_trash', $in_trash);
        if (isset($_GET['select']) != '') {
            $bs->compare('trang_thai', $_GET['select']);
        }
        if (isset($_GET['tukhoa'])) {
            $bs->addSearchCondition('ten', $_GET['tukhoa']);
        }
        $bs->limit = $limit;
        $bs->offset = $offset;
        $bacsi = TbBacsi::model()->findAll($bs);
        $numRec = TbBacsi::model()->count($bs);
        $numPage = ($numRec % $limit) ? ($numRec / $limit + 1) : $numRec / $limit;
        $rtn = array();
        foreach ($bacsi as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'ten' => $item->ten,
                'image' => $item->image,
                'danhmuc' => $item->chuc_vu,
                'title' => $item->noidung,
                'tomtat' => $item->chitiet,
                'trang_thai' => $item->trang_thai
            );
        }
        $value = array('data' => $rtn, 'page' => $page, 'offset' => $offset, 'numPage' => $numPage, 'total' => $numRec);
        $this->renderJson("200", "thanh cong", $value);
    }

    function actionAddList() {
        $bs = new TbBacsi();
        $bs->ten = $_POST['ten'];
        $bs->image = $_POST['anh'];
        $bs->noidung = $_POST['title'];
        $bs->chitiet = $_POST['tomtat'];
        $bs->url = MyUtil::to_slug($_POST['ten']);
        $bs->trang_thai = $_POST['rd1'];
        $bs->chuc_vu = $_POST['danhmuc'];
        $bs->uu_tien = $_POST['uu_tien'];
        $bs->in_trash = 0;
        $bs->created_at = time();
        $bs->han_dat=15;
        $bs->khung_gio = '[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"],["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"],["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"],["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"],["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"],["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"],["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"]]';
        $bs->save();
    }

    function actionUpList() {
        $id = $_POST['hiddenId'];
        $bs = TbBacsi::model()->findByPk($id);
        $bs->ten = $_POST['ten'];
        $bs->image = $_POST['anh'];
        $bs->noidung = $_POST['title'];
        $bs->chitiet = $_POST['tomtat'];
        $bs->url = MyUtil::to_slug($_POST['ten']);
        $bs->uu_tien = $_POST['uu_tien'];
        $bs->trang_thai = $_POST['rd1'];
        $bs->chuc_vu = $_POST['danhmuc'];
        $bs->update_at = time();
        $bs->save();
    }

    function actionHidden() {
        $id = $_POST['id'];
        $bs = TbBacsi::model()->findByPk($id);
        $bs->in_trash = 1;
        $bs->update_at = time();
        $bs->save();
    }

    function actionUnHidden() {
        $id = $_POST['id'];
        $bs = TbBacsi::model()->findByPk($id);
        $bs->in_trash = 0;
        $bs->update_at = time();
        $bs->save();
    }

    function actiongetOneList() {
        $id = $_GET['id'];
        $data = TbBacsi::model()->findByPk($id);
        $rtn = array();
        $rtn['id'] = $data->id;
        $rtn['anh'] = $data->image;
        $rtn['ten'] = $data->ten;
        $rtn['danhmuc'] = json_decode($data->chuc_vu);
        $rtn['title'] = $data->noidung;
        $rtn['tomtat'] = $data->chitiet;
        $rtn['uu_tien'] = $data->uu_tien;
        $rtn['trang_thai'] = $data->trang_thai;

        $this->renderJson("200", "thanh cong", $rtn);
    }

    function actionsettime() {
        $time = TbTime::model()->findAll();
        $bs = new CDbCriteria();
        $bs->order = 'id DESC';
        $bs->compare('trang_thai', 1);
        $count = TbBacsi::model()->Count($bs);
        $pages = new CPagination($count);
        $pages->pageSize = 5;
        $pages->applyLimit($bs);
        $models = TbBacsi::model()->findAll($bs);
        $this->render('settime', array(
            'bs' => $models,
            'pages' => $pages,
            'count' => $count,
            'time' => $time
        ));
    }

    function actionSearch1() {
        $bs = TbBacsi::SearchAdminBS();
        $time = TbTime::model()->findAll();
        $this->render('settime', array('bs' => $bs, 'time' => $time));
    }

    function actionSearch() {
        $bs = TbBacsi::SearchAdminBS();
        $ck = TbChuyenkhoa::model()->findAll();
        $this->render('index', array('bs' => $bs, 'ck' => $ck));
    }

    function actionDelete($id) {
        $bs = TbBacsi::model()->findByPk($id);
        $bs->delete();
        return $this->redirect(['index']);
    }

    function actionSet($id) {
        $bs = TbBacsi::model()->findByPk($id);
        $lich = json_decode($bs['lich_kham']);
        $a = array();
        $b = array();
        $kq = 1;
        for ($i = 0; $i <= 6; $i++) {

            if (isset($_REQUEST['s' . $i])) {
                $b[$i] = array_diff($lich[$i], $_REQUEST['s' . $i]);
                if (count($b[$i]) == 0) {
                    $a[$i] = $_REQUEST['s' . $i];
                } else {
                    $kq = 0;
                    break;
                }
            } else {
                if (count($lich[$i]) == 0) {
                    $a[$i] = [];
                } else {
                    $kq = 0;
                    break;
                }
            }
        }
        if ($kq == 1) {
            $bs->khung_gio = json_encode($a);
            $bs->save();
            return $this->redirect(['settime']);
        } else {
            echo"<script>"
            . "alert('Update không thành công do có khung giờ đã có hẹn khám - xin hãy xem lại lịch khám');"
            . "window.location.replace('http://localhost/hospital/bacsi/admin/settime');"
            . "</script>";
        }
    }

}