<?php

/**
 * Admin Controller of bacsi module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 31/10/2021 10:22:38
 * @version 1.0
 */
class DefaultController extends FrontController {

    function init() {
        parent::init();
        $this->pageTitle = '';
    }

    function actionIndex($id) {
        $bs = TbBacsi::model()->findByPk($id);
        $this->render("index", array('bs' => $bs));
    }

    function actionBacsi() {
        $bs = new CDbCriteria();
        $bs->compare('trang_thai', 1);
        $bs->compare('in_trash', 0);
        $bs->order = 'uu_tien';
        $count = TbBacsi::model()->count($bs);
        $pages = new CPagination($count);
        $pages->pageSize = 5;
        $pages->applyLimit($bs);
        $models = TbBacsi::model()->findAll($bs);

        $this->render('bacsi', array(
            'bs' => $models,
            'pages' => $pages,
            'count' => $count
        ));
    }

    function actionDatlich($id) {
        $bs = TbBacsi::model()->findByPk($id);
        $chuyenkhoa = json_decode($bs['chuc_vu']);
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $ddate = explode("-", $_POST['id_date']);
        $ngaykham = strtotime($ddate[1] . "/" . $ddate[2] . "/" . $ddate[0]);
        $lich = new TbLich();
        $lich->ten_kh = $_POST['ten'];
        $lich->sdt = $_POST['sdt'];
        $lich->dia_chi = $_POST['dc'];
        $lich->tuoi = $_POST['age'];
        $lich->time = $_POST['time'];
        $lich->weekday = $ddate[3];
        $lich->lydo = $_POST['lydo'];
        $lich->ngay_dat = time();
        $lich->ngay_kham = $ngaykham;
        $lich->time_slot = $_POST['time_slot'];
        $lich->hour = $_POST['hour'];
        $lich->minute = $_POST['minute'];
        $lich->deadtime = $_POST['deadtime'];
        $lich->bac_si = $id;
        $lich->chuyenkhoa = $chuyenkhoa[0];
        $lich->trang_thai = 0;
        $lich->in_trash = 0;
        $lich->created_at = time();
        $lich->save();
        return $this->redirect($this->createUrl('default/' . $id));
    }

    function actiongettime() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $ddate = $_GET['date'];
        $duedt = explode("-", $ddate);
        $nk = strtotime($duedt[1] . '/' . $duedt[2] . '/' . $duedt[0]);
        $s = ['s', 'c', 't', 'd'];
        $rtn = array();
        foreach ($s as $s) {
            $l = new CDbCriteria();
            $l->compare('id_bs', $_GET['id']);
            $l->compare('weekday', (int) $duedt[3]);
            $l->compare('session_day', $s);
            $l->compare('day', (int) $duedt[2]);
            $l->compare('month', (int) $duedt[1]);
            $l->compare('year', (int) $duedt[0]);
            $l->compare('trang_thai', 0);
            $count = TbSettime::model()->count($l);
            if ($count >= 1) {
                $lich = TbSettime::model()->findAll($l);
            } else {
                $k = new CDbCriteria();
                $k->compare('id_bs', $_GET['id']);
                $k->compare('weekday', $duedt[3]);
                $k->compare('black_day', 0);
                $k->compare('session_day', $s);
                $k->compare('trang_thai', 0);
                $lich = TbSettime::model()->findAll($k);
            }
            foreach ($lich as $item) {
                if ($duedt[3] >= $item['han_thu']) {
                    $day = $duedt[3] - $item['han_thu'];
                } elseif ($duedt[3] < $item['han_thu']) {
                    $day = 7 - $item['han_thu'] + $duedt[3];
                }
                $newdate = date('m/d/Y', strtotime('-' . $day . ' day', $nk));
                $time = strtotime($item['han_gio'] . ":" . $item['han_phut'] . ":00 " . $newdate);
                $t = new CDbCriteria();
                $t->compare('bac_si', $_GET['id']);
                $t->compare('weekday', $duedt[3]);
                $t->compare('ngay_kham', $nk);
                $t->addSearchCondition('time_slot', $item['name']);
                $lk = TbLich::model()->findAll($t);
                if (count($lk) == 0 && strtotime("now") < $time) {
                    $rtn[] = array(
                        'id' => $item->id,
                        'hour' => $item->hour,
                        'minute' => $item->minute,
                        'time' => $item->time,
                        'deadtime' => $item->timedate,
                        'name' => $item->name,
                    );
                }
            }
        }
        $this->renderJson("200", "thanh cong", $rtn);
    }

}
