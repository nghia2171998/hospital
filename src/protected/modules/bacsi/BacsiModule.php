<?php
/**
 * bacsi module
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 31/10/2021 10:22:38
 * @version 1.0
 */
class BacsiModule extends MyModule
{

    public function init()
    {        
        /**
        * $this->setImport(array(
        *    'bacsi.models.*',
        *    'bacsi.components.*',
        * ));
        */
    }
    
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {     
            return true;
        }
        else
        {
            return false;
        }    
    }

}
