<?php
/**
 * trangtinh module
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 18/10/2021 11:19:12
 * @version 1.0
 */
class TrangtinhModule extends MyModule
{

    public function init()
    {        
        /**
        * $this->setImport(array(
        *    'trangtinh.models.*',
        *    'trangtinh.components.*',
        * ));
        */
    }
    
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {     
            return true;
        }
        else
        {
            return false;
        }    
    }

}
