<?php

/**
 * Admin Controller of trangtinh module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 18/10/2021 11:19:12
 * @version 1.0
 */
class AdminController extends BackController {

    public $modelName = 'Trangtinh';

    function init() {
        parent::init();
        $this->pageTitle = $this->modelName;
    }

    function getFormAttr() {
        return array(
            '' => array(
                array(
                    'field' => 'name'
                ),
                array(
                    'field' => 'status',
                    'type' => 'select'
                )
            ),
        );
    }

    /**
     * Danh sách các trường dùng cho danh sách
     * @return array
     */
    public function getListAttr() {
        return array(
            'name',
            'status'
        );
    }

    /**
     * Danh sách các trường dùng cho lọc
     * @return array
     */
    public function getFilterAttr() {
        return array(
            array(
                'field' => 'name',
                'type' => 'search'
            ),
            array(
                'field' => 'status',
                'type' => 'select'
            ),
        );
    }

    public function getAttrData() {
        $rtn = parent::getAttrData();
        return $rtn;
    }

//    function actionIndex() {
//        $this->render('index');
//    }
    function actionDelete($id) {
        $nd = noidung::model()->findByPk($id);
        $nd->delete();
        return $this->redirect(['index']);
    }

    function actionGetList() {
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $offset = ($page - 1) * $limit;
        $in_trash = $_GET['in_trash'];

        $cn = new CDbCriteria();
        $cn->order = 'id DESC';
        $cn->compare('in_trash', $in_trash);
        if (isset($_GET['select']) != '') {
            $cn->compare('trang_thai', $_GET['select']);
        }
        if (isset($_GET['tukhoa'])) {
            $cn->addSearchCondition('name', $_GET['tukhoa']);
        }
        $cn->limit = $limit;
        $cn->offset = $offset;
        $danhmuc = noidung::model()->findAll($cn);
        $numRec = noidung::model()->count($cn);
        $numPage = ($numRec % $limit) ? ($numRec / $limit + 1) : $numRec / $limit;
        $rtn = array();
        foreach ($danhmuc as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'name' => $item->name,
                'trang_thai' => $item->trang_thai
            );
        }
        $value = array('data' => $rtn, 'page' => $page, 'offset' => $offset, 'numPage' => $numPage, 'total' => $numRec);
        $this->renderJson("200", "thanh cong", $value);
    }

    function actionAddList() {
        $nd = new noidung();
        $nd->name = $_POST['name'];
        $nd->title = $_POST['title'];
        $nd->url = MyUtil::to_slug($_POST['name']);
        $nd->trang_thai = $_POST['rd1'];
        $nd->in_trash = 0;
        $nd->created_at = time();
        $nd->save();
    }

    function actiongetOneList() {
        $id = $_GET['id'];
        $data = TrangTinh::model()->findByPk($id);
        $rtn = array();
        $rtn['id'] = $data->id;
        $rtn['title'] = $data->title;
        $rtn['name'] = $data->name;
        $rtn['trang_thai'] = $data->trang_thai;

        $this->renderJson("200", "thanh cong", $rtn);
    }

    function actionUpList() {
        $id = $_POST['hiddenId'];
        $nd = noidung::model()->findByPk($id);
        $nd->name = $_POST['name'];
        $nd->title = $_POST['title'];
        $nd->url = MyUtil::to_slug($_POST['name']);
        $nd->trang_thai = $_POST['rd1'];
        $nd->update_at = time();
        $nd->save();
    }

    function actionHidden() {
        $id = $_POST['id'];
        $nd = noidung::model()->findByPk($id);
        $nd->in_trash = 1;
        $nd->update_at = time();
        $nd->save();
    }

    function actionunHidden() {
        $id = $_POST['id'];
        $nd = noidung::model()->findByPk($id);
        $nd->in_trash = 0;
        $nd->update_at = time();
        $nd->save();
    }

    function actionsearchList() {
        $timkiem = new CDbCriteria();
        $timkiem->addSearchCondition('name', $_GET['tukhoa']);
        $in_trash = $_GET['in_trash'];
        $timkiem->compare('in_trash', $in_trash);
        $models = TrangTinh::model()->findAll($timkiem);
        $rtn = array();
        foreach ($models as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'name' => $item->name,
                'title' => $item->title,
                'trang_thai' => $item->trang_thai
            );
        }
        $this->renderJson("200", "thanh cong", $rtn);
    }

    function getListAttr1() {
        return array(
            array(
                'field' => 'name',
                'name' => 'Tên',
                'type' => 'text'
            ),
            array(
                'field' => 'trang_thai',
                'name' => 'Trạng thái',
                'type' => 'text'
            )
        );
    }

    function getFormAttr1() {
        return array(
          array(
              'field'=>'name',
              'name'=>'Tên',
              'type'=>'text',
              'value'=>''
          ),
            array(
                'field'=>'title',
                'name'=>"Nội dung",
                'type'=>'textarea',
                'value'=>''
            ),
            array(
              'field'=>'trang_thai',
                'name'=>'trạng thái',
                'type'=>'radio',
                'value'=>''
            ),
        );
    }
    function getTitle(){
        return array(
            'Title'=>'Trang tĩnh',
            'addbutton'=>'1',
        );
    }
}
