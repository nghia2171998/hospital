<?php

/**
 * Admin Controller of trangtinh module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 18/10/2021 11:19:12
 * @version 1.0
 */
class DefaultController extends FrontController {

    function init() {
        parent::init();
        $this->pageTitle = '';
    }

    function actionIndex() {
        $this->render("index");
    }

    function actionGetList() {
        $id = $_GET['id'];
        $data = TrangTinh::model()->findByPk($id);
        $rtn = array();
        $rtn['id'] = $data->id;
        $rtn['title'] = $data->title;
        $rtn['name'] = $data->name;

        $this->renderJson("200", "thnh cong", $rtn);
    }

}