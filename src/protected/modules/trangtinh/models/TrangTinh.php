<?php

/**
 * TrangTinh là lớp chính của bảng "trang_tinh".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 18/10/2021 11:19:47
 * @version 1.0
 *
 */
class TrangTinh extends TrangTinhBase
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Danh sách các cấu hình của model
     * @return array
     */
    public static function configArray()
    {
        $cnf = parent::configArray();
        return $cnf;
    }

    /**
     * Hàm gọi cấu hình theo model
     * @param string $item Tên của cấu hình
     * @return mix Giá trị của cấu hình tùy theo từng kiểu cấu hình mà có kiểu khác nhau
     */
    public static function getConf($item)
    {
        $option = Setting::getItem($item, __CLASS__);
        return $option ? $option : self::getDefaultConfig($item);
    }
    public static function SearchAdminTT()
    {
        $timkiem = new CDbCriteria();
        $timkiem->addSearchCondition('name', $_REQUEST['tukhoa1']);
        $tt = TrangTinh::model()->findAll($timkiem);
        return $tt;
    }
    
}