<?php
class noidung extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
 
    public function tableName()
    {
        return 'trang_tinh';
    }
    public static function Sort()
    {
        $sort = new CDbCriteria();
        $sort -> order = 'id DESC';
        $tt  = TrangTinh::model()->findAll($sort);
        return $tt;
    }
}