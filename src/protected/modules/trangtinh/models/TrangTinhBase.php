<?php

/**
 * Đây là lớp model của bảng "trang_tinh".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 28/12/2021 09:26:38
 * @version 1.0
 *
 * Dưới đây là các cột của bảng 'trang_tinh':
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $url
 * @property integer $trang_thai
 * @property integer $in_trash
 * @property integer $created_at
 * @property integer $update_at
 */
class TrangTinhBase extends MyModel
{

    public $modelName = 'TrangTinh';

    /**
     * @return string trả về tên của bảng
     */
    public function tableName()
    {
        return 'trang_tinh';
    }

    /**
     * @return array Các quy định về thuộc tính của lớp.
     */
    public function rules()
    {
        // LƯU Ý: bạn chỉ nên định nghĩa các quy định cho các trường
        // mà người dùng sẽ thể nhập liệu    
        return array(
            array('name, url, trang_thai, in_trash, created_at', 'required'),
            array('trang_thai, in_trash, created_at, update_at', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max' => 100),
            array('url', 'length', 'max' => 200),
            array('title', 'safe'),
            // Những thuộc tính dưới đây để phục vụ cho hàm search().            
            // @todo Hãy xóa đi những trường mà bạn không muốn cho người dùng tìm kiếm
            array('id, name, title, url, trang_thai, in_trash, created_at, update_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array mối quan hệ với các bảng khác.
     */
    public function relations()
    {
        // LƯU Ý: Bạn có thể cần phải điều chỉnh tên các quan hệ 
        // và các lớp liên quan được tự động sinh ra bên dưới đây        
        $rtn = parent::relations();
        return $rtn;
    }

    /**
     * @return array Tùy chỉnh tên các thuộc tính (name=>label)
     */
    public function attributeLabels()
    {
        $rtn = parent::attributeLabels();
        $rtn['id'] = 'ID';
        $rtn['name'] = 'Name';
        $rtn['title'] = 'Title';
        $rtn['url'] = 'Url';
        $rtn['trang_thai'] = 'Trang Thai';
        $rtn['in_trash'] = 'In Trash';
        $rtn['created_at'] = 'Created At';
        $rtn['update_at'] = 'Update At';
        return $rtn;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TrangTinh the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}