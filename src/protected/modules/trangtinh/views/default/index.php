<silde-show>
    <div class='khung'>
        <div class='detail' style='margin-top: 20px;' id='trangtinh'>
            <div>
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo Yii::app()->baseUrl ?>/">Trang chủ</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{allData.name}}</li>
                    </ol>
                </nav>
                <hr/>
                <h1 style='margin-top: 100px;'>{{allData.name}}</h1>
            </div>

            <div class='chi_tiet'>
                <p id='show' v-html='allData.title'></p>
            </div>
        </div>
    </div>
</silde-show>
<script>
    var application = new Vue({
        el: '#trangtinh',
        data: {
            allData: '',
            limit: 8,
        },
        methods: {
            fetchAllData: function () {
                $.ajax({
                    url: "<?php echo Yii::app()->createUrl('trangtinh/default/getlist') ?>",
//                   
                    type: 'GET',
                    data: {
                        id: <?php echo $_GET['id']?>,
                    },
                    success: function (res) {

//                    alert(res);
//            res.data.title =  removeHTML(res.data.title)
                        application.allData = res.data;
                        console.log(application.allData);
                    },

                });
            }
        },

//        created: function () {
//            this.fetchAllData();
//        }
    });
//    }
    application.fetchAllData();
</script>