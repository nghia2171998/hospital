<?php

/**
 * TbDanhmuc là lớp chính của bảng "tb_danhmuc".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 11/11/2021 05:55:09
 * @version 1.0
 *
 */
class TbDanhmuc extends TbDanhmucBase {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Danh sách các cấu hình của model
     * @return array
     */
    public static function configArray() {
        $cnf = parent::configArray();
        return $cnf;
    }

    /**
     * Hàm gọi cấu hình theo model
     * @param string $item Tên của cấu hình
     * @return mix Giá trị của cấu hình tùy theo từng kiểu cấu hình mà có kiểu khác nhau
     */
    public static function getConf($item) {
        $option = Setting::getItem($item, __CLASS__);
        return $option ? $option : self::getDefaultConfig($item);
    }

    public static function SearchAdminDM() {
        $timkiem = new CDbCriteria();
        $timkiem->addSearchCondition('name', $_REQUEST['tukhoa1']);
        $dm = TbDanhmuc::model()->findAll($timkiem);
        return $dm;
    }

    public static function Sort() {
        $sort = new CDbCriteria();
        $sort->order = 'id DESC';
        $dm = TbDanhmuc::model()->findAll($sort);
        return $dm;
    }

    static function Getdanhmuc() {
        $dm = new CDbCriteria();
        $dm->order='id DESC';
        $dm->compare('trang_thai', 1);
        $danhmuc= TbDanhmuc::model()->findAll($dm);
        $rtn=array();
        foreach($danhmuc as $item)
        {
            $rtn[]=array(
                'id' => $item->id,
                'text'=>$item->name,
            );
        }
        return $rtn;
    }

}
