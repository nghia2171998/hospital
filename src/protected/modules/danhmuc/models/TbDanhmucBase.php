<?php

/**
 * Đây là lớp model của bảng "tb_danhmuc".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 11/11/2021 05:55:09
 * @version 1.0
 *
 * Dưới đây là các cột của bảng 'tb_danhmuc':
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property integer $trang_thai
 */
class TbDanhmucBase extends MyModel
{

    public $modelName = 'TbDanhmuc';

    /**
     * @return string trả về tên của bảng
     */
    public function tableName()
    {
        return 'tb_danhmuc';
    }

    /**
     * @return array Các quy định về thuộc tính của lớp.
     */
    public function rules()
    {
        // LƯU Ý: bạn chỉ nên định nghĩa các quy định cho các trường
        // mà người dùng sẽ thể nhập liệu	
        return array(
            array('name, trang_thai', 'required'),
            array('trang_thai', 'numerical', 'integerOnly'=>true),
            array('name, url', 'length', 'max' => 500),
            // Những thuộc tính dưới đây để phục vụ cho hàm search().			
            // @todo Hãy xóa đi những trường mà bạn không muốn cho người dùng tìm kiếm
            array('id, name, url, trang_thai', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array mối quan hệ với các bảng khác.
     */
    public function relations()
    {
        // LƯU Ý: Bạn có thể cần phải điều chỉnh tên các quan hệ 
        // và các lớp liên quan được tự động sinh ra bên dưới đây		
        $rtn = parent::relations();
        return $rtn;
    }

    /**
     * @return array Tùy chỉnh tên các thuộc tính (name=>label)
     */
    public function attributeLabels()
    {
        $rtn = parent::attributeLabels();
        $rtn['id'] = 'ID';
        $rtn['name'] = 'Name';
        $rtn['url'] = 'Url';
        $rtn['trang_thai'] = 'Trang Thai';
        return $rtn;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TbDanhmuc the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
