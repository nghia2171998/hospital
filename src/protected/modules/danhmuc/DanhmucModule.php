<?php
/**
 * danhmuc module
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 11/11/2021 05:54:48
 * @version 1.0
 */
class DanhmucModule extends MyModule
{

    public function init()
    {        
        /**
        * $this->setImport(array(
        *    'danhmuc.models.*',
        *    'danhmuc.components.*',
        * ));
        */
    }
    
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {     
            return true;
        }
        else
        {
            return false;
        }    
    }

}
