<silde-show>
    <div class='khung'>
        <div class='detail' style='margin-top: 20px;'>
            <div>
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo Yii::app()->baseUrl ?>/">Trang chủ</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Cẩm nang</li>
                    </ol>
                </nav>
                <h1> Cẩm nang </h1>
                <?php
                if(isset($_REQUEST['id']))
                {
					$g=MyUtil::checkDM($_REQUEST['id']);
                    foreach($g as $i)
                    {
                        $cn1=TbBaiviet::model()->findByPk($i);
                        {
                            if($cn1['trang_thai']==1 && $cn1['in_trash']==0)
						    {
                                ?>
                <div class='article'>
                    <div class='article1'>
                        <a
                            href='<?php echo Yii::app()->createUrl('baiviet/default', array('id' => $cn1['id'], 'url' => $cn1['url'])) ?>'>
                            <img style="width=200px; height=120px"
                                src='<?php echo Yii::app()->baseUrl ?>/file/<?=$cn1['image']?>' />
                            <h4 style='font-size: medium; margin: 10px 0 10px 20px'><?=$cn1['ten']?></h4>
                        </a>
                    </div>
                </div>
                <?php
                            }
                        }
                    }
                }
                else{
                    foreach($cn as $bv)
					{
						if($bv['in_trash']==0){
					?>
                <div class='article'>
                    <div class='article1'>
                        <a
                            href='<?php echo Yii::app()->createUrl('baiviet/default', array('id' => $bv['id'], 'url' => $bv['url'])) ?>'>
                            <div class='article2' style="">
                                <img style='width:200px; height:120px;'
                                    src='<?php echo Yii::app()->baseUrl ?>/file/<?=$bv['image']?>' />
                            </div>
                            <h4 style='font-size: medium; margin: 10px 0 10px 20px'>
                                <?=$bv['ten']?>
                            </h4>
                        </a>
                        <hr />
                    </div>
                </div>
                <?php
                        }
                    }
                    ?>
                <div class='p_trang'>
                    <?php
        $config = [
            'total' => $count,
            'limit' => $pages->pageSize,
            'full' => false,
            'querystring' => 'page',
            'url'=>'danhmuc'
        ];
        
        $page = new Pagination($config);
        $h= ceil($count/$pages->pageSize);
        if($h>1)
        {
            echo $page->getPagination();
        }
        
    ?>
                </div>
                <?php
                }
                ?>

            </div>
        </div>
    </div>
    <div class="viewdm">
        <div class='htdm'>
            <h4>Danh Mục Cẩm Nang</h4>
            <ul>
                <?php foreach ($dm as $dm): ?>
                <li>
                    <a href="<?php echo Yii::app()->createUrl('danhmuc/default', array('id' => $dm['id'], 'url' => $dm['url']
                         )) ?>">
                        <?php echo $dm['name']?>
                    </a>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</silde-show>