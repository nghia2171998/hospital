<?php

/**
 * Admin Controller of danhmuc module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 11/11/2021 05:54:48
 * @version 1.0
 */
class AdminController extends BackController {

    public $modelName = 'Danhmuc';

    function init() {
        parent::init();
        $this->pageTitle = $this->modelName;
    }

    function getFormAttr() {
        return array(
            '' => array(
                array(
                    'field' => 'name'
                ),
                array(
                    'field' => 'status',
                    'type' => 'select'
                )
            ),
        );
    }

    /**
     * Danh sách các trường dùng cho danh sách
     * @return array
     */
    public function getListAttr() {
        return array(
            'name',
            'status'
        );
    }

    /**
     * Danh sách các trường dùng cho lọc
     * @return array
     */
    public function getFilterAttr() {
        return array(
            array(
                'field' => 'name',
                'type' => 'search'
            ),
            array(
                'field' => 'status',
                'type' => 'select'
            ),
        );
    }

    public function getAttrData() {
        $rtn = parent::getAttrData();
        return $rtn;
    }

    function actionSearch() {
        $dm = TbDanhmuc::SearchAdminDM();
        $this->render('index', array('dm' => $dm));
    }

    function actionAdd() {
        $dm = new TbDanhmuc();
        $dm->name = $_REQUEST['a1'];
        $dm->url = MyUtil::to_slug($_REQUEST['a1']);
        ;
        $dm->trang_thai = $_REQUEST['rd1'];
        $dm->save();
        return $this->redirect(['index']);
    }

    function actionUpdate($id) {
        $dm = TbDanhmuc::model()->findByPk($id);
        $dm->name = $_REQUEST['a1'];
        $dm->url = MyUtil::to_slug($_REQUEST['a1']);
        $dm->trang_thai = $_REQUEST['rd1'];
        $dm->save();
        return $this->redirect(['index']);
    }

    function actionDelete($id) {
        $a = MyUtil::KiemTraChuyenKhoa($id);
        $dm = TbDanhmuc::model()->findByPk($id);
        if ($a == 1) {
            $dm->trang_thai = 0;
            $dm->save();
            echo "<script>
                    alert('Không thể xoá chuyển sang trạng thái ẩn');
                    window.history.back();
                </script>";
        } else {
            $dm->delete();
            return $this->redirect(['index']);
        }
    }

    function actionGetList() {
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $offset = ($page - 1) * $limit;
        $in_trash = $_GET['in_trash'];

        $cn = new CDbCriteria();
        $cn->order = 'id DESC';
        $cn->compare('in_trash', $in_trash);
        if (isset($_GET['select']) != '') {
            $cn->compare('trang_thai', $_GET['select']);
        }
        if (isset($_GET['tukhoa'])) {
            $cn->addSearchCondition('name', $_GET['tukhoa']);
        }
        $cn->limit = $limit;
        $cn->offset = $offset;
        $danhmuc = TbDanhmuc::model()->findAll($cn);
        $numRec = TbDanhmuc::model()->count($cn);
        $numPage = ($numRec % $limit) ? ($numRec / $limit + 1) : $numRec / $limit;
        $rtn = array();
        foreach ($danhmuc as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'name' => $item->name,
                'trang_thai' => $item->trang_thai
            );
        }
        $value = array('data' => $rtn, 'page' => $page, 'offset' => $offset, 'numPage' => $numPage, 'total' => $numRec);
        $this->renderJson("200", "thanh cong", $value);
    }

    function actionGetOneList() {
        $id = $_GET['id'];
        $data = TbDanhmuc::model()->findByPk($id);
        $rtn = array();
        $rtn['id'] = $data->id;
        $rtn['name'] = $data->name;
        $rtn['trang_thai'] = $data->trang_thai;

        $this->renderJson("200", "thanh cong", $rtn);
    }

    function actionAddList() {
        $cn = new TbDanhmuc();
        $cn->name = $_POST['name'];
        $cn->trang_thai = $_POST['rd1'];
        $cn->url = MyUtil::to_slug($_POST['name']);
        $cn->in_trash = 0;
        $cn->created_at = time();
        $cn->save();
    }

    function actionUpList() {
        $id = $_POST['hiddenId'];
        $cn = TbDanhmuc::model()->findByPk($id);
        $cn->name = $_POST['name'];
        $cn->trang_thai = $_POST['rd1'];
        $cn->url = MyUtil::to_slug($_POST['name']);
        $cn->update_at = time();
        $cn->save();
    }

    function actionHidden() {
        $id = $_POST['id'];
        $yk = TbDanhmuc::model()->findByPk($id);
        $yk->in_trash = 1;
        $yk->update_at = time();
        $yk->save();
    }

    function actionUnHidden() {
        $id = $_POST['id'];
        $yk = TbDanhmuc::model()->findByPk($id);
        $yk->in_trash = 0;
        $yk->update_at = time();
        $yk->save();
    }

    function actionGetList1(){
        $dm = new CDbCriteria();
        $dm->order='id DESC';
        $dm->compare('trang_thai', 1);
        $danhmuc= TbDanhmuc::model()->findAll($dm);
        $rtn=array();
        foreach($danhmuc as $item)
        {
            $rtn[]=array(
                'id' => $item->id,
                'text'=>$item->name,
            );
        }
        $this->renderJson("200", "thanh cong", $rtn);
   }
   
       function getListAttr1() {
        return array(
            array(
                'field' => 'name',
                'name' => 'Tên',
                'type' => 'text'
            ),
            array(
                'field' => 'trang_thai',
                'name' => 'Trạng thái',
                'type' => 'text'
            )
        );
    }

    function getFormAttr1() {
        return array(
          array(
              'field'=>'name',
              'name'=>'Tên',
              'type'=>'text',
              'value'=>''
          ),
            array(
              'field'=>'trang_thai',
                'name'=>'trạng thái',
                'type'=>'radio',
                'value'=>''
            ),
        );
    }
    function getTitle(){
        return array(
            'Title'=>'Danh mục',
            'addbutton'=>'1',
        );
    }
}