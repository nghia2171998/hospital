<?php
/**
 * Admin Controller of danhmuc module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 11/11/2021 05:54:48
 * @version 1.0
 */
class DefaultController extends FrontController 
{
    
    function init()
    {
        parent::init();
        $this->pageTitle = '';
    }

    function actionIndex()
    {
        $dm = TbDanhmuc::model()->findAllBySql('SELECT * FROM tb_danhmuc WHERE trang_thai=1');
        $cn=new CDbCriteria();
        $cn->compare('trang_thai', 1);
        $count=TbBaiviet::model()->count($cn);
        $pages=new CPagination($count);
        $pages->pageSize=5;
        $pages->applyLimit($cn);
        $models=TbBaiviet::model()->findAll($cn);

        $this->render('index', array(
        'cn' => $models,
            'pages' => $pages,
            'count'=>$count,
            'dm'=>$dm
    ));
    }
    
}