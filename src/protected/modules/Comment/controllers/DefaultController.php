<?php
/**
 * Admin Controller of Comment module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 10/03/2022 16:40:23
 * @version 1.0
 */
class DefaultController extends FrontController 
{
    
    function init()
    {
        parent::init();
        $this->pageTitle = '';
    }

    function actionIndex()
    {
        $this->render("index");
    }
    
}