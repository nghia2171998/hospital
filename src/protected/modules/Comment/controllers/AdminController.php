<?php
/**
 * Admin Controller of Comment module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 10/03/2022 16:40:23
 * @version 1.0
 */
class AdminController extends BackController 
{

    public $modelName = 'Comment';
    
    function init()
    {
        parent::init();
        $this->pageTitle = $this->modelName;
    }

    function getFormAttr()
    {
        return array(
            '' => array(
                array(
                    'field' => 'name'
                ),
                array(
                    'field' => 'status',
                    'type' => 'select'
                )
            ),
        );
    }

    /**
     * Danh sách các trường dùng cho danh sách
     * @return array
     */
    public function getListAttr()
    {
        return array(
            'name',
            'status'
        );
    }

    /**
     * Danh sách các trường dùng cho lọc
     * @return array
     */
    public function getFilterAttr()
    {
        return array(
            array(
                'field' => 'name',
                'type' => 'search'
            ),
            array(
                'field' => 'status',
                'type' => 'select'
            ),
        );
    }

    public function getAttrData()
    {
        $rtn = parent::getAttrData();
        return $rtn;
    }
}