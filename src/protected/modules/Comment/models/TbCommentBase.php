<?php

/**
 * Đây là lớp model của bảng "tb_comment".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 10/03/2022 16:56:46
 * @version 1.0
 *
 * Dưới đây là các cột của bảng 'tb_comment':
 * @property integer $id
 * @property integer $id_schedule
 * @property string $name
 * @property string $content
 * @property integer $create_at
 */
class TbCommentBase extends MyModel
{

    public $modelName = 'TbComment';

    /**
     * @return string trả về tên của bảng
     */
    public function tableName()
    {
        return 'tb_comment';
    }

    /**
     * @return array Các quy định về thuộc tính của lớp.
     */
    public function rules()
    {
        // LƯU Ý: bạn chỉ nên định nghĩa các quy định cho các trường
        // mà người dùng sẽ thể nhập liệu    
        return array(
            array('id_schedule, name, content, create_at', 'required'),
            array('id_schedule, create_at', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max' => 50),
            // Những thuộc tính dưới đây để phục vụ cho hàm search().            
            // @todo Hãy xóa đi những trường mà bạn không muốn cho người dùng tìm kiếm
            array('id, id_schedule, name, content, create_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array mối quan hệ với các bảng khác.
     */
    public function relations()
    {
        // LƯU Ý: Bạn có thể cần phải điều chỉnh tên các quan hệ 
        // và các lớp liên quan được tự động sinh ra bên dưới đây        
        $rtn = parent::relations();
        return $rtn;
    }

    /**
     * @return array Tùy chỉnh tên các thuộc tính (name=>label)
     */
    public function attributeLabels()
    {
        $rtn = parent::attributeLabels();
        $rtn['id'] = 'ID';
        $rtn['id_schedule'] = 'Id Schedule';
        $rtn['name'] = 'Name';
        $rtn['content'] = 'Content';
        $rtn['create_at'] = 'Create At';
        return $rtn;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TbComment the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}