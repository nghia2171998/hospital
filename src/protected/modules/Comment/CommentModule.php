<?php
/**
 * Comment module
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 10/03/2022 16:40:23
 * @version 1.0
 */
class CommentModule extends MyModule
{

    public function init()
    {        
        /**
        * $this->setImport(array(
        *    'Comment.models.*',
        *    'Comment.components.*',
        * ));
        */
    }
    
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {     
            return true;
        }
        else
        {
            return false;
        }    
    }

}
