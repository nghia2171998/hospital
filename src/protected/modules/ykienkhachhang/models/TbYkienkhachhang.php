<?php

/**
 * TbYkienkhachhang là lớp chính của bảng "tb_ykienkhachhang".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 10/11/2021 08:24:18
 * @version 1.0
 *
 */
class TbYkienkhachhang extends TbYkienkhachhangBase
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Danh sách các cấu hình của model
     * @return array
     */
    public static function configArray()
    {
        $cnf = parent::configArray();
        return $cnf;
    }

    /**
     * Hàm gọi cấu hình theo model
     * @param string $item Tên của cấu hình
     * @return mix Giá trị của cấu hình tùy theo từng kiểu cấu hình mà có kiểu khác nhau
     */
    public static function getConf($item)
    {
        $option = Setting::getItem($item, __CLASS__);
        return $option ? $option : self::getDefaultConfig($item);
    }
    public static function VYKien()
    {
        $cond = new CDbCriteria();
        $cond->compare('trang_thai', 1);
        $cond->order = 'uu_tien';
        $yk = TbYkienkhachhang::model()->findAll($cond);
        return $yk;
    }   
    public static function Sort()
    {
        $sort = new CDbCriteria();
        $sort -> order = 'id DESC';
        $yk = TbYkienkhachhang::model()->findAll($sort);
        return $yk;
    }
}