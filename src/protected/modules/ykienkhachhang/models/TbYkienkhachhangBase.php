<?php

/**
 * Đây là lớp model của bảng "tb_ykienkhachhang".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 11/01/2022 08:06:13
 * @version 1.0
 *
 * Dưới đây là các cột của bảng 'tb_ykienkhachhang':
 * @property integer $id
 * @property string $ten
 * @property string $img
 * @property string $img_kq
 * @property string $y_kien
 * @property string $date
 * @property integer $trang_thai
 * @property integer $uu_tien
 * @property integer $in_trash
 * @property integer $created_at
 * @property integer $update_at
 */
class TbYkienkhachhangBase extends MyModel
{

    public $modelName = 'TbYkienkhachhang';

    /**
     * @return string trả về tên của bảng
     */
    public function tableName()
    {
        return 'tb_ykienkhachhang';
    }

    /**
     * @return array Các quy định về thuộc tính của lớp.
     */
    public function rules()
    {
        // LƯU Ý: bạn chỉ nên định nghĩa các quy định cho các trường
        // mà người dùng sẽ thể nhập liệu    
        return array(
            array('ten, img, img_kq, y_kien, date, trang_thai, uu_tien, in_trash, created_at', 'required'),
            array('trang_thai, uu_tien, in_trash, created_at, update_at', 'numerical', 'integerOnly'=>true),
            array('ten', 'length', 'max' => 100),
            array('img, img_kq', 'length', 'max' => 200),
            array('y_kien', 'length', 'max' => 500),
            // Những thuộc tính dưới đây để phục vụ cho hàm search().            
            // @todo Hãy xóa đi những trường mà bạn không muốn cho người dùng tìm kiếm
            array('id, ten, img, img_kq, y_kien, date, trang_thai, uu_tien, in_trash, created_at, update_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array mối quan hệ với các bảng khác.
     */
    public function relations()
    {
        // LƯU Ý: Bạn có thể cần phải điều chỉnh tên các quan hệ 
        // và các lớp liên quan được tự động sinh ra bên dưới đây        
        $rtn = parent::relations();
        return $rtn;
    }

    /**
     * @return array Tùy chỉnh tên các thuộc tính (name=>label)
     */
    public function attributeLabels()
    {
        $rtn = parent::attributeLabels();
        $rtn['id'] = 'ID';
        $rtn['ten'] = 'Ten';
        $rtn['img'] = 'Img';
        $rtn['img_kq'] = 'Img Kq';
        $rtn['y_kien'] = 'Y Kien';
        $rtn['date'] = 'Date';
        $rtn['trang_thai'] = 'Trang Thai';
        $rtn['uu_tien'] = 'Uu Tien';
        $rtn['in_trash'] = 'In Trash';
        $rtn['created_at'] = 'Created At';
        $rtn['update_at'] = 'Update At';
        return $rtn;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TbYkienkhachhang the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}