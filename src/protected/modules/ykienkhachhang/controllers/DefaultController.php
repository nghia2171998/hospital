<?php
/**
 * Admin Controller of ykienkhachhang module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 10/11/2021 08:24:02
 * @version 1.0
 */
class DefaultController extends FrontController 
{
    
    function init()
    {
        parent::init();
        $this->pageTitle = '';
    }

    function actionIndex()
    {
        $a = TbYkienkhachhang::VYKien();
        
        $this->render("index");
    }
    
}