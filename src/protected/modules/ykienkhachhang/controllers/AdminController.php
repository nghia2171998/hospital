<?php

/**
 * Admin Controller of ykienkhachhang module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 10/11/2021 08:24:02
 * @version 1.0
 */
class AdminController extends BackController {

    public $modelName = 'Ykienkhachhang';

    function init() {
        parent::init();
        $this->pageTitle = $this->modelName;
    }

    function getFormAttr() {
        return array(
            '' => array(
                array(
                    'field' => 'name'
                ),
                array(
                    'field' => 'status',
                    'type' => 'select'
                )
            ),
        );
    }

    /**
     * Danh sách các trường dùng cho danh sách
     * @return array
     */
    public function getListAttr() {
        return array(
            'name',
            'status'
        );
    }

    /**
     * Danh sách các trường dùng cho lọc
     * @return array
     */
    public function getFilterAttr() {
        return array(
            array(
                'field' => 'name',
                'type' => 'search'
            ),
            array(
                'field' => 'status',
                'type' => 'select'
            ),
        );
    }

    public function getAttrData() {
        $rtn = parent::getAttrData();
        return $rtn;
    }

    function getListAttr1() {
        return array(
            array(
                'field' => 'ten',
                'name' => 'Tên khách hàng',
                'type' => 'text'
            ),
            array(
                'field' => 'image',
                'name' => 'Ảnh khách hàng',
                'type' => 'img',
            ),
             array(
                'field' => 'images',
                'name' => 'Ảnh khám',
                'type' => 'img',
            ),
            array(
                'field' => 'trang_thai',
                'name' => 'Trạng thái',
                'type' => 'text'
            ),
        );
    }

    function getFormAttr1() {
        return array(
            array(
                'field' => 'ten',
                'name' => 'Tên khách hàng',
                'type' => 'text',
                'value' => ''
            ),
            array(
                'field' => 'image',
                'name' => 'Ảnh khách hàng',
                'type' => 'img',
                'value' => '',
            ),
            array(
                'field' => 'images',
                'name' => 'Ảnh khám',
                'type' => 'imgs',
                'value' => '',
            ),
            array(
                'field' => 'date',
                'name' => 'Ngày khám',
                'type' => 'date',
                'value' => '',
            ),
            array(
                'field' => 'title',
                'name' => "Nội dung",
                'type' => 'textarea',
                'value' => ''
            ),
            array(
                'field' => 'uu_tien',
                'name' => "Thứ tự ưu tiên",
                'type' => 'text',
                'value' => ''
            ),
            array(
                'field' => 'trang_thai',
                'name' => 'trạng thái',
                'type' => 'radio',
                'value' => ''
            ),
        );
    }

    function getTitle() {
        return array(
            'Title' => 'Ý kiến khách hàng',
            'addbutton'=>'1',
        );
    }

    function actionGetList() {
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $offset = ($page - 1) * $limit;
        $in_trash = $_GET['in_trash'];

        $yk = new CDbCriteria();
        $yk->order = 'id DESC';
        $yk->compare('in_trash', $in_trash);
        if (isset($_GET['select']) != '') {
            $yk->compare('trang_thai', $_GET['select']);
        }
        if (isset($_GET['tukhoa'])) {
            $yk->addSearchCondition('ten', $_GET['tukhoa']);
        }
        $yk->limit = $limit;
        $yk->offset = $offset;
        $ykien = TbYkienkhachhang::model()->findAll($yk);
        $numRec = TbYkienkhachhang::model()->count($yk);
        $numPage = ($numRec % $limit) ? ($numRec / $limit + 1) : $numRec / $limit;
        $rtn = array();
        foreach ($ykien as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'ten' => $item->ten,
                'image' => $item->img,
                'images' => $item->img_kq,
                'date' => $item->date,
                'uu_tien' => $item->uu_tien,
                'trang_thai' => $item->trang_thai
            );
        }
        $value = array('data' => $rtn, 'page' => $page, 'offset' => $offset, 'numPage' => $numPage, 'total' => $numRec);
        $this->renderJson("200", "thanh cong", $value);
    }

    function actionGetOneList() {
        $id = $_GET['id'];
        $data = TbYkienkhachhang::model()->findByPk($id);
        $rtn = array();
        $rtn['id'] = $data->id;
        $rtn['anh'] = $data->img;
        $rtn['anh_kq'] = $data->img_kq;
        $rtn['ten'] = $data->ten;
        $rtn['title'] = $data->y_kien;
        $rtn['date'] = $data->date;
        $rtn['uu_tien'] = $data->uu_tien;
        $rtn['trang_thai'] = $data->trang_thai;

        $this->renderJson("200", "thanh cong", $rtn);
    }

    function actionAddList() {
        $yk = new TbYkienkhachhang();
        $yk->ten = $_POST['ten'];
        $yk->img = $_POST['anh'];
        $yk->img_kq = $_POST['anh_kq'];
        $yk->y_kien = $_POST['title'];
        $yk->date = $_POST['date'];
        $yk->trang_thai = $_POST['rd1'];
        $yk->uu_tien = $_POST['uu_tien'];
        $yk->in_trash = 0;
        $yk->created_at = time();
        $yk->save();
    }

    function actionUplist() {
        $id = $_POST['hiddenId'];
        $yk = TbYkienkhachhang::model()->findByPk($id);
        $yk->ten = $_POST['ten'];
        $yk->img = $_POST['anh'];
        $yk->img_kq = $_POST['anh_kq'];
        $yk->y_kien = $_POST['title'];
        $yk->date = $_POST['date'];
        $yk->trang_thai = $_POST['rd1'];
        $yk->uu_tien = $_POST['uu_tien'];
        $yk->update_at = time();
        $yk->save();
    }

    function actionHidden() {
        $id = $_POST['id'];
        $yk = TbYkienkhachhang::model()->findByPk($id);
        $yk->in_trash = 1;
        $yk->update_at = time();
        $yk->save();
    }

    function actionUnHidden() {
        $id = $_POST['id'];
        $yk = TbYkienkhachhang::model()->findByPk($id);
        $yk->in_trash = 0;
        $yk->update_at = time();
        $yk->save();
    }


}