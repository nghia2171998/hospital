<?php
/**
 * ykienkhachhang module
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 10/11/2021 08:24:02
 * @version 1.0
 */
class YkienkhachhangModule extends MyModule
{

    public function init()
    {        
        /**
        * $this->setImport(array(
        *    'ykienkhachhang.models.*',
        *    'ykienkhachhang.components.*',
        * ));
        */
    }
    
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {     
            return true;
        }
        else
        {
            return false;
        }    
    }

}
