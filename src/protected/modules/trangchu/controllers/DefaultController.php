<?php

/**
 * Admin Controller of trangchu module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 08/10/2021 11:03:04
 * @version 1.0
 */
class DefaultController extends FrontController {

    function init() {
        parent::init();
        $this->pageTitle = '';
    }

    function actionIndex() {
        $this->render("index");
    }

    function actionKetqua() {
        $tk = TbChuyenkhoa::SearchCK();
        $bs = TbBacsi::SearchBS();
        $bv = TbBaiviet::SearchBV();
        if ($tk == NULL) {
            echo '';
        } else {
            echo "
        <div class='tk_main'>
            <h6 style='background-color:#ebebeb'>Chuyên khoa</h6>

            ";
            foreach ($tk as $row) {
                echo
                '<a style="display:flex;border-bottom:1px solid black;" href="' . Yii::app()->createUrl('chuyenkhoa/default', array('id' => $row['id'], 'url' => $row['url'])) . '">
                        <img width="50px"; height="50px"; src="'.Yii::app()->baseUrl.'/file/' . $row['image'] . '"/>
                        <p>' . $row["ten_khoa"] . '</p>
                   </a>
                   ';
            }
            echo "
        </div>";
        }
        if ($bs == NULL) {
            echo '';
        } else {
            echo "
        <div class='tk_main'>
            <h6 style='background-color:#ebebeb'>Bác sĩ</h6>

            ";
            foreach ($bs as $b) {
                echo
                '<a style="display:flex;border-bottom:1px solid black;" href="' . Yii::app()->createUrl('bacsi/default', array('id' => $b['id'], 'url' => $b['url'])) . '">
                        <img width="50px"; height="50px"; src="'.Yii::app()->baseUrl.'/file/' . $b['image'] . '"/>
                        <p>' . $b["ten"] . '</p>
                   </a>
                   ';
            }
            echo "
        </div>";
        }

        if ($bv == NULL) {
            echo '';
        } else {
            echo "
        <div class='tk_main'>
            <h6 style='background-color:#ebebeb'>Bài viết</h6>

            ";
            foreach ($bv as $baiviet) {
                echo
                '<a style="display:flex;border-bottom:1px solid black;" href="' . Yii::app()->createUrl('baiviet/default', array('id' => $baiviet['id'], 'url' => $baiviet['url'])) . '">
                        <img width="50px"; height="50px"; src="'.Yii::app()->baseUrl.'/file/' . $baiviet['image'] . '"/>
                        <p>' . $baiviet["ten"] . '</p>
                   </a>
                   ';
            }
            echo "
        </div>";
        }
    }

}
