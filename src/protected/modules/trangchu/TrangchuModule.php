<?php
/**
 * trangchu module
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 08/10/2021 11:03:04
 * @version 1.0
 */
class TrangchuModule extends MyModule
{

    public function init()
    {        
        /**
        * $this->setImport(array(
        *    'trangchu.models.*',
        *    'trangchu.components.*',
        * ));
        */
    }
    
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {     
            return true;
        }
        else
        {
            return false;
        }    
    }

}
