<slide-show>
    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="https://cdn.bookingcare.vn/fo/2021/09/06/155549-37d096f2e8e81eb647f9.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5>First slide label</h5>
                    <p>Some representative placeholder content for the first slide.</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="https://cdn.bookingcare.vn/fo/2021/07/27/140801-test-covid.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Second slide label</h5>
                    <p>Some representative placeholder content for the second slide.</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="https://cdn.bookingcare.vn/fo/2021/09/24/091949-bs-videocall.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Third slide label</h5>
                    <p>Some representative placeholder content for the third slide.</p>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</slide-show>

<main id="main">
    <div class='nen'>
        <div class='container'>
            <section id="testimonials">
                <div style='width:100%' data-aos="fade-up">
                    <div class='title'>
                        <div class='left'>
                            <h3>Chuyên khoa phổ biến</h3>
                        </div>
                        <div class='right'>
                            <a class="btn" href="<?php echo Yii::app()->baseUrl ?>/Chuyen-khoa" role="button">Xem thêm</a>
                        </div>
                    </div>
                    <div class="testimonials-slider swiper item" data-aos="fade-up"
                         data-aos-delay="100">
                        <div class="swiper-wrapper">
                            <?php
                            $rows = TbChuyenkhoa::model()->Sort();
                            foreach ($rows as $row) {
                                ?>
                                <div class="swiper-slide">
                                    <a
                                        href="<?php echo Yii::app()->createUrl('chuyenkhoa/default', array('id' => $row['id'], 'url' => $row['url'])) ?>">
                                        <div class="testimonial-item-1 dich_vu">
                                            <p>
                                                <img src='<?php echo Yii::app()->baseUrl ?>/file/<?= $row['image'] ?>'
                                                     style="" />
                                            </p>

                                            <h3><?= $row['ten_khoa'] ?></h3>
                                        </div>
                                    </a>
                                </div><!-- End testimonial item -->
                                <?php
                            }
                            ?>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class='nen nen-xam'>
        <div class='container'>
            <section id="testimonials">
                <div style='width:100%' data-aos="fade-up">
                    <div class='title'>
                        <div class='left'>
                            <h3>Đội ngũ bác sĩ</h3>
                        </div>
                        <div class='right'>
                            <a class="btn" href="<?php echo Yii::app()->baseUrl ?>/Bac-si">Xem thêm</a>
                        </div>
                    </div>

                    <div class="testimonials-slider swiper" style='width:100%' data-aos="fade-up" data-aos-delay="100">
                        <div class="swiper-wrapper">
                            <?php
                            $bs = TbBacsi::model()->Sort();
                            foreach ($bs as $b) {
                                ?>
                                <div class="swiper-slide">
                                    <div class="testimonial-item 1_bs">
                                        <a
                                            href='<?php echo Yii::app()->createUrl('bacsi/default', array('id' => $b['id'], 'url' => $b['url'])) ?>'>
                                            <img class="img_bs" src='<?php echo Yii::app()->baseUrl ?>/file/<?= $b['image'] ?>' />

                                            <h3 class="ten_bs"><?= $b['ten'] ?></h3>
                                            <h4>
                                                <?php
                                                $x = json_decode($b['chuc_vu'], true);
                                                foreach ($x as $y) {
                                                    $cv = TbChuyenkhoa::model()->findByPk($y);
                                                    echo $cv['ten_khoa'];
                                                }
                                                ?>
                                            </h4>
                                        </a>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>

                </div>
            </section>
        </div>
    </div>

    <div class='nen'>
        <div class='container'>
            <section id="testimonials">
                <div class="item_cn" style='width:100%;' data-aos="fade-up">
                    <div class='title'>
                        <div class='left'>
                            <h3>Cẩm nang</h3>
                        </div>
                        <div class='right'>
                            <a class="btn" href="<?php echo Yii::app()->baseUrl ?>/danhmuc">Xem thêm</a>
                        </div>
                    </div>

                    <div class="testimonials-slider2 swiper" style='width:100%' data-aos="fade-up" data-aos-delay="100">
                        <div class="swiper-wrapper khung_cn">
                            <?php
                            $cn = TbBaiviet::model()->Sort();
                            foreach ($cn as $bv) {
                                ?>
                                <div class="swiper-slide">
                                    <a
                                        href='<?php echo Yii::app()->createUrl('baiviet/default', array('id' => $bv['id'], 'url' => $bv['url'])) ?>'>
                                        <div class="testimonial-item1">
                                            <div class="img_cn">
                                                <img src='<?php echo Yii::app()->baseUrl ?>/file/<?= $bv['image'] ?>' />
                                            </div>
                                            <h4><?= $bv['ten'] ?></h4>
                                        </div>
                                    </a>
                                </div><!-- End testimonial item -->
                                <?php
                            }
                            ?>

                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>

                </div>
            </section>
        </div>
    </div>

    <div class='nen nen-xam'>
        <div class='container'>
            <section id="testimonials">
                <div style='width:100%' data-aos="fade-up">
                    <div class='title'>
                        <div style='text-align: center;'>
                            <h3>Trải nghiệm khách hàng</h3>
                        </div>
                    </div>

                    <div class="testimonials-slider1 swiper y_kien_khach_hang" style='width:100%;' data-aos="fade-up" data-aos-delay="100">
                        <div class="swiper-wrapper">
                            <?php
                            $ykien = TbYkienkhachhang::model()->VYKien();
                            foreach ($ykien as $yk) {
                                if ($yk['in_trash'] == 0) {
                                    ?>
                                    <div class="swiper-slide">
                                        <div class="testimonial-item2">
                                            <div class='ykien'>
                                                <div class='tt_kh'>
                                                    <img src="<?php echo Yii::app()->baseUrl ?>/file/<?= $yk['img'] ?>">
                                                    <div class="ct_kh">
                                                        <h5><?= $yk['ten'] ?></h5>
                                                        <p><?= $yk['y_kien'] ?></p>
                                                    </div>
                                                </div>
                                                <div class='img_kh'>
                                                    <img src='<?php echo Yii::app()->baseUrl ?>/file/<?= $yk['img_kq'] ?>' />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class='nen'>
        <div class='container'>
            <section id="testimonials">
                <div style='width:100%' data-aos="fade-up">
                    <div class='title'>
                        <div class='left'>
                            <h3>Truyền thông</h3>
                        </div>
                        <div class='right'>
                            <a class="btn" href="<?php echo Yii::app()->baseUrl ?>/Truyen-thong">Xem thêm</a>
                        </div>
                    </div>

                    <div class="testimonials-slider3 swiper" style='width:100%' data-aos="fade-up" data-aos-delay="100">
                        <div class="swiper-wrapper">
                            <?php
                            $media = TbTruyenthong::model()->Sort();
                            foreach ($media as $row) {
                                ?>

                                <div class="swiper-slide">
                                    <a href="<?php echo $row['url'] ?>" target="_blank">
                                        <div class="testimonial-item s_truyen_thong">
                                            <p>
                                                <img src='<?php echo Yii::app()->baseUrl ?>/file/<?= $row['image'] ?>'/>
                                            </p>
                                            <h3 style="font-weight: normal;"><?= $row['content'] ?></h3>
                                        </div>
                                    </a>
                                </div><!-- End testimonial item -->
                                <?php
                            }
                            ?>


                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>

                </div>
            </section>
        </div>
    </div>
</main>

<!-- Messenger Plugin chat Code -->
<div id="fb-root"></div>

<!-- Your Plugin chat code -->
<div id="fb-customer-chat" class="fb-customerchat">
</div>

<script>
    var chatbox = document.getElementById('fb-customer-chat');
    chatbox.setAttribute("page_id", "110329668169910");
    chatbox.setAttribute("attribution", "biz_inbox");
</script>

<!-- Your SDK code -->
<script>
    window.fbAsyncInit = function () {
        FB.init({
            xfbml: true,
            version: 'v13.0'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script>
    new Swiper('.testimonials-slider', {
        speed: 1500,
        loop: true,
        pauseOnHover: true,
        autoplay: {
            delay: 7000,
            disableOnInteraction: false
        },
        slidesPerView: 'auto',
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 1.3,
                spaceBetween: 20
            },
            360: {
                slidesPerView: 1.5,
                spaceBetween: 20
            },
            1200: {
                slidesPerView: 4,
                spaceBetween: 20
            },
            500: {
                slidesPerView: 3,
                spaceBetween: 20
            }
        }
    });

    new Swiper('.testimonials-slider2', {
        speed: 1500,
        loop: true,
        autoplay: {
            delay: 2000,
            disableOnInteraction: false
        },
        slidesPerView: 'auto',
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 1.5,
                spaceBetween: 20
            },
            600: {
                slidesPerView: 2,
                spaceBetween: 20
            }
        }
    });

    new Swiper('.testimonials-slider3', {
        speed: 600,
        loop: true,
        autoplay: {
            delay: 7000,
            disableOnInteraction: false
        },
        slidesPerView: 'auto',

        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 20
            },

            1200: {
                slidesPerView: 3,
                spaceBetween: 20
            },
            700: {
                slidesPerView: 2,
                spaceBetween: 20
            }
        }
    });

    new Swiper('.testimonials-slider1', {
        speed: 1500,
        loop: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false
        },
        slidesPerView: 'auto',
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 20
            },

            700: {
                slidesPerView: 1,
                spaceBetween: 20
            }
        }
    });
</script>
<script src="https://bootstrapmade.com/demo/templates/Reveal/assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="https://bootstrapmade.com/demo/templates/Reveal/assets/vendor/swiper/swiper-bundle.min.js"></script>