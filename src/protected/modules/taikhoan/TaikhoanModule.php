<?php
/**
 * taikhoan module
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 01/11/2021 10:07:20
 * @version 1.0
 */
class TaikhoanModule extends MyModule
{

    public function init()
    {        
        /**
        * $this->setImport(array(
        *    'taikhoan.models.*',
        *    'taikhoan.components.*',
        * ));
        */
    }
    
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {     
            return true;
        }
        else
        {
            return false;
        }    
    }

}
