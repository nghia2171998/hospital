<?php

/**
 * Admin Controller of taikhoan module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 01/11/2021 10:07:20
 * @version 1.0
 */
class AdminController extends BackController {

    public $modelName = 'Taikhoan';

    function init() {
        parent::init();
        $this->pageTitle = $this->modelName;
    }

    function getFormAttr() {
        return array(
            '' => array(
                array(
                    'field' => 'name'
                ),
                array(
                    'field' => 'status',
                    'type' => 'select'
                )
            ),
        );
    }

    /**
     * Danh sách các trường dùng cho danh sách
     * @return array
     */
    public function getListAttr() {
        return array(
            'name',
            'status'
        );
    }

    /**
     * Danh sách các trường dùng cho lọc
     * @return array
     */
    public function getFilterAttr() {
        return array(
            array(
                'field' => 'name',
                'type' => 'search'
            ),
            array(
                'field' => 'status',
                'type' => 'select'
            ),
        );
    }

    function getListAttr1() {
        return array(
            array(
                'field' => 'ten',
                'name' => 'Tên tài khoản',
                'type' => 'text'
            ),
            array(
                'field' => 'email',
                'name' => 'Email',
                'type' => 'text'
            ),
        );
    }

    function getFormAttr1() {
        return array(
            array(
                'field' => 'ten',
                'name' => 'Tên tài khoản',
                'type' => 'user',
                'value' => ''
            ),
            array(
                'field' => 'email',
                'name' => "Email",
                'type' => 'email',
                'value' => ''
            ),
            array(
                'field' => 'pass',
                'name' => 'Mật khẩu',
                'type' => 'password',
                'value' => ''
            ),
        );
    }

    function getTitle() {
        return array(
            'Title' => 'Tài khoản',
            'addbutton' => '1',
        );
    }

    public function getAttrData() {
        $rtn = parent::getAttrData();
        return $rtn;
    }

    function actioncheckuser() {
        $tk = new CDbCriteria();
        $tk->compare('username', $_GET['ten']);
        $user = TbUser::model()->count($tk);
        $kq = ($user == 0) ? 1 : 'tài khoản đã có người đăng ký';
        $this->renderJson("200", "thanh cong", $kq);
    }

    function actioncheckemail() {
        $tk = new CDbCriteria();
        $tk->compare('email', $_GET['email']);
        $user = TbUser::model()->count($tk);
        $kq = ($user == 0) ? 1 : 'email đã có người đăng ký';
        $this->renderJson("200", "thanh cong", $kq);
    }

    function actionGetList() {
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $offset = ($page - 1) * $limit;
        $in_trash = $_GET['in_trash'];

        $tk = new CDbCriteria();
        $tk->order = 'id DESC';
        $tk->compare('in_trash', $in_trash);
        if (isset($_GET['select']) != '') {
            $tk->compare('cap_quyen', $_GET['select']);
        }
        if (isset($_GET['tukhoa'])) {
            $tk->addSearchCondition('username', $_GET['tukhoa']);
        }
        $tk->limit = $limit;
        $tk->offset = $offset;
        $taikhoan = TbUser::model()->findAll($tk);
        $numRec = TbUser::model()->count($tk);
        $numPage = ($numRec % $limit) ? ($numRec / $limit + 1) : $numRec / $limit;
        $rtn = array();
        foreach ($taikhoan as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'ten' => $item->username,
                'email' => $item->email,
                'trang_thai' => $item->cap_quyen
            );
        }
        $value = array('data' => $rtn, 'page' => $page, 'offset' => $offset, 'numPage' => $numPage, 'total' => $numRec);
        $this->renderJson("200", "thanh cong", $value);
    }

    function actionGetOneList() {
        $id = $_GET['id'];
        $data = TbUser::model()->findByPk($id);
        $rtn = array();
        $rtn['id'] = $data->id;
        $rtn['ten'] = $data->username;
        $rtn['email'] = $data->email;
        $rtn['trang_thai'] = $data->cap_quyen;

        $this->renderJson("200", "thanh cong", $rtn);
    }

    function actionAddList() {
        $tk = new TbUser();
        $tk->username = $_POST['ten'];
        $tk->email = $_POST['email'];
        $tk->password = MyUtil::mahoa($_POST['mk1']);
        $tk->cap_quyen = 1;
        $tk->in_trash = 0;
        $tk->created_at = time();
        MyUtil::senmail($_POST['ten'],$_POST['email'],$_POST['mk1']);
        $tk->save();
    }

    function actionUplist() {
        $id = $_POST['hiddenId'];
        $tk = TbUser::model()->findByPk($id);
        $tk->username = $_POST['ten'];
        $tk->email = $_POST['email'];
        $tk->password = MyUtil::mahoa($_POST['mk1']);
        $tk->cap_quyen = 1;
        $tk->update_at = time();
        $tk->save();
        MyUtil::senmail($_POST['ten'],$_POST['email'],$_POST['mk1']);
    }

    function actionHidden() {
        $id = $_POST['id'];
        $yk = TbUser::model()->findByPk($id);
        $yk->in_trash = 1;
        $yk->update_at = time();
        $yk->save();
    }

    function actionUnHidden() {
        $id = $_POST['id'];
        $yk = TbUser::model()->findByPk($id);
        $yk->in_trash = 0;
        $yk->update_at = time();
        $yk->save();
    }

    public function actionLogout() {
        session_unset();
        $this->redirect(Yii::app()->createUrl('taikhoan'));
    }
    
}