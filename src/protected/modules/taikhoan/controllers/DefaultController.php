<?php
/**
 * Admin Controller of taikhoan module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 01/11/2021 10:07:20
 * @version 1.0
 */

class DefaultController extends FrontController 
{
    // 
    function init()
    {
        parent::init();
        $this->pageTitle = '';
    }

    function actionIndex()
    {
        $this->renderPartial('index');
    }
    function actionLogin()
    {
        session_start();
        $user=$_REQUEST['tk'];
        $pass=MyUtil::mahoa($_REQUEST['mk']);
        $n=TbUserBase::model ()->login($user,$pass);
        if($n==''){
            $tb='Tài Khoản hoặc Mật Khẩu không khớp.';
            $this->renderPartial('index', array('tb'=>$tb));
        }
        else{
            $_SESSION['login'] = "OK";
            $_SESSION['User'] = $n['username'];
            $_SESSION['Time'] = time();
            $this->redirect( Yii::app()->createUrl('taikhoan/admin'));
        }
    }

}