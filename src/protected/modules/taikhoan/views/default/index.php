<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <title>Login</title>
    <!-- jQuery + Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <style>
    * {
        box-sizing: border-box;
    }

    body {
        font-weight: 400;
        background-color: #EEEFF4;
    }

    body,
    html,
    .App,
    .vertical-center {
        width: 100%;
        height: 100%;
    }

    .navbar {
        background: #1833FF !important;
        width: 100%;
    }

    .btn-outline-primary {
        border-color: #1833FF;
        color: #1833FF;
    }

    .btn-outline-primary:hover {
        background-color: #1833FF;
        color: #ffffff;
    }

    .vertical-center {
        display: flex;
        text-align: left;
        justify-content: center;
        flex-direction: column;
    }

    .inner-block {
        width: 450px;
        margin: auto;
        background: #ffffff;
        box-shadow: 0px 14px 80px rgba(34, 35, 58, 0.2);
        padding: 40px 55px 45px 55px;
        transition: all .3s;
        border-radius: 20px;
    }

    .vertical-center .form-control:focus {
        border-color: #2554FF;
        box-shadow: none;
    }

    .vertical-center h3 {
        text-align: center;
        margin: 0;
        line-height: 1;
        padding-bottom: 20px;
    }

    label {
        font-weight: 500;
    }
    </style>
</head>

<body>
    <?php
  $thongbao='';
  if(isset($tb))
  {
    $thongbao=$tb;
  }
  ?>
    <!-- Login form -->
    <div class="App">
        <div class="vertical-center">
            <div class="inner-block">
                <form action="<?php echo yii::app()->createUrl('taikhoan/default/login')?>" method="post">
                    <h3>Đăng Nhập</h3>
                    <div class="form-group">
                        <label>Tài Khoản</label>
                        <input type="text" class="form-control" name="tk" id="email_signin" />
                    </div>

                    <div class="form-group">
                        <label>Mật Khẩu</label>
                        <input type="password" class="form-control" name="mk" id="password_signin" />
                    </div>

                    <button type="submit" name="login" id="sign_in"
                        class="btn btn-outline-primary btn-lg btn-block">Đăng Nhập</button>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label"
                            style='color:red;margin-top:10px'><?=$thongbao?></label>
                    </div>

                </form>
            </div>
        </div>
    </div>

</body>

</html>