<?php

/**
 * TbUser là lớp chính của bảng "tb_user".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 01/11/2021 10:07:44
 * @version 1.0
 *
 */
class TbUser extends TbUserBase
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Danh sách các cấu hình của model
     * @return array
     */
    public static function configArray()
    {
        $cnf = parent::configArray();
        return $cnf;
    }

    /**
     * Hàm gọi cấu hình theo model
     * @param string $item Tên của cấu hình
     * @return mix Giá trị của cấu hình tùy theo từng kiểu cấu hình mà có kiểu khác nhau
     */
    public static function getConf($item)
    {
        $option = Setting::getItem($item, __CLASS__);
        return $option ? $option : self::getDefaultConfig($item);
    }
    public static function SearchAdminTK()
    {
        $timkiem = new CDbCriteria();
        $timkiem->addSearchCondition('username', $_REQUEST['tukhoa1']);
        $tk = TbUser::model()->findAll($timkiem);
        return $tk;
    }
}
