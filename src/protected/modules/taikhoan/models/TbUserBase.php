<?php

/**
 * Đây là lớp model của bảng "tb_user".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 14/01/2022 03:30:00
 * @version 1.0
 *
 * Dưới đây là các cột của bảng 'tb_user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property integer $cap_quyen
 * @property integer $in_trash
 * @property integer $created_at
 * @property integer $update_at
 */
class TbUserBase extends MyModel
{

    public $modelName = 'TbUser';

    /**
     * @return string trả về tên của bảng
     */
    public function tableName()
    {
        return 'tb_user';
    }

    /**
     * @return array Các quy định về thuộc tính của lớp.
     */
    public function rules()
    {
        // LƯU Ý: bạn chỉ nên định nghĩa các quy định cho các trường
        // mà người dùng sẽ thể nhập liệu    
        return array(
            array('username, password, email, cap_quyen, in_trash, created_at', 'required'),
            array('cap_quyen, in_trash, created_at, update_at', 'numerical', 'integerOnly'=>true),
            array('username, email', 'length', 'max' => 100),
            array('password', 'length', 'max' => 50),
            // Những thuộc tính dưới đây để phục vụ cho hàm search().            
            // @todo Hãy xóa đi những trường mà bạn không muốn cho người dùng tìm kiếm
            array('id, username, password, email, cap_quyen, in_trash, created_at, update_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array mối quan hệ với các bảng khác.
     */
    public function relations()
    {
        // LƯU Ý: Bạn có thể cần phải điều chỉnh tên các quan hệ 
        // và các lớp liên quan được tự động sinh ra bên dưới đây        
        $rtn = parent::relations();
        return $rtn;
    }

    /**
     * @return array Tùy chỉnh tên các thuộc tính (name=>label)
     */
    public function attributeLabels()
    {
        $rtn = parent::attributeLabels();
        $rtn['id'] = 'ID';
        $rtn['username'] = 'Username';
        $rtn['password'] = 'Password';
        $rtn['email'] = 'Email';
        $rtn['cap_quyen'] = 'Cap Quyen';
        $rtn['in_trash'] = 'In Trash';
        $rtn['created_at'] = 'Created At';
        $rtn['update_at'] = 'Update At';
        return $rtn;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TbUser the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function login($user,$pass)
	{
		$row = Yii::app()->db->createCommand(array(
            'select' => array('id', 'username'),
            'from' => 'tb_user',
            'where' => array('and','username=:user','password=:pass'),
            'params' => array(':user'=>$user,':pass'=>$pass),
        ))->queryRow();
        return $row;
	}
}