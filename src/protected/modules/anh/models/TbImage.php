<?php

/**
 * TbImage là lớp chính của bảng "tb_image".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 10/01/2022 07:49:50
 * @version 1.0
 *
 */
class TbImage extends TbImageBase
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Danh sách các cấu hình của model
     * @return array
     */
    public static function configArray()
    {
        $cnf = parent::configArray();
        return $cnf;
    }

    /**
     * Hàm gọi cấu hình theo model
     * @param string $item Tên của cấu hình
     * @return mix Giá trị của cấu hình tùy theo từng kiểu cấu hình mà có kiểu khác nhau
     */
    public static function getConf($item)
    {
        $option = Setting::getItem($item, __CLASS__);
        return $option ? $option : self::getDefaultConfig($item);
    }

    public static function Sort()
    {
        $sort = new CDbCriteria();
        $sort -> order = 'id DESC';
        $img  = TbImage::model()->findAll($sort);
        return $img;
    }

}