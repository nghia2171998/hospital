<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Admin</title>
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/assets/css/style_admin.css" />
        <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/https _ajax.googleapis.com_ajax_libs_jquery_3.6.0_jquery.min.js"></script>
        <link type="text/css" rel="stylesheet"
              href="<?php echo Yii::app()->baseUrl ?>/assets/css/https _cdn.jsdelivr.net_npm_bootstrap@5.1.3_dist_css_bootstrap.min.css" />
        <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/https _cdn.jsdelivr.net_npm_bootstrap@5.1.3_dist_js_bootstrap.bundle.min.js"></script>

        <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/vue.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/sweetalert2@11.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/vue_index.js"></script>
    </head>

    <body>
        <div id='anh' style='width:100%;height:auto;display:block'>
            <div class='title'>
                <nav class="navbar navbar-light bg-light">
                    <div class="container-fluid">
                        <a class="navbar-brand"></a>
                        <p>Ảnh</p>
                        <div class="d-flex">
                            <form enctype="multipart/form-data" id='img'>
                                <label class="btn btn-primary custom-file-upload" v-if='in_trash==0'>
                                    <input type="file" v-on:change="onFileChange()" name='img' />
                                    Tải ảnh lên
                                </label>
                            </form>
                            <div id="preview">
                                <img v-if="url" :src="url" />
                            </div>
                        </div>
                    </div>
                </nav>
                <div class='act' v-if='in_trash==0'>
                    <input v-model='tukhoa' v-on:keyup.enter="fetchAllData()" type="search" placeholder="Tìm kiếm" />
                    <button type="button" class="btn btn-secondary" onclick="reload()">Tất cả ảnh</button>
                </div>
            </div>
            <div class="container" style='margin-top:100px;max-width:1550px;'>
                <div class="row row-cols-4">
                    <div class="col" style='margin-bottom:10px' v-for='row in allData'>
                        <div class='image'>
                            <img width='200px' height='100px' :src="'<?php echo Yii::app()->baseUrl ?>/file/'+row.url" />
                        </div>
                        <div class='title_img'>
                            <h6>{{row.url}}</h6>
                            <button v-if="in_trash==0" id="myIframe" type="button" class="btn btn-primary close1"
                                    @click="send(row.url);">chọn</button>

                            <button v-if='in_trash==0' type="button" class="btn btn-primary"
                                    @click="hiddenData(row.id)">Xoá</button>
                            <button v-if='in_trash==1' type="button" class="btn btn-primary"
                                    @click="unHiddenData(row.id)">Khôi
                                phục</button>
                        </div>

                    </div>
                </div>
            </div>
            <div class="text-center" v-if="pageList.length > 1">
                <button v-if='in_trash==0' class="btn btn-info mr-2" v-bind:class="page ==  current_page ? 'btn-dark' : ''"
                        v-for="page in pageList" v-on:click="fetchAllData(page)">{{page}}</button>
                <button v-if='in_trash==1' class="btn btn-info mr-2" v-bind:class="page ==  current_page ? 'btn-dark' : ''"
                        v-for="page in pageList" v-on:click="fetchAllHiddenData(page)">{{page}}</button>
            </div>
        </div>
    </body>

</html>
<script>
    var application = new Vue({
        el: '#anh',
        data: {
            allData: '',
            in_trash: 0,
            pageList: [],
            limit: 12,
            current_page: 1,
            url: '',
            tukhoa: '',
        },
        methods: {
            fetchAllData: function (page = 1) {
                application.in_trash = 0;
                if (page) {
                    application.current_page = page;
                }
                $.ajax({
                    url: "<?php echo Yii::app()->createUrl('anh/admin/GetList') ?>",
                    type: 'GET',
                    data: {
                        in_trash: 0,
                        limit: 12,
                        page: application.current_page,
                        tukhoa: application.tukhoa,
                    },
                    success: function (res) {
                        application.allData = res.data.data;
                        var page_number = Math.ceil(res.data.total / application.limit);
                        application.pageList = [];
                        for (var i = 1; i <= page_number; i++) {
                            application.pageList.push(i);
                        }
                    },
                });
            },
            send: function (url) {
                parent.hello(url);
            },
            fetchAllHiddenData: function (page = 1) {
                application.in_trash = 1;
                if (page) {
                    application.current_page = page;
                }
                $.ajax({
                    url: "<?php echo Yii::app()->createUrl('anh/admin/GetList') ?>",
                    type: 'GET',
                    data: {
                        in_trash: 1,
                        limit: 12,
                        page: application.current_page,
                    },
                    success: function (res) {
                        application.allData = res.data.data;
                        var page_number = Math.ceil(res.data.total / application.limit);
                        application.pageList = [];
                        for (var i = 1; i <= page_number; i++) {
                            application.pageList.push(i);
                        }
                    },
                });
            },
            submitData: function () {},
            hiddenData: function (id) {
                Swal.fire({
                    title: 'Bạn muốn xoá?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Đồng ý',
                    cancelButtonText: 'Không'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire(
                                'Đã xoá!',
                                ),
                                $.ajax({
                                    url: "<?php echo Yii::app()->createUrl('anh/admin/Hidden') ?>",
                                    type: 'POST',
                                    data: {
                                        id: id,
                                    },
                                }).then(function (res) {
                            application.fetchAllData();
                        });
                    }
                })
            },
            unHiddenData: function (id) {
                $.ajax({
                    url: "<?php echo Yii::app()->createUrl('anh/admin/UnHidden') ?>",
                    type: 'POST',
                    data: {
                        id: id,
                    },
                }).then(function (res) {
                    application.fetchAllHiddenData();
                });
            },
            onFileChange(e) {

                var attachment_data = document.getElementById('img');
                var form_data = new FormData(document.getElementById("img"));
                console.log('form_data', form_data);
                $.ajax({
                    url: "<?php echo Yii::app()->createUrl('anh/admin/AddList') ?>",
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: form_data,
                }).then(function (res) {
                    application.fetchAllData();

                });
            }
        },
        unHiddenData: function (id) {
            $.ajax({
                url: "<?php echo Yii::app()->createUrl('anh/admin/UnHidden') ?>",
                type: 'POST',
                data: {
                    id: id,
                },
            }).then(function (res) {
                application.fetchAllHiddenData();
            });
        },
    });
    application.fetchAllData();

    function reload() {
        application.tukhoa = '';
        application.fetchAllData();
    }
</script>