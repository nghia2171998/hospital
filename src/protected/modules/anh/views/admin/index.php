<div id='anh' style='width:100%;height:auto;display:block'>
    <div class='title'>
        <nav class="navbar navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand"></a>
                <p>Ảnh</p>
                <div class="d-flex">
                    <form enctype="multipart/form-data" id='img'>
                        <label class="btn btn-primary custom-file-upload" v-if='in_trash==0'>
                            <input type="file" v-on:change="onFileChange()" name='img' />
                            Tải ảnh lên
                        </label>
                    </form>
                    <div id="preview">
                        <img v-if="url" :src="url" />
                    </div>
                    <button v-if='in_trash==0' type="button" class="btn btn-warning float-end"
                        @click='fetchAllHiddenData()'>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-trash-fill" viewBox="0 0 16 16">
                            <path
                                d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z" />
                        </svg>
                    </button>
                    <button v-if='in_trash==1' type="button" class="btn btn-dark float-end" style='margin-bottom: 20px'
                        @click='fetchAllData()'>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-arrow-return-left" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M14.5 1.5a.5.5 0 0 1 .5.5v4.8a2.5 2.5 0 0 1-2.5 2.5H2.707l3.347 3.346a.5.5 0 0 1-.708.708l-4.2-4.2a.5.5 0 0 1 0-.708l4-4a.5.5 0 1 1 .708.708L2.707 8.3H12.5A1.5 1.5 0 0 0 14 6.8V2a.5.5 0 0 1 .5-.5z" />
                        </svg>
                    </button>
                </div>
            </div>
        </nav>
    </div>
    <div class='act' v-if='in_trash==0'>
        <input v-model='tukhoa' v-on:keyup.enter="fetchAllData()" type="search" placeholder="Tìm kiếm" />
        <button type="button" class="btn btn-secondary" onclick="reload()">Tất cả ảnh</button>
    </div>
    <div class="container" style='margin-top:100px;max-width:1550px;'>
        <div class="row row-cols-4">
            <div class="col" style='margin-bottom:10px' v-for='row in allData'>
                <div class='image'>
                    <img width='200px' height='100px' :src="'<?php echo Yii::app()->baseUrl ?>/file/'+row.url" />
                </div>
                <div class='title_img'>
                    <h6>{{row.url}} </h6>
                    <button v-if='in_trash==0' type="button" class="btn btn-primary">chọn</button>
                    <button v-if='in_trash==0' type="button" class="btn btn-primary"
                        @click="hiddenData(row.id)">Xoá</button>
                    <button v-if='in_trash==1' type="button" class="btn btn-primary" @click="unHiddenData(row.id)">Khôi
                        phục</button>
                </div>

            </div>
        </div>
    </div>
    <div class="text-center" v-if="pageList.length > 1">
        <button v-if='in_trash==0' class="btn btn-info mr-2" v-bind:class="page ==  current_page ? 'btn-dark' : ''"
            v-for="page in pageList" v-on:click="fetchAllData(page)">{{page}}</button>
        <button v-if='in_trash==1' class="btn btn-info mr-2" v-bind:class="page ==  current_page ? 'btn-dark' : ''"
            v-for="page in pageList" v-on:click="fetchAllHiddenData(page)">{{page}}</button>
    </div>
</div>
<script>
var application = new Vue({
    el: '#anh',
    data: {
        allData: '',
        in_trash: 0,
        pageList: [],
        limit: 12,
        current_page: 1,
        url: '',
        tukhoa: '',
    },
    methods: {
        fetchAllData: function(page = 1) {
            application.in_trash = 0;
            if (page) {
                application.current_page = page;
            }
            $.ajax({
                url: "<?php echo Yii::app()->createUrl('anh/admin/GetList') ?>",
                type: 'GET',
                data: {
                    in_trash: 0,
                    limit: 12,
                    page: application.current_page,
                    tukhoa: application.tukhoa,
                },
                success: function(res) {
                    application.allData = res.data.data;
                    var page_number = Math.ceil(res.data.total / application.limit);
                    application.pageList = [];
                    for (var i = 1; i <= page_number; i++) {
                        application.pageList.push(i);
                    }
                },
            });
        },
        fetchAllHiddenData: function(page = 1) {
            application.in_trash = 1;
            if (page) {
                application.current_page = page;
            }
            $.ajax({
                url: "<?php echo Yii::app()->createUrl('anh/admin/GetList') ?>",
                type: 'GET',
                data: {
                    in_trash: 1,
                    limit: 12,
                    page: application.current_page,
                },
                success: function(res) {
                    application.allData = res.data.data;
                    var page_number = Math.ceil(res.data.total / application.limit);
                    application.pageList = [];
                    for (var i = 1; i <= page_number; i++) {
                        application.pageList.push(i);
                    }
                },
            });
        },
        submitData: function() {},
        hiddenData: function(id) {
            Swal.fire({
                title: 'Bạn muốn xoá?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Đồng ý',
                cancelButtonText: 'Không'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                            'Đã xoá!',
                        ),
                        $.ajax({
                            url: "<?php echo Yii::app()->createUrl('anh/admin/Hidden') ?>",
                            type: 'POST',
                            data: {
                                id: id,
                            },
                        }).then(function(res) {
                            application.fetchAllData();
                        });
                }
            })
        },
        unHiddenData: function(id) {
            $.ajax({
                url: "<?php echo Yii::app()->createUrl('anh/admin/UnHidden') ?>",
                type: 'POST',
                data: {
                    id: id,
                },
            }).then(function(res) {
                application.fetchAllHiddenData();
            });
        },
        onFileChange(e) {
            var attachment_data = document.getElementById('img');
            var form_data = new FormData(document.getElementById("img"));
            console.log('form_data', form_data);
            $.ajax({
                url: "<?php echo Yii::app()->createUrl('anh/admin/AddList') ?>",
                type: 'POST',
                contentType: false,
                processData: false,
                data: form_data,
            }).then(function(res) {
                application.fetchAllData();

            });
        }
    },
    unHiddenData: function(id) {
        $.ajax({
            url: "<?php echo Yii::app()->createUrl('anh/admin/UnHidden') ?>",
            type: 'POST',
            data: {
                id: id,
            },
        }).then(function(res) {
            application.fetchAllHiddenData();
        });
    },
});
application.fetchAllData();

function reload() {
    application.tukhoa = '';
    application.fetchAllData();
}
</script>