<?php
/**
 * anh module
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 23/11/2021 08:51:28
 * @version 1.0
 */
class AnhModule extends MyModule
{

    public function init()
    {        
        /**
        * $this->setImport(array(
        *    'anh.models.*',
        *    'anh.components.*',
        * ));
        */
    }
    
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {     
            return true;
        }
        else
        {
            return false;
        }    
    }

}
