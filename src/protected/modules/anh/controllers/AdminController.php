<?php

/**
 * Admin Controller of anh module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 23/11/2021 08:51:28
 * @version 1.0
 */
class AdminController extends BackController {

    public $modelName = 'Anh';

    function init() {
        parent::init();
        $this->pageTitle = $this->modelName;
    }

    function getFormAttr() {
        return array(
            '' => array(
                array(
                    'field' => 'name'
                ),
                array(
                    'field' => 'status',
                    'type' => 'select'
                )
            ),
        );
    }

    /**
     * Danh sách các trường dùng cho danh sách
     * @return array
     */
    public function getListAttr() {
        return array(
            'name',
            'status'
        );
    }

    /**
     * Danh sách các trường dùng cho lọc
     * @return array
     */
    public function getFilterAttr() {
        return array(
            array(
                'field' => 'name',
                'type' => 'search'
            ),
            array(
                'field' => 'status',
                'type' => 'select'
            ),
        );
    }

    public function getAttrData() {
        $rtn = parent::getAttrData();
        return $rtn;
    }

    // function actionIndex($layout = '') {
    //     if($layout != '')
    //     $this->layout = "//layouts/$layout";

    //     $this->render('index');
    // }

    function actionIndex()
    {
        $this->render('index');
    }

    function actionAdd() {
        $a = MyUtil::CheckFolder();
        $b = MyUtil::UploadFile('img', "file/$a");
        MyUtil::SaveImage('img', "file/f100/$a", 100);
        MyUtil::SaveImage('img', "file/f300/$a", 300);
        $anh = new TbImage();
        $anh->url = "$a/$b";
        $anh->save();
        $this->renderPartial("show", array('img' => $img));
    }

    function actionSearch() 
    {
        $search = new CDbCriteria();
        $search->compare('in_trash', 0);
        $search->addSearchCondition('url', $_REQUEST['search']);
        $img = TbImage::model()->findAll($search);
        $this->renderPartial("show", array('img' => $img));
    }


    function actionAdd1() {
        $img = TbImage::model()->Sort();
        $this->renderPartial("show", array('img' => $img));
    }

    function actionGetList() {
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $offset = ($page - 1) * $limit;
        $in_trash = $_GET['in_trash'];

        $img = new CDbCriteria();
        $img->order = 'id DESC';
        $img->compare('in_trash', $in_trash);
        if (isset($_GET['tukhoa'])) {
            $img->addSearchCondition('url', $_GET['tukhoa']);
        }
        $img->limit = $limit;
        $img->offset = $offset;
        $image = TbImage::model()->findAll($img);
        $numRec = TbImage::model()->count($img);
        $numPage = ($numRec % $limit) ? ($numRec / $limit + 1) : $numRec / $limit;
        $rtn = array();
        foreach ($image as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'url' => $item->url,
            );
        }
        $value = array('data' => $rtn, 'page' => $page, 'offset' => $offset, 'numPage' => $numPage, 'total' => $numRec);
        $this->renderJson("200", "thanh cong", $value);
    }

    function actionHidden() {
        $id = $_POST['id'];
        $bs = TbImage::model()->findByPk($id);
        $bs->in_trash = 1;
        $bs->update_at = time();
        $bs->save();
    }

    function actionUnHidden() {
        $id = $_POST['id'];
        $bs = TbImage::model()->findByPk($id);
        $bs->in_trash = 0;
        $bs->update_at = time();
        $bs->save();
    }

    function actionAddList() {
        $a = MyUtil::CheckFolder();
        $b = MyUtil::UploadFile('img', "file/$a");
        MyUtil::SaveImage('img', "file/f100/$a", 100);
        MyUtil::SaveImage('img', "file/f300/$a", 300);
        $anh = new TbImage();
        $anh->url = "$a/$b";
        $anh->created_at = time();
        if ($anh->save()) {
            echo 'da luu';
        } else {
            echo "chua luu";
        }
    }

}