<?php
/**
 * Admin Controller of anh module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 23/11/2021 08:51:28
 * @version 1.0
 */
class DefaultController extends FrontController 
{
    
    function init()
    {
        parent::init();
        $this->pageTitle = '';
    }

    function actionIndex()
    {
        $this->render("index");
    }
    
}