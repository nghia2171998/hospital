<div id='Ketqua1' style='width:100%;hieght:auto;display:block'>
    <div class='title' id='settime'>
        <nav class="navbar navbar-light bg-light">
            <div style='text-align: center' class="container-fluid">
                <div class="d-flex" v-if='in_trash==0'>
                    <input v-model='tukhoa' v-on:keyup.enter="fetchAllData()" class="form-control me-2" type="search" placeholder="Tìm kiếm" />
                    <button style='width:150px;' type="button" class="btn btn-secondary" onclick="reload()">Trở
                        lại</button>
                </div>
                <a v-if='in_trash==1' class="navbar-brand"></a>
                <p style='text-align: center'>SetUp Time</p>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-primary" v-on:click='fetchWeek(tuan=tuan-7)'>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/>
                        </svg>
                    </button>
                    <button type="button" class="btn btn-primary"  v-on:click='fetchWeek(tuan=tuan+7)'>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
                        </svg>
                    </button>
                </div>
            </div>
        </nav>
        <table class="table">

            <thead>
                <tr class="table-primary">
                    <th scope="col">STT</th>
                    <th scope="col" >Tên bác sĩ</th>
                    <th scope="col" v-bind:class="week.t2 ==  week.today ? 'col-active' : ''" >T2/{{week.t2}}</th>
                    <th scope="col" v-bind:class="week.t3 ==  week.today ? 'col-active' : ''">T3/{{week.t3}}</th>
                    <th scope="col" v-bind:class="week.t4 ==  week.today ? 'col-active' : ''">T4/{{week.t4}}</th>
                    <th scope="col" v-bind:class="week.t5 ==  week.today ? 'col-active' : ''">T5/{{week.t5}}</th>
                    <th scope="col" v-bind:class="week.t6 ==  week.today ? 'col-active' : ''">T6/{{week.t6}}</th>
                    <th scope="col" v-bind:class="week.t7 ==  week.today ? 'col-active' : ''">T7/{{week.t7}}</th>
                    <th scope="col" v-bind:class="week.cn ==  week.today ? 'col-active' : ''">CN/{{week.cn}}</th>
                </tr>
            </thead>

            <tr  v-for='row, index in allData'>
                <th scope="row">{{index+1}}</th>
                <td >{{row.ten}}</td>
                <td ><a @click='fetchData(row.id, 0, row.ten,  week.w)' data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight" style='display:flex'><p>S</p><p>C</p><p>T</p><p>Đ</p></a></td>
                <td ><a @click='fetchData(row.id, 1, row.ten, week.w)' data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight" style='display:flex'><p>S</p><p>C</p><p>T</p><p>Đ</p></a></td>
                <td ><a @click='fetchData(row.id, 2, row.ten, week.w)' data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight" style='display:flex'><p>S</p><p>C</p><p>T</p><p>Đ</p></a></td>
                <td ><a @click='fetchData(row.id, 3, row.ten, week.w)' data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight" style='display:flex'><p>S</p><p>C</p><p>T</p><p>Đ</p></a></td>
                <td ><a @click='fetchData(row.id, 4, row.ten, week.w)' data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight" style='display:flex'><p>S</p><p>C</p><p>T</p><p>Đ</p></a></td>
                <td ><a @click='fetchData(row.id, 5, row.ten, week.w)' data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight" style='display:flex'><p>S</p><p>C</p><p>T</p><p>Đ</p></a></td>
                <td ><a @click='fetchData(row.id, 6, row.ten, week.w)' data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight" style='display:flex'><p>S</p><p>C</p><p>T</p><p>Đ</p></a></td>
            </tr>
        </table>
        <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel" style="width:550px;height:936px;overflow: auto">
            <div class="offcanvas-header">
                <div style='display: block'>
                    <h5 id="offcanvasRightLabel">{{namebs}}</h5>
                    <b>{{day}}/{{month}}</b>
                </div>
                <div class="col-md-3">
                </div>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <div class="accordion" id="accordionPanelsStayOpenExample">
                    <div class="accordion-item settime_bs">
                        <h2 class="accordion-header" id="sang">
                            <button @click='fetchSchedule(week.w,"s")' class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#v_sang" aria-controls="panelsStayOpen-collapseTwo">
                                Sáng
                            </button>
                        </h2>
                        <form method='POST' id='myFormS'enctype="multipart/form-data">
                            <div id="v_sang" class="accordion-collapse collapse" aria-labelledby="sang">
                                <div class="accordion-body">
                                    <div class="accordion-body">
                                        <div class="set_bottom set_sang">
                                            <div class="row category">
                                                <div class="col-md-5">
                                                    <label for="inputState" class="form-label">Lịch</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <label for="inputState" class="form-label">Hạn đặt</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <a  class="add_time_s">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-square" viewBox="0 0 16 16">
                                                            <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                                            <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3 navbar">
                                            <b for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm">Khoảng thời gian chờ:</b>
                                            <div class="col-sm-1">
                                                <select id="Ccho" name='cho' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>0p</option>
                                                    <option value='5'>5p</option>
                                                    <option value='10'>10p</option>
                                                    <option value='15'>15p</option>
                                                    <option value='30'>30p</option>
                                                    <option value='45'>45p</option>
                                                    <option value='60'>1 giờ</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button" class="btn btn-primary btn-sm" @click='submitData("sang")'>Lưu</button>
                                            </div>
                                        </div>
                                        <div class="row mb-3 navbar">
                                            <div class="col-sm-6">
                                                <div class="row mb-4 form-switch navbar float-end">
                                                    <label class="form-check-label" for="flexSwitchCheckDefault">Mặc định</label>
                                                    <input class="form-check-input" name="mac_dinh" v-model="macdinh" type="checkbox" role="switch" id="macdinh" value="1"> 
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="row mb-4 form-switch navbar float-end">
                                                    <label class="form-check-label" for="flexSwitchCheckDefault">Tắt lịch</label>
                                                    <input class="form-check-input" name="tat_lich" v-model="tatlich" type="checkbox" role="switch" id="tatlich" value="1"> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Scheck0" value="0">
                                                <label class="form-check-label" for="inlineCheckbox1">T2</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Scheck1" value="1">
                                                <label class="form-check-label" for="inlineCheckbox2">T3</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Scheck2" value="2" >
                                                <label class="form-check-label" for="inlineCheckbox3">T4</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Scheck3" value="3">
                                                <label class="form-check-label" for="inlineCheckbox1">T5</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Scheck4" value="4">
                                                <label class="form-check-label" for="inlineCheckbox2">T6</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Scheck5" value="5" >
                                                <label class="form-check-label" for="inlineCheckbox3">T7</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Scheck6" value="6" >
                                                <label class="form-check-label" for="inlineCheckbox3">CN</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type='hidden' v-model='id' name='id'/>
                            <input type='hidden' v-model='w' name='w' />
                            <input type='hidden'value='s' name='session' />
                            <input type='hidden' v-model='day' name='day'/>
                            <input type='hidden' v-model='month' name='month'/>
                            <input type='hidden' v-model='year' name='year'/>
                            <input type='hidden' v-model='today' name='today'/>
                        </form>
                    </div>
                    <div class="accordion-item settime_bs">
                        <h2 class="accordion-header" id="chieu">
                            <button class="accordion-button collapsed" @click='fetchSchedule(week.w,"c")' type="button" data-bs-toggle="collapse" data-bs-target="#v_chieu" aria-expanded="false" aria-controls="panelsStayOpen-collapseTwo">
                                Chiều
                            </button>
                        </h2>
                        <form method='POST' id='myFormC'enctype="multipart/form-data">
                            <div id="v_chieu" class="accordion-collapse collapse" aria-labelledby="chieu">
                                <div class="accordion-body">
                                    <div class="accordion-body">
                                        <div class="set_bottom set_chieu">
                                            <div class="row category">
                                                <div class="col-md-5">
                                                    <label for="inputState" class="form-label">Lịch</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <label for="inputState" class="form-label">Hạn đặt</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <a  class="add_time_c">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-square" viewBox="0 0 16 16">
                                                            <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                                            <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3 navbar">
                                            <b for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm">Khoảng thời gian chờ:</b>
                                            <div class="col-sm-4">
                                                <select id="Ccho" name='Scho' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>0p</option>
                                                    <option value='5'>5p</option>
                                                    <option value='10'>10p</option>
                                                    <option value='15'>15p</option>
                                                    <option value='30'>30p</option>
                                                    <option value='45'>45p</option>
                                                    <option value='60'>1 giờ</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <button type="button" class="btn btn-primary btn-sm" @click='submitData("chieu")'>Lưu</button>
                                            </div>
                                        </div>
                                        <div class="row mb-3 navbar">
                                            <div class="col-sm-6">
                                                <div class="row mb-4 form-switch navbar float-end">
                                                    <label class="form-check-label" for="flexSwitchCheckDefault">Mặc định</label>
                                                    <input class="form-check-input" name="mac_dinh" v-model="macdinh" type="checkbox" role="switch" id="Cmacdinh" value="1"> 
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="row mb-4 form-switch navbar float-end">
                                                    <label class="form-check-label" for="flexSwitchCheckDefault">Tắt lịch</label>
                                                    <input class="form-check-input" name="tat_lich" v-model="tatlich" type="checkbox" role="switch" id="Ctatlich" value="1"> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Ccheck0" value="0">
                                                <label class="form-check-label" for="inlineCheckbox1">T2</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Ccheck1" value="1">
                                                <label class="form-check-label" for="inlineCheckbox2">T3</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Ccheck2" value="2" >
                                                <label class="form-check-label" for="inlineCheckbox3">T4</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Ccheck3" value="3">
                                                <label class="form-check-label" for="inlineCheckbox1">T5</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Ccheck4" value="4">
                                                <label class="form-check-label" for="inlineCheckbox2">T6</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Ccheck5" value="5" >
                                                <label class="form-check-label" for="inlineCheckbox3">T7</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Ccheck6" value="6" >
                                                <label class="form-check-label" for="inlineCheckbox3">CN</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type='hidden' v-model='id' name='id'/>
                            <input type='hidden' v-model='w' name='w' />
                            <input type='hidden'value='c' name='session' />
                            <input type='hidden' v-model='day' name='day'/>
                            <input type='hidden' v-model='month' name='month'/>
                            <input type='hidden' v-model='year' name='year'/>
                            <input type='hidden' v-model='today' name='today'/>
                        </form>
                    </div>
                    <div class="accordion-item settime_bs">
                        <h2 class="accordion-header" id="toi">
                            <button class="accordion-button collapsed" @click='fetchSchedule(week.w,"t")' type="button" data-bs-toggle="collapse" data-bs-target="#v_toi" aria-expanded="false" aria-controls="panelsStayOpen-collapseThree">
                                Tối
                            </button>
                        </h2>
                        <form method='POST' id='myFormT'enctype="multipart/form-data">
                            <div id="v_toi" class="accordion-collapse collapse" aria-labelledby="toi">
                                <div class="accordion-body">
                                    <div class="accordion-body">
                                        <div class="set_bottom set_toi">
                                            <div class="row category">
                                                <div class="col-md-5">
                                                    <label for="inputState" class="form-label">Lịch</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <label for="inputState" class="form-label">Hạn đặt</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <a  class="add_time_t">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-square" viewBox="0 0 16 16">
                                                            <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                                            <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3 navbar">
                                            <b for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm">Khoảng thời gian chờ:</b>
                                            <div class="col-sm-4">
                                                <select id="Tcho" name='cho' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>0p</option>
                                                    <option value='5'>5p</option>
                                                    <option value='10'>10p</option>
                                                    <option value='15'>15p</option>
                                                    <option value='30'>30p</option>
                                                    <option value='45'>45p</option>
                                                    <option value='60'>1 giờ</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <button type="button" class="btn btn-primary btn-sm" @click='submitData("toi")'>Lưu</button>
                                            </div>
                                        </div>
                                        <div class="row mb-3 navbar">
                                            <div class="col-sm-6">
                                                <div class="row mb-4 form-switch navbar float-end">
                                                    <label class="form-check-label" for="flexSwitchCheckDefault">Mặc định</label>
                                                    <input class="form-check-input" name="mac_dinh" v-model="macdinh" type="checkbox" role="switch" id="Tmacdinh" value="1"> 
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="row mb-4 form-switch navbar float-end">
                                                    <label class="form-check-label" for="flexSwitchCheckDefault">Tắt lịch</label>
                                                    <input class="form-check-input" name="tat_lich" v-model="tatlich" type="checkbox" role="switch" id="Ttatlich" value="1"> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Tcheck0" value="0">
                                                <label class="form-check-label" for="inlineCheckbox1">T2</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Tcheck1" value="1">
                                                <label class="form-check-label" for="inlineCheckbox2">T3</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Tcheck2" value="2" >
                                                <label class="form-check-label" for="inlineCheckbox3">T4</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Tcheck3" value="3">
                                                <label class="form-check-label" for="inlineCheckbox1">T5</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Tcheck4" value="4">
                                                <label class="form-check-label" for="inlineCheckbox2">T6</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Tcheck5" value="5" >
                                                <label class="form-check-label" for="inlineCheckbox3">T7</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="Tcheck6" value="6" >
                                                <label class="form-check-label" for="inlineCheckbox3">CN</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type='hidden' v-model='id' name='id'/>
                            <input type='hidden' v-model='w' name='w' />
                            <input type='hidden' value='t' name='session' />
                            <input type='hidden' v-model='day' name='day'/>
                            <input type='hidden' v-model='month' name='month'/>
                            <input type='hidden' v-model='year' name='year'/>
                            <input type='hidden' v-model='today' name='today'/>
                        </form>
                    </div>
                    <div class="accordion-item settime_bs">
                        <h2 class="accordion-header" id="dem">
                            <button class="accordion-button collapsed" @click='fetchSchedule(week.w,"d")' type="button" data-bs-toggle="collapse" data-bs-target="#v_dem" aria-expanded="false" aria-controls="panelsStayOpen-collapseThree">
                                Đêm
                            </button>
                        </h2>
                        <form method='POST' id='myFormD'enctype="multipart/form-data">
                            <div id="v_dem" class="accordion-collapse collapse" aria-labelledby="dem">
                                <div class="accordion-body">
                                    <div class="accordion-body">
                                        <div class="set_bottom set_dem">
                                            <div class="row category">
                                                <div class="col-md-5">
                                                    <label for="inputState" class="form-label">Lịch</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <label for="inputState" class="form-label">Hạn đặt</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <a  class="add_time_d">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-square" viewBox="0 0 16 16">
                                                            <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                                            <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3 navbar">
                                            <b for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm">Khoảng thời gian chờ:</b>
                                            <div class="col-sm-4">
                                                <select id="Dcho" name='cho' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>0p</option>
                                                    <option value='5'>5p</option>
                                                    <option value='10'>10p</option>
                                                    <option value='15'>15p</option>
                                                    <option value='30'>30p</option>
                                                    <option value='45'>45p</option>
                                                    <option value='60'>1 giờ</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <button type="button" class="btn btn-primary btn-sm" @click='submitData("dem")'>Lưu</button>
                                            </div>
                                        </div>
                                        <div class="row mb-3 navbar">
                                            <div class="col-sm-6">
                                                <div class="row mb-4 form-switch navbar float-end">
                                                    <label class="form-check-label" for="flexSwitchCheckDefault">Mặc định</label>
                                                    <input class="form-check-input" name="mac_dinh" v-model="macdinh" type="checkbox" role="switch" id="Dmacdinh" value="1"> 
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="row mb-4 form-switch navbar float-end">
                                                    <label class="form-check-label" for="flexSwitchCheckDefault">Tắt lịch</label>
                                                    <input class="form-check-input" name="tat_lich" v-model="tatlich" type="checkbox" role="switch" id="Dtatlich" value="1"> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="check0" value="0">
                                                <label class="form-check-label" for="inlineCheckbox1">T2</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="check1" value="1">
                                                <label class="form-check-label" for="inlineCheckbox2">T3</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="check2" value="2" >
                                                <label class="form-check-label" for="inlineCheckbox3">T4</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="check3" value="3">
                                                <label class="form-check-label" for="inlineCheckbox1">T5</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="check4" value="4">
                                                <label class="form-check-label" for="inlineCheckbox2">T6</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="check5" value="5" >
                                                <label class="form-check-label" for="inlineCheckbox3">T7</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name='t[]' id="check6" value="6" >
                                                <label class="form-check-label" for="inlineCheckbox3">CN</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type='hidden' v-model='id' name='id'/>
                            <input type='hidden' v-model='w' name='w' />
                            <input type='hidden' value='d' name='session' />
                            <input type='hidden' v-model='day' name='day'/>
                            <input type='hidden' v-model='month' name='month'/>
                            <input type='hidden' v-model='year' name='year'/>
                            <input type='hidden' v-model='today' name='today'/>
                        </form>
                    </div>
                </div>
            </div> 
        </div>
        <div class="text-center" v-if="pageList.length > 1">
            <button v-if='in_trash==0' class="btn btn-info mr-2" v-bind:class="page ==  current_page ? 'btn-dark' : ''"
                    v-for="page in pageList" v-on:click="fetchAllData(page)">{{page}}</button>
        </div>
    </div>
</div>
<script>
    var application = new Vue({
        el: '#settime',
        data: {
            allData: '',
            namebs: '',
            myModel: false,
            select: '',
            tukhoa: '',
            day: '',
            month: '',
            year: '',
            id: '',
            in_trash: 0,
            pageList: [],
            limit: 5,
            current_page: 1,
            actionButton: 'Up',
            w: 0,
            week: '',
            ngay: '',
            tuan: 0,
            today: '',
            macdinh: '',
            tatlich: '',
        },
        methods: {
            fetchAllData: function (page = 1) {
                if (page) {
                    application.current_page = page;
                }
                $.ajax({
                    url: "<?php echo Yii::app()->createUrl('SetTimeBS/admin/GetList') ?>",
                    type: 'GET',
                    data: {
                        limit: 5,
                        in_trash: 0,
                        tukhoa: application.tukhoa,
                        page: application.current_page,
                    },
                    success: function (res) {
                        application.allData = res.data.data;
                        var page_number = Math.ceil(res.data.total / application.limit);
                        application.pageList = [];
                        for (var i = 1; i <= page_number; i++) {
                            application.pageList.push(i);
                        }
                    }
                });
            },
            fetchWeek: function (tuan) {
                if (tuan) {
                    application.tuan = tuan;
                }

                $.ajax({
                    url: "<?php echo Yii::app()->createUrl('SetTimeBS/admin/GetWeek') ?>",
                    type: 'GET',
                    data: {
                        w: application.tuan,
                    },
                }).then(function (res) {
                    application.week = res.data;
                });
            },
            fetchData: function (id, day, name, w) {
                $('.accordion-collapse').removeClass('show');
                $('.detail_lich').remove();
                for (var i = 0; i <= 6; i++) {
                    document.getElementById(`Scheck${i}`).checked = false;
                    document.getElementById(`Ccheck${i}`).checked = false;
                    document.getElementById(`Tcheck${i}`).checked = false;
                    document.getElementById(`check${i}`).checked = false;
                }
                $.ajax({
                    url: "<?php echo Yii::app()->createUrl('SetTimeBS/admin/getOneList') ?>",
                    type: 'GET',
                    data: {
                        id: id,
                        day: day,
                        week: w,
                    },
                }).then(function (res) {
                    application.namebs = name;
                    application.day = res.data.day;
                    application.month = res.data.month;
                    application.year = res.data.year;
                    application.id = id;
                    application.ngay = day;
                    application.w = w;
                    application.today = res.data.today;
                });
            },
            fetchSchedule: function (w, s) {
                if (s == 's') {
                    $('.timeS').remove();
                } else if (s == 'c') {
                    $('.timeC').remove();
                } else if (s == 't') {
                    $('.timeT').remove();
                } else if (s == 'd') {
                    $('.timeD').remove();
                }
                $.ajax({
                    url: "<?php echo Yii::app()->createUrl('SetTimeBS/admin/getSchedule') ?>",
                    type: 'GET',
                    data: {
                        id: application.id,
                        week: w,
                        day: application.ngay,
                        session: s,
                    },
                }).then(function (res) {
                    document.getElementById(`macdinh`).checked = false;
                    document.getElementById(`tatlich`).checked = false;
                    if (res.data.length > 0) {
                        for (var i = 0; i < res.data.length; i++) {
                            if (s == 's') {
                                for (var j = 0; j <= 6; j++) {
                                    document.getElementById(`Scheck${j}`).checked = false;
                                }
                                if (res.data[i].blackday == 2) {
                                    document.getElementById(`tatlich`).checked = true;
                                } else {
                                    $('.set_sang').append(`<div class="row detail_lich timeS">
                                        <div class="col-md-5" style='display: flex'>
                                            <select id="Sgio${i}" name='gio[]' class="form-select form-select-sm select_1">
                                                <option value='5' selected>5</option>
                                                <option value='6'>6</option>
                                                <option value='7'>7</option>
                                                <option value='8'>8</option>
                                                <option value='9'>9</option>
                                                <option value='10'>10</option>
                                                <option value='11'>11</option>
                                            </select>
                                            <p> : </p>
                                            <select id="Sphut${i}" name='phut[]' class="form-select form-select-sm select_1">
                                                <option value='0' selected>00</option>
                                                <option value='5'>05</option>
                                                <option value='10'>10</option>
                                                <option value='15'>15</option>
                                                <option value='20'>20</option>
                                                <option value='25'>25</option>
                                                <option value='30'>30</option>
                                                <option value='35'>35</option>
                                                <option value='40'>40</option>
                                                <option value='45'>45</option>
                                                <option value='50'>50</option>
                                                <option value='55'>55</option>
                                            </select>
                                            <p>+</p>
                                            <select id="Stime${i}" name='time[]' class="form-select form-select-sm select_1">
                                                <option value='10' selected>10p</option>
                                                <option value='15'>15p</option>
                                                <option value='30'>30p</option>
                                                <option value='45'>45p</option>
                                                <option value='60'>1h</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5" style='display: flex'>
                                            <select id="Sthu${i}" name='thu[]' class="form-select form-select-sm select_1">
                                                <option value='6' selected>CN</option>
                                                <option value='0'>T2</option>
                                                <option value='1'>T3</option>
                                                <option value='2'>T4</option>
                                                <option value='3'>T5</option>
                                                <option value='4'>T6</option>
                                                <option value='5'>T7</option>
                                            </select>
                                            <p>-</p>
                                            <select id="Shan_gio${i}" name='han_gio[]' class="form-select form-select-sm select_1">
                                                <option value='0' selected>0</option>
                                                <option value='1'>1</option>
                                                <option value='2'>2</option>
                                                <option value='3'>3</option>
                                                <option value='4'>4</option>
                                                <option value='5'>5</option>
                                                <option value='6'>6</option>
                                                <option value='7'>7</option>
                                                <option value='8'>8</option>
                                                <option value='9'>9</option>
                                                <option value='10'>10</option>
                                                <option value='11'>11</option>
                                                <option value='12'>12</option>
                                                <option value='13'>13</option>
                                                <option value='14'>14</option>
                                                <option value='15'>15</option>
                                                <option value='16'>16</option>
                                                <option value='17'>17</option>
                                                <option value='18'>18</option>
                                                <option value='19'>19</option>
                                                <option value='20'>20</option>
                                                <option value='21'>21</option>
                                                <option value='22'>22</option>
                                                <option value='23'>23</option>
                                            </select>
                                            <p> : </p>
                                            <select id="Shan_phut${i}" name='han_phut[]' class="form-select form-select-sm select_1">
                                                <option value='0' selected>00</option>
                                                <option value='5'>05</option>
                                                <option value='10'>10</option>
                                                <option value='15'>15</option>
                                                <option value='20'>20</option>
                                                <option value='25'>25</option>
                                                <option value='30'>30</option>
                                                <option value='35'>35</option>
                                                <option value='40'>40</option>
                                                <option value='45'>45</option>
                                                <option value='50'>50</option>
                                                <option value='55'>55</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <a  class="remove-btn">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash-square" viewBox="0 0 16 16">
                                                    <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                                    <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                                                </svg>
                                            </a>
                                        </div>
                                    </div>`);
                                    $(`#Sgio${i}`).val(res.data[i].hour);
                                    $(`#Sphut${i}`).val(res.data[i].minute);
                                    $(`#Stime${i}`).val(res.data[i].time);
                                    $(`#Sthu${i}`).val(res.data[i].han_thu);
                                    $(`#Shan_gio${i}`).val(res.data[i].han_gio);
                                    $(`#Shan_phut${i}`).val(res.data[i].han_phut);
                                    $(`#Scho`).val(res.data[0].timedate);
                                }
                                if (res.data[0].blackday == 0) {
                                    document.getElementById(`macdinh`).checked = true;
                                }
                                document.getElementById(`Scheck${res.data[0].weekday}`).checked = true;
                            } else if (s == 'c') {
                                for (var j = 0; j <= 6; j++) {
                                    document.getElementById(`Ccheck${j}`).checked = false;
                                }
                                if (res.data[i].blackday == 2) {
                                    document.getElementById(`Ctatlich`).checked = true;
                                } else {
                                    $('.set_chieu').append(`<div class="row detail_lich timeC">
                                            <div class="col-md-5" style='display: flex'>
                                                <select id="Cgio${i}" name='gio[]' class="form-select form-select-sm select_1">
                                                    <option value='12' selected>12</option>
                                                    <option value='13'>13</option>
                                                    <option value='14'>14</option>
                                                    <option value='15'>15</option>
                                                    <option value='16'>16</option>                     
                                                </select>
                                                <p> : </p>
                                                <select id="Cphut${i}" name='phut[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>00</option>
                                                    <option value='5'>05</option>
                                                    <option value='10'>10</option>
                                                    <option value='15'>15</option>
                                                    <option value='20'>20</option>
                                                    <option value='25'>25</option>
                                                    <option value='30'>30</option>
                                                    <option value='35'>35</option>
                                                    <option value='40'>40</option>
                                                    <option value='45'>45</option>
                                                    <option value='50'>50</option>
                                                    <option value='55'>55</option>
                                                </select>
                                                <p>+</p>
                                                <select id="Ctime${i}" name='time[]' class="form-select form-select-sm select_1">
                                                    <option value='10' selected>10p</option>
                                                    <option value='15'>15p</option>
                                                    <option value='30'>30p</option>
                                                    <option value='45'>45p</option>
                                                    <option value='60'>1h</option>
                                                </select>
                                            </div>
                                            <div class="col-md-5" style='display: flex'>
                                                <select id="Cthu${i}" name='thu[]' class="form-select form-select-sm select_1">
                                                    <option value='6' selected>CN</option>
                                                    <option value='0'>T2</option>
                                                    <option value='1'>T3</option>
                                                    <option value='2'>T4</option>
                                                    <option value='3'>T5</option>
                                                    <option value='4'>T6</option>
                                                    <option value='5'>T7</option>
                                                </select>
                                                <p>-</p>
                                                <select id="Chan_gio${i}" name='han_gio[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>0</option>
                                                    <option value='1'>1</option>
                                                    <option value='2'>2</option>
                                                    <option value='3'>3</option>
                                                    <option value='4'>4</option>
                                                    <option value='5'>5</option>
                                                    <option value='6'>6</option>
                                                    <option value='7'>7</option>
                                                    <option value='8'>8</option>
                                                    <option value='9'>9</option>
                                                    <option value='10'>10</option>
                                                    <option value='11'>11</option>
                                                    <option value='12'>12</option>
                                                    <option value='13'>13</option>
                                                    <option value='14'>14</option>
                                                    <option value='15'>15</option>
                                                    <option value='16'>16</option>
                                                    <option value='17'>17</option>
                                                    <option value='18'>18</option>
                                                    <option value='19'>19</option>
                                                    <option value='20'>20</option>
                                                    <option value='21'>21</option>
                                                    <option value='22'>22</option>
                                                    <option value='23'>23</option>
                                                </select>
                                                <p> : </p>
                                                <select id="Chan_phut${i}" name='han_phut[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>00</option>
                                                    <option value='5'>05</option>
                                                    <option value='10'>10</option>
                                                    <option value='15'>15</option>
                                                    <option value='20'>20</option>
                                                    <option value='25'>25</option>
                                                    <option value='30'>30</option>
                                                    <option value='35'>35</option>
                                                    <option value='40'>40</option>
                                                    <option value='45'>45</option>
                                                    <option value='50'>50</option>
                                                    <option value='55'>55</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <a  class="remove-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash-square" viewBox="0 0 16 16">
                                                        <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                                        <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>`);
                                    $(`#Cgio${i}`).val(res.data[i].hour);
                                    $(`#Cphut${i}`).val(res.data[i].minute);
                                    $(`#Ctime${i}`).val(res.data[i].time);
                                    $(`#Cthu${i}`).val(res.data[i].han_thu);
                                    $(`#Chan_gio${i}`).val(res.data[i].han_gio);
                                    $(`#Chan_phut${i}`).val(res.data[i].han_phut);
                                    $(`#Ccho`).val(res.data[0].timedate);
                                }
                                document.getElementById(`Ccheck${res.data[0].weekday}`).checked = true;
                                if (res.data[0].blackday == 0) {
                                    document.getElementById(`Cmacdinh`).checked = true;
                                }
                            } else if (s == 't') {
                                for (var j = 0; j <= 6; j++) {
                                    document.getElementById(`Tcheck${j}`).checked = false;
                                }
                                if (res.data[i].blackday == 2) {
                                    document.getElementById(`Ttatlich`).checked = true;
                                } else {
                                    $('.set_toi').append(`<div class="row detail_lich timeT">
                                            <div class="col-md-5" style='display: flex'>
                                                <select id="Tgio${i}" name='gio[]' class="form-select form-select-sm select_1">
                                                    <option value='17' selected>17</option>
                                                    <option value='18'>18</option>
                                                    <option value='19'>19</option>
                                                </select>
                                                <p> : </p>
                                                <select id="Tphut${i}" name='phut[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>00</option>
                                                    <option value='5'>05</option>
                                                    <option value='10'>10</option>
                                                    <option value='15'>15</option>
                                                    <option value='20'>20</option>
                                                    <option value='25'>25</option>
                                                    <option value='30'>30</option>
                                                    <option value='35'>35</option>
                                                    <option value='40'>40</option>
                                                    <option value='45'>45</option>
                                                    <option value='50'>50</option>
                                                    <option value='55'>55</option>
                                                </select>
                                                <p>+</p>
                                                <select id="Ttime${i}" name='time[]' class="form-select form-select-sm select_1">
                                                    <option value='10' selected>10p</option>
                                                    <option value='15'>15p</option>
                                                    <option value='30'>30p</option>
                                                    <option value='45'>45p</option>
                                                    <option value='60'>1h</option>
                                                </select>
                                            </div>
                                            <div class="col-md-5" style='display: flex'>
                                                <select id="Tthu${i}" name='thu[]' class="form-select form-select-sm select_1">
                                                    <option value='6' selected>CN</option>
                                                    <option value='0'>T2</option>
                                                    <option value='1'>T3</option>
                                                    <option value='2'>T4</option>
                                                    <option value='3'>T5</option>
                                                    <option value='4'>T6</option>
                                                    <option value='5'>T7</option>
                                                </select>
                                                <p>-</p>
                                                <select id="Than_gio${i}" name='han_gio[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>0</option>
                                                    <option value='1'>1</option>
                                                    <option value='2'>2</option>
                                                    <option value='3'>3</option>
                                                    <option value='4'>4</option>
                                                    <option value='5'>5</option>
                                                    <option value='6'>6</option>
                                                    <option value='7'>7</option>
                                                    <option value='8'>8</option>
                                                    <option value='9'>9</option>
                                                    <option value='10'>10</option>
                                                    <option value='11'>11</option>
                                                    <option value='12'>12</option>
                                                    <option value='13'>13</option>
                                                    <option value='14'>14</option>
                                                    <option value='15'>15</option>
                                                    <option value='16'>16</option>
                                                    <option value='17'>17</option>
                                                    <option value='18'>18</option>
                                                    <option value='19'>19</option>
                                                    <option value='20'>20</option>
                                                    <option value='21'>21</option>
                                                    <option value='22'>22</option>
                                                    <option value='23'>23</option>
                                                </select>
                                                <p> : </p>
                                                <select id="Than_phut${i}" name='han_phut[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>00</option>
                                                    <option value='5'>05</option>
                                                    <option value='10'>10</option>
                                                    <option value='15'>15</option>
                                                    <option value='20'>20</option>
                                                    <option value='25'>25</option>
                                                    <option value='30'>30</option>
                                                    <option value='35'>35</option>
                                                    <option value='40'>40</option>
                                                    <option value='45'>45</option>
                                                    <option value='50'>50</option>
                                                    <option value='55'>55</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <a  class="remove-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash-square" viewBox="0 0 16 16">
                                                        <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                                        <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>`);
                                    $(`#Tgio${i}`).val(res.data[i].hour);
                                    $(`#Tphut${i}`).val(res.data[i].minute);
                                    $(`#Ttime${i}`).val(res.data[i].time);
                                    $(`#Tthu${i}`).val(res.data[i].han_thu);
                                    $(`#Than_gio${i}`).val(res.data[i].han_gio);
                                    $(`#Than_phut${i}`).val(res.data[i].han_phut);
                                    $(`#Tcho`).val(res.data[0].timedate);
                                }
                                document.getElementById(`Tcheck${res.data[0].weekday}`).checked = true;
                                if (res.data[0].blackday == 0) {
                                    document.getElementById(`Tmacdinh`).checked = true;
                                }
                            } else if (s == 'd') {
                                for (var j = 0; j <= 6; j++) {
                                    document.getElementById(`check${j}`).checked = false;
                                }
                                if (res.data[i].blackday == 2) {
                                    document.getElementById(`Dtatlich`).checked = true;
                                } else {
                                    $('.set_dem').append(`<div class="row detail_lich timeD">
                                            <div class="col-md-5" style='display: flex'>
                                                <select id="Dgio${i}" name='gio[]' class="form-select form-select-sm select_1">
                                                    <option value='20' selected>20</option>
                                                    <option value='21'>21</option>
                                                    <option value='22'>22</option>
                                                    <option value='23'>23</option>
                                                    <option value='0'>0</option>
                                                    <option value='1'>1</option>
                                                    <option value='2'>2</option>
                                                    <option value='3'>3</option>
                                                    <option value='4'>4</option>
                                                </select>
                                                <p> : </p>
                                                <select id="Dphut${i}" name='phut[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>00</option>
                                                    <option value='5'>05</option>
                                                    <option value='10'>10</option>
                                                    <option value='15'>15</option>
                                                    <option value='20'>20</option>
                                                    <option value='25'>25</option>
                                                    <option value='30'>30</option>
                                                    <option value='35'>35</option>
                                                    <option value='40'>40</option>
                                                    <option value='45'>45</option>
                                                    <option value='50'>50</option>
                                                    <option value='55'>55</option>
                                                </select>
                                                <p>+</p>
                                                <select id="Dtime${i}" name='time[]' class="form-select form-select-sm select_1">
                                                    <option value='10' selected>10p</option>
                                                    <option value='15'>15p</option>
                                                    <option value='30'>30p</option>
                                                    <option value='45'>45p</option>
                                                    <option value='60'>1h</option>
                                                </select>
                                            </div>
                                            <div class="col-md-5" style='display: flex'>
                                                <select id="Dthu${i}" name='thu[]' class="form-select form-select-sm select_1">
                                                    <option value='6' selected>CN</option>
                                                    <option value='0'>T2</option>
                                                    <option value='1'>T3</option>
                                                    <option value='2'>T4</option>
                                                    <option value='3'>T5</option>
                                                    <option value='4'>T6</option>
                                                    <option value='5'>T7</option>
                                                </select>
                                                <p>-</p>
                                                <select id="Dhan_gio${i}" name='han_gio[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>0</option>
                                                    <option value='1'>1</option>
                                                    <option value='2'>2</option>
                                                    <option value='3'>3</option>
                                                    <option value='4'>4</option>
                                                    <option value='5'>5</option>
                                                    <option value='6'>6</option>
                                                    <option value='7'>7</option>
                                                    <option value='8'>8</option>
                                                    <option value='9'>9</option>
                                                    <option value='10'>10</option>
                                                    <option value='11'>11</option>
                                                    <option value='12'>12</option>
                                                    <option value='13'>13</option>
                                                    <option value='14'>14</option>
                                                    <option value='15'>15</option>
                                                    <option value='16'>16</option>
                                                    <option value='17'>17</option>
                                                    <option value='18'>18</option>
                                                    <option value='19'>19</option>
                                                    <option value='20'>20</option>
                                                    <option value='21'>21</option>
                                                    <option value='22'>22</option>
                                                    <option value='23'>23</option>
                                                </select>
                                                <p> : </p>
                                                <select id="Dhan_phut${i}" name='han_phut[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>00</option>
                                                    <option value='5'>05</option>
                                                    <option value='10'>10</option>
                                                    <option value='15'>15</option>
                                                    <option value='20'>20</option>
                                                    <option value='25'>25</option>
                                                    <option value='30'>30</option>
                                                    <option value='35'>35</option>
                                                    <option value='40'>40</option>
                                                    <option value='45'>45</option>
                                                    <option value='50'>50</option>
                                                    <option value='55'>55</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <a  class="remove-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash-square" viewBox="0 0 16 16">
                                                        <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                                        <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>`);
                                    $(`#Dgio${i}`).val(res.data[i].hour);
                                    $(`#Dphut${i}`).val(res.data[i].minute);
                                    $(`#Dtime${i}`).val(res.data[i].time);
                                    $(`#Dthu${i}`).val(res.data[i].han_thu);
                                    $(`#Dhan_gio${i}`).val(res.data[i].han_gio);
                                    $(`#Dhan_phut${i}`).val(res.data[i].han_phut);
                                    $(`#Dcho`).val(res.data[0].timedate);
                                }
                                document.getElementById(`check${res.data[0].weekday}`).checked = true;
                                if (res.data[0].blackday == 0) {
                                    document.getElementById(`Dmacdinh`).checked = true;
                                }
                            }
                        }
                    }
                });
            },
            submitData: function (s) {
                if (application.actionButton == 'Up') {
                    if (s == 'sang') {
                        var form_data = new FormData(document.getElementById("myFormS"));
                    } else if (s == 'chieu') {
                        var form_data = new FormData(document.getElementById("myFormC"));
                    } else if (s == 'toi') {
                        var form_data = new FormData(document.getElementById("myFormT"));
                    } else if (s == 'dem') {
                        var form_data = new FormData(document.getElementById("myFormD"));
                    }
                    $.ajax({
                        url: "<?php echo Yii::app()->createUrl('SetTimeBS/admin/UpList') ?>",
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: form_data,
                    }).then(function (res) {
                        console.log('thành công');
                    });
                }
            },
        },
    });
    application.fetchAllData();
    application.fetchWeek(w = 0);
    $('.add_time_s').click(function () {
        $('.set_sang').append(`<div class="row detail_lich timeS">
                                        <div class="col-md-5" style='display: flex'>
                                            <select id="gio" name='gio[]' class="form-select form-select-sm select_1">
                                                <option value='5' selected>5</option>
                                                <option value='6'>6</option>
                                                <option value='7'>7</option>
                                                <option value='8'>8</option>
                                                <option value='9'>9</option>
                                                <option value='10'>10</option>
                                                <option value='11'>11</option>
                                            </select>
                                            <p> : </p>
                                            <select id="phut" name='phut[]' class="form-select form-select-sm select_1">
                                                <option value='0' selected>00</option>
                                                <option value='5'>05</option>
                                                <option value='10'>10</option>
                                                <option value='15'>15</option>
                                                <option value='20'>20</option>
                                                <option value='25'>25</option>
                                                <option value='30'>30</option>
                                                <option value='35'>35</option>
                                                <option value='40'>40</option>
                                                <option value='45'>45</option>
                                                <option value='50'>50</option>
                                                <option value='55'>55</option>
                                            </select>
                                            <p>+</p>
                                            <select id="time" name='time[]' class="form-select form-select-sm select_1">
                                                <option value='10' selected>10p</option>
                                                <option value='15'>15p</option>
                                                <option value='30'>30p</option>
                                                <option value='45'>45p</option>
                                                <option value='60'>1h</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5" style='display: flex'>
                                            <select id="time" name='thu[]' class="form-select form-select-sm select_1">
                                                <option value='6' selected>CN</option>
                                                <option value='0'>T2</option>
                                                <option value='1'>T3</option>
                                                <option value='2'>T4</option>
                                                <option value='3'>T5</option>
                                                <option value='4'>T6</option>
                                                <option value='5'>T7</option>
                                            </select>
                                            <p>-</p>
                                            <select id="gio" name='han_gio[]' class="form-select form-select-sm select_1">
                                                <option value='0' selected>0</option>
                                                <option value='1'>1</option>
                                                <option value='2'>2</option>
                                                <option value='3'>3</option>
                                                <option value='4'>4</option>
                                                <option value='5'>5</option>
                                                <option value='6'>6</option>
                                                <option value='7'>7</option>
                                                <option value='8'>8</option>
                                                <option value='9'>9</option>
                                                <option value='10'>10</option>
                                                <option value='11'>11</option>
                                                <option value='12'>12</option>
                                                <option value='13'>13</option>
                                                <option value='14'>14</option>
                                                <option value='15'>15</option>
                                                <option value='16'>16</option>
                                                <option value='17'>17</option>
                                                <option value='18'>18</option>
                                                <option value='19'>19</option>
                                                <option value='20'>20</option>
                                                <option value='21'>21</option>
                                                <option value='22'>22</option>
                                                <option value='23'>23</option>
                                            </select>
                                            <p> : </p>
                                            <select id="phut" name='han_phut[]' class="form-select form-select-sm select_1">
                                                <option value='0' selected>00</option>
                                                <option value='5'>05</option>
                                                <option value='10'>10</option>
                                                <option value='15'>15</option>
                                                <option value='20'>20</option>
                                                <option value='25'>25</option>
                                                <option value='30'>30</option>
                                                <option value='35'>35</option>
                                                <option value='40'>40</option>
                                                <option value='45'>45</option>
                                                <option value='50'>50</option>
                                                <option value='55'>55</option>
                                            </select>
                                        </div>

                                        <div class="col-md-2">
                                            <a  class="remove-btn">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash-square" viewBox="0 0 16 16">
                                                    <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                                    <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                                                </svg>
                                            </a>
                                        </div>
                                    </div>`);
    });
    $('.add_time_c').click(function () {
        $('.set_chieu').append(`<div class="row detail_lich timeC">
                                            <div class="col-md-5" style='display: flex'>
                                                <select id="gio" name='gio[]' class="form-select form-select-sm select_1">
                                                    <option value='12' selected>12</option>
                                                    <option value='13'>13</option>
                                                    <option value='14'>14</option>
                                                    <option value='15'>15</option>
                                                    <option value='16'>16</option>                     
                                                </select>
                                                <p> : </p>
                                                <select id="phut" name='phut[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>00</option>
                                                    <option value='5'>05</option>
                                                    <option value='10'>10</option>
                                                    <option value='15'>15</option>
                                                    <option value='20'>20</option>
                                                    <option value='25'>25</option>
                                                    <option value='30'>30</option>
                                                    <option value='35'>35</option>
                                                    <option value='40'>40</option>
                                                    <option value='45'>45</option>
                                                    <option value='50'>50</option>
                                                    <option value='55'>55</option>
                                                </select>
                                                <p>+</p>
                                                <select id="time" name='time[]' class="form-select form-select-sm select_1">
                                                    <option value='10' selected>10p</option>
                                                    <option value='15'>15p</option>
                                                    <option value='30'>30p</option>
                                                    <option value='45'>45p</option>
                                                    <option value='60'>1h</option>
                                                </select>
                                            </div>
                                            <div class="col-md-5" style='display: flex'>
                                                <select id="time" name='thu[]' class="form-select form-select-sm select_1">
                                                    <option value='6' selected>CN</option>
                                                    <option value='0'>T2</option>
                                                    <option value='1'>T3</option>
                                                    <option value='2'>T4</option>
                                                    <option value='3'>T5</option>
                                                    <option value='4'>T6</option>
                                                    <option value='5'>T7</option>
                                                </select>
                                                <p>-</p>
                                                <select id="gio" name='han_gio[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>0</option>
                                                    <option value='1'>1</option>
                                                    <option value='2'>2</option>
                                                    <option value='3'>3</option>
                                                    <option value='4'>4</option>
                                                    <option value='5'>5</option>
                                                    <option value='6'>6</option>
                                                    <option value='7'>7</option>
                                                    <option value='8'>8</option>
                                                    <option value='9'>9</option>
                                                    <option value='10'>10</option>
                                                    <option value='11'>11</option>
                                                    <option value='12'>12</option>
                                                    <option value='13'>13</option>
                                                    <option value='14'>14</option>
                                                    <option value='15'>15</option>
                                                    <option value='16'>16</option>
                                                    <option value='17'>17</option>
                                                    <option value='18'>18</option>
                                                    <option value='19'>19</option>
                                                    <option value='20'>20</option>
                                                    <option value='21'>21</option>
                                                    <option value='22'>22</option>
                                                    <option value='23'>23</option>
                                                </select>
                                                <p> : </p>
                                                <select id="phut" name='han_phut[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>00</option>
                                                    <option value='5'>05</option>
                                                    <option value='10'>10</option>
                                                    <option value='15'>15</option>
                                                    <option value='20'>20</option>
                                                    <option value='25'>25</option>
                                                    <option value='30'>30</option>
                                                    <option value='35'>35</option>
                                                    <option value='40'>40</option>
                                                    <option value='45'>45</option>
                                                    <option value='50'>50</option>
                                                    <option value='55'>55</option>
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <a  class="remove-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash-square" viewBox="0 0 16 16">
                                                        <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                                        <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>`);
    });
    $('.add_time_t').click(function () {
        $('.set_toi').append(`<div class="row detail_lich timeT">
                                            <div class="col-md-5" style='display: flex'>
                                                <select id="gio" name='gio[]' class="form-select form-select-sm select_1">
                                                    <option value='17' selected>17</option>
                                                    <option value='18'>18</option>
                                                    <option value='19'>19</option>
                                                </select>
                                                <p> : </p>
                                                <select id="phut" name='phut[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>00</option>
                                                    <option value='5'>05</option>
                                                    <option value='10'>10</option>
                                                    <option value='15'>15</option>
                                                    <option value='20'>20</option>
                                                    <option value='25'>25</option>
                                                    <option value='30'>30</option>
                                                    <option value='35'>35</option>
                                                    <option value='40'>40</option>
                                                    <option value='45'>45</option>
                                                    <option value='50'>50</option>
                                                    <option value='55'>55</option>
                                                </select>
                                                <p>+</p>
                                                <select id="time" name='time[]' class="form-select form-select-sm select_1">
                                                    <option value='10' selected>10p</option>
                                                    <option value='15'>15p</option>
                                                    <option value='30'>30p</option>
                                                    <option value='45'>45p</option>
                                                    <option value='60'>1h</option>
                                                </select>
                                            </div>
                                            <div class="col-md-5" style='display: flex'>
                                                <select id="time" name='thu[]' class="form-select form-select-sm select_1">
                                                    <option value='6' selected>CN</option>
                                                    <option value='0'>T2</option>
                                                    <option value='1'>T3</option>
                                                    <option value='2'>T4</option>
                                                    <option value='3'>T5</option>
                                                    <option value='4'>T6</option>
                                                    <option value='5'>T7</option>
                                                </select>
                                                <p>-</p>
                                                <select id="gio" name='han_gio[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>0</option>
                                                    <option value='1'>1</option>
                                                    <option value='2'>2</option>
                                                    <option value='3'>3</option>
                                                    <option value='4'>4</option>
                                                    <option value='5'>5</option>
                                                    <option value='6'>6</option>
                                                    <option value='7'>7</option>
                                                    <option value='8'>8</option>
                                                    <option value='9'>9</option>
                                                    <option value='10'>10</option>
                                                    <option value='11'>11</option>
                                                    <option value='12'>12</option>
                                                    <option value='13'>13</option>
                                                    <option value='14'>14</option>
                                                    <option value='15'>15</option>
                                                    <option value='16'>16</option>
                                                    <option value='17'>17</option>
                                                    <option value='18'>18</option>
                                                    <option value='19'>19</option>
                                                    <option value='20'>20</option>
                                                    <option value='21'>21</option>
                                                    <option value='22'>22</option>
                                                    <option value='23'>23</option>
                                                </select>
                                                <p> : </p>
                                                <select id="phut" name='han_phut[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>00</option>
                                                    <option value='5'>05</option>
                                                    <option value='10'>10</option>
                                                    <option value='15'>15</option>
                                                    <option value='20'>20</option>
                                                    <option value='25'>25</option>
                                                    <option value='30'>30</option>
                                                    <option value='35'>35</option>
                                                    <option value='40'>40</option>
                                                    <option value='45'>45</option>
                                                    <option value='50'>50</option>
                                                    <option value='55'>55</option>
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <a  class="remove-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash-square" viewBox="0 0 16 16">
                                                        <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                                        <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>`);
    });
    $('.add_time_d').click(function () {
        $('.set_dem').append(`<div class="row detail_lich timeD">
                                            <div class="col-md-5" style='display: flex'>
                                                <select id="gio" name='gio[]' class="form-select form-select-sm select_1">
                                                    <option value='20' selected>20</option>
                                                    <option value='21'>21</option>
                                                    <option value='22'>22</option>
                                                    <option value='23'>23</option>
                                                    <option value='0'>0</option>
                                                    <option value='1'>1</option>
                                                    <option value='2'>2</option>
                                                    <option value='3'>3</option>
                                                    <option value='4'>4</option>
                                                </select>
                                                <p> : </p>
                                                <select id="phut" name='phut[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>00</option>
                                                    <option value='5'>05</option>
                                                    <option value='10'>10</option>
                                                    <option value='15'>15</option>
                                                    <option value='20'>20</option>
                                                    <option value='25'>25</option>
                                                    <option value='30'>30</option>
                                                    <option value='35'>35</option>
                                                    <option value='40'>40</option>
                                                    <option value='45'>45</option>
                                                    <option value='50'>50</option>
                                                    <option value='55'>55</option>
                                                </select>
                                                <p>+</p>
                                                <select id="time" name='time[]' class="form-select form-select-sm select_1">
                                                    <option value='10' selected>10p</option>
                                                    <option value='15'>15p</option>
                                                    <option value='30'>30p</option>
                                                    <option value='45'>45p</option>
                                                    <option value='60'>1h</option>
                                                </select>
                                            </div>
                                            <div class="col-md-5" style='display: flex'>
                                                <select id="time" name='thu[]' class="form-select form-select-sm select_1">
                                                    <option value='6' selected>CN</option>
                                                    <option value='0'>T2</option>
                                                    <option value='1'>T3</option>
                                                    <option value='2'>T4</option>
                                                    <option value='3'>T5</option>
                                                    <option value='4'>T6</option>
                                                    <option value='5'>T7</option>
                                                </select>
                                                <p>-</p>
                                                <select id="gio" name='han_gio[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>0</option>
                                                    <option value='1'>1</option>
                                                    <option value='2'>2</option>
                                                    <option value='3'>3</option>
                                                    <option value='4'>4</option>
                                                    <option value='5'>5</option>
                                                    <option value='6'>6</option>
                                                    <option value='7'>7</option>
                                                    <option value='8'>8</option>
                                                    <option value='9'>9</option>
                                                    <option value='10'>10</option>
                                                    <option value='11'>11</option>
                                                    <option value='12'>12</option>
                                                    <option value='13'>13</option>
                                                    <option value='14'>14</option>
                                                    <option value='15'>15</option>
                                                    <option value='16'>16</option>
                                                    <option value='17'>17</option>
                                                    <option value='18'>18</option>
                                                    <option value='19'>19</option>
                                                    <option value='20'>20</option>
                                                    <option value='21'>21</option>
                                                    <option value='22'>22</option>
                                                    <option value='23'>23</option>
                                                </select>
                                                <p> : </p>
                                                <select id="phut" name='han_phut[]' class="form-select form-select-sm select_1">
                                                    <option value='0' selected>00</option>
                                                    <option value='5'>05</option>
                                                    <option value='10'>10</option>
                                                    <option value='15'>15</option>
                                                    <option value='20'>20</option>
                                                    <option value='25'>25</option>
                                                    <option value='30'>30</option>
                                                    <option value='35'>35</option>
                                                    <option value='40'>40</option>
                                                    <option value='45'>45</option>
                                                    <option value='50'>50</option>
                                                    <option value='55'>55</option>
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <a  class="remove-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash-square" viewBox="0 0 16 16">
                                                        <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                                        <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>`);
    });
    $(".settime_bs").on("click", ".remove-btn", function (e) {
        $(this).parents('.row').remove();
    });
    function reload() {
        application.select = '';
        application.tukhoa = '';
        application.fetchAllData();
    }
</script>
