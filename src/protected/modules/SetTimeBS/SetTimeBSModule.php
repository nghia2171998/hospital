<?php
/**
 * SetTimeBS module
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 10/02/2022 03:00:17
 * @version 1.0
 */
class SetTimeBSModule extends MyModule
{

    public function init()
    {        
        /**
        * $this->setImport(array(
        *    'SetTimeBS.models.*',
        *    'SetTimeBS.components.*',
        * ));
        */
    }
    
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {     
            return true;
        }
        else
        {
            return false;
        }    
    }

}
