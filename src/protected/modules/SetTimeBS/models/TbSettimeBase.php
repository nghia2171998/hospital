<?php

/**
 * Đây là lớp model của bảng "tb_settime".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since 27/03/2022 16:44:20
 * @version 1.0
 *
 * Dưới đây là các cột của bảng 'tb_settime':
 * @property integer $id
 * @property integer $id_bs
 * @property integer $hour
 * @property integer $minute
 * @property integer $time
 * @property string $name
 * @property integer $weekday
 * @property integer $weekyear
 * @property integer $day
 * @property integer $month
 * @property integer $year
 * @property integer $timedate
 * @property string $session_day
 * @property integer $han_thu
 * @property integer $han_gio
 * @property integer $han_phut
 * @property integer $black_day
 * @property integer $trang_thai
 */
class TbSettimeBase extends MyModel
{

    public $modelName = 'TbSettime';

    /**
     * @return string trả về tên của bảng
     */
    public function tableName()
    {
        return 'tb_settime';
    }

    /**
     * @return array Các quy định về thuộc tính của lớp.
     */
    public function rules()
    {
        // LƯU Ý: bạn chỉ nên định nghĩa các quy định cho các trường
        // mà người dùng sẽ thể nhập liệu    
        return array(
            array('id_bs, hour, minute, time, name, weekday, weekyear, timedate, session_day, han_thu, han_gio, han_phut, black_day, trang_thai', 'required'),
            array('id_bs, hour, minute, time, weekday, weekyear, day, month, year, timedate, han_thu, han_gio, han_phut, black_day, trang_thai', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max' => 100),
            array('session_day', 'length', 'max' => 5),
            // Những thuộc tính dưới đây để phục vụ cho hàm search().            
            // @todo Hãy xóa đi những trường mà bạn không muốn cho người dùng tìm kiếm
            array('id, id_bs, hour, minute, time, name, weekday, weekyear, day, month, year, timedate, session_day, han_thu, han_gio, han_phut, black_day, trang_thai', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array mối quan hệ với các bảng khác.
     */
    public function relations()
    {
        // LƯU Ý: Bạn có thể cần phải điều chỉnh tên các quan hệ 
        // và các lớp liên quan được tự động sinh ra bên dưới đây        
        $rtn = parent::relations();
        return $rtn;
    }

    /**
     * @return array Tùy chỉnh tên các thuộc tính (name=>label)
     */
    public function attributeLabels()
    {
        $rtn = parent::attributeLabels();
        $rtn['id'] = 'ID';
        $rtn['id_bs'] = 'Id Bs';
        $rtn['hour'] = 'Hour';
        $rtn['minute'] = 'Minute';
        $rtn['time'] = 'Time';
        $rtn['name'] = 'Name';
        $rtn['weekday'] = 'Weekday';
        $rtn['weekyear'] = 'Weekyear';
        $rtn['day'] = 'Day';
        $rtn['month'] = 'Month';
        $rtn['year'] = 'Year';
        $rtn['timedate'] = 'Timedate';
        $rtn['session_day'] = 'Session Day';
        $rtn['han_thu'] = 'Han Thu';
        $rtn['han_gio'] = 'Han Gio';
        $rtn['han_phut'] = 'Han Phut';
        $rtn['black_day'] = 'Black Day';
        $rtn['trang_thai'] = 'Trang Thai';
        return $rtn;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TbSettime the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}