<?php

/**
 * Admin Controller of SetTimeBS module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 10/02/2022 03:00:17
 * @version 1.0
 */
class AdminController extends BackController {

    public $modelName = 'SetTimeBS';

    function init() {
        parent::init();
        $this->pageTitle = $this->modelName;
    }

    function getFormAttr() {
        return array(
            '' => array(
                array(
                    'field' => 'name'
                ),
                array(
                    'field' => 'status',
                    'type' => 'select'
                )
            ),
        );
    }

    /**
     * Danh sách các trường dùng cho danh sách
     * @return array
     */
    public function getListAttr() {
        return array(
            'name',
            'status'
        );
    }

    /**
     * Danh sách các trường dùng cho lọc
     * @return array
     */
    public function getFilterAttr() {
        return array(
            array(
                'field' => 'name',
                'type' => 'search'
            ),
            array(
                'field' => 'status',
                'type' => 'select'
            ),
        );
    }

    public function getAttrData() {
        $rtn = parent::getAttrData();
        return $rtn;
    }

    function actionIndex() {
        $this->render("index");
    }

    function actionGetList() {
        $title = 'CK';
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $offset = ($page - 1) * $limit;
        $in_trash = $_GET['in_trash'];
        $bs = new CDbCriteria();
        $bs->order = 'id DESC';
        $bs->compare('in_trash', $in_trash);
        if (isset($_GET['select']) != '') {
            $bs->compare('trang_thai', $_GET['select']);
        }
        if (isset($_GET['tukhoa'])) {
            $bs->addSearchCondition('ten', $_GET['tukhoa']);
        }
        $bs->limit = $limit;
        $bs->offset = $offset;
        $bacsi = TbBacsi::model()->findAll($bs);
        $numRec = TbBacsi::model()->count($bs);
        $numPage = ($numRec % $limit) ? ($numRec / $limit + 1) : $numRec / $limit;
        $rtn = array();
        foreach ($bacsi as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'ten' => $item->ten,
                'han_dat' => $item->han_dat,
            );
        }
        $value = array('data' => $rtn, 'page' => $page, 'offset' => $offset, 'numPage' => $numPage, 'total' => $numRec);
        $this->renderJson("200", "thanh cong", $value);
    }

    function actionGetWeek() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $w = $_GET['w'];
        $ddate = date('Y-m-d');
        $year = date('Y');
        $duedt = explode("-", $ddate);
        $date = mktime(0, 0, 0, $duedt[1], $duedt[2] + $w, $duedt[0]);
        $week = (int) date('W', $date);
        $rtn = array();
        $rtn = array(
            'w' => $week,
            't2' => date('d/m', strtotime((0) . 'day', strtotime(sprintf("%4dW%02d", $year, $week)))),
            't3' => date('d/m', strtotime((1) . 'day', strtotime(sprintf("%4dW%02d", $year, $week)))),
            't4' => date('d/m', strtotime((2) . 'day', strtotime(sprintf("%4dW%02d", $year, $week)))),
            't5' => date('d/m', strtotime((3) . 'day', strtotime(sprintf("%4dW%02d", $year, $week)))),
            't6' => date('d/m', strtotime((4) . 'day', strtotime(sprintf("%4dW%02d", $year, $week)))),
            't7' => date('d/m', strtotime((5) . 'day', strtotime(sprintf("%4dW%02d", $year, $week)))),
            'cn' => date('d/m', strtotime((6) . 'day', strtotime(sprintf("%4dW%02d", $year, $week)))),
            'today' => date('d/m'),
            'date' => date('d/m/Y', strtotime((0) . 'day', strtotime(sprintf("%4dW%02d", $year, $week)))),
        );
        $this->renderJson("200", "thanh cong", $rtn);
    }

    function actiongetOneList() {
        $id = $_GET['id'];
        $day = $_GET['day'];
        $week = $_GET['week'];
        $year = date('Y');
        $t = new CDbCriteria();
        $t->compare('id_bs', $id);
        $t->compare('weekday', $day);
        $time = TbSettime::model()->findAll($t);
        $rtn = array();
        foreach ($time as $item) {
            $rtn['data'][] = array(
                'id' => $item->id,
                'hour' => $item->hour,
                'minute' => $item->minute,
                'time' => $item->time,
                'session_day' => $item->session_day,
                'weekday' => $item->weekday,
            );
        }
        $rtn['day'] = date('d', strtotime(($day) . 'day', strtotime(sprintf("%4dW%02d", $year, $week))));
        $rtn['month'] = date('m', strtotime(($day) . 'day', strtotime(sprintf("%4dW%02d", $year, $week))));
        $rtn['year'] = date('Y', strtotime(($day) . 'day', strtotime(sprintf("%4dW%02d", $year, $week))));
        $dayofweek = date('w', strtotime(($day) . 'day', strtotime(sprintf("%4dW%02d", $year, $week)))) - 1;
        if ($dayofweek == -1) {
            $dayofweek = 6;
        }
        $rtn['today'] = $dayofweek;
        $this->renderJson("200", "thanh cong", $rtn);
    }

    function actiongetSchedule() {
        $day = date('d', strtotime(($_GET['day']) . 'day', strtotime(sprintf("%4dW%02d", 2022, $_GET['week']))));
        $month = date('m', strtotime(($_GET['day']) . 'day', strtotime(sprintf("%4dW%02d", 2022, $_GET['week']))));
        $year = date('Y', strtotime(($_GET['day']) . 'day', strtotime(sprintf("%4dW%02d", 2022, $_GET['week']))));
        $l = new CDbCriteria();
        $l->compare('id_bs', $_GET['id']);
        $l->compare('weekday', $_GET['day']);
        $l->compare('session_day', $_GET['session']);
        $l->compare('day', $day);
        $l->compare('month', $month);
        $l->compare('year', $year);
        $count = (int) TbSettime::model()->count($l);
        if ($count >= 1) {
            $lich = TbSettime::model()->findAll($l);
        } else {
            $t = new CDbCriteria();
            $t->compare('id_bs', $_GET['id']);
            $t->compare('weekday', $_GET['day']);
            $t->compare('session_day', $_GET['session']);
            $t->compare('black_day', 0);
            $lich = TbSettime::model()->findAll($t);
        }
        $rtn = array();
        foreach ($lich as $item) {
            $rtn[] = array(
                'id' => $item->id,
                'hour' => $item->hour,
                'minute' => $item->minute,
                'time' => $item->time,
                'timedate' => $item->timedate,
                'session_day' => $item->session_day,
                'weekday' => $item->weekday,
                'han_thu' => $item->han_thu,
                'han_gio' => $item->han_gio,
                'han_phut' => $item->han_phut,
                'blackday' => $item->black_day,
            );
        }
        $this->renderJson("200", "thanh cong", $rtn);
    }

    function actionGetdata() {
        $arr = $_GET;
        echo count($arr['cho']);
        for ($i = 0; $i < count($arr['cho']); $i++) {
            echo $arr['cho'][$i];
            echo $arr['gio'][$i];
            echo $arr['phut'][$i];
            echo $arr['time'][$i];
        }
    }

    function actionUpList() {
        $arr = $_POST;
        if (isset($arr['mac_dinh']) && isset($arr['tat_lich'])) {
            foreach ($arr['t'] as $t) {
                $bs = new CDbCriteria();
                $bs->compare('id_bs', $arr['id']);
                $bs->compare('weekday', $t);
                $bs->compare('session_day', $arr['session']);
                TbSettime::model()->deleteAll($bs);
            }
        } else if (isset($arr['tat_lich'])) {
            foreach ($arr['t'] as $t) {
                $day = date('d', strtotime(($t) . 'day', strtotime(sprintf("%4dW%02d", $arr['year'], $arr['w']))));
                $month = date('m', strtotime(($t) . 'day', strtotime(sprintf("%4dW%02d", $arr['year'], $arr['w']))));
                $year = date('Y', strtotime(($t) . 'day', strtotime(sprintf("%4dW%02d", $arr['year'], $arr['w']))));
                $bs = new CDbCriteria();
                $bs->compare('id_bs', $arr['id']);
                $bs->compare('weekday', $t);
                $bs->compare('day', $day);
                $bs->compare('session_day', $arr['session']);
                $bs->compare('month', $month);
                $bs->compare('year', $year);
                TbSettime::model()->deleteAll($bs);
                $time = new TbSettime();
                $time->id_bs = $arr['id'];
                $time->hour = 0;
                $time->minute = 0;
                $time->time = 0;
                $time->name = 0;
                $time->weekday = $t;
                $time->weekyear = $arr['w'];
                $time->timedate = 0;
                $time->session_day = $arr['session'];
                $time->day = $day;
                $time->month = $month;
                $time->year = $year;
                $time->han_thu = 0;
                $time->han_gio = 0;
                $time->han_phut = 0;
                $time->black_day = 2;
                $time->trang_thai = 0;
                $time->save();
            }
        } else if (isset($arr['mac_dinh'])) {
            $a = 0;
            foreach ($arr['t'] as $t) {
                $bs = new CDbCriteria();
                $bs->compare('id_bs', $arr['id']);
                $bs->compare('weekday', $t);
                $bs->compare('session_day', $arr['session']);
                TbSettime::model()->deleteAll($bs);
                if (isset($arr['gio'])) {
                    $i = 0;
                    foreach ($arr['gio'] as $item) {
                        $a = $arr['today'] - $arr['thu'][$i];
                        if ($a < 0) {
                            $a = $a + 7;
                        }
                        $han = $t - $a;
                        if ($han < 0) {
                            $han = $han + 7;
                        }
                        $time = new TbSettime();
                        $a = MyUtil::count_time($arr['gio'][$i], $arr['phut'][$i], $arr['time'][$i]);
                        $time->id_bs = $arr['id'];
                        $time->hour = $arr['gio'][$i];
                        $time->minute = $arr['phut'][$i];
                        $time->time = $arr['time'][$i];
                        $time->name = $a;
                        $time->weekday = $t;
                        $time->weekyear = $arr['w'];
                        $time->timedate = $arr['cho'];
                        $time->session_day = $arr['session'];
                        $time->han_thu = $han;
                        $time->han_gio = $arr['han_gio'][$i];
                        $time->han_phut = $arr['han_phut'][$i];
                        $time->black_day = 0;
                        $time->trang_thai = 0;
                        $time->save();
                        $i++;
                    }
                }
            }
        } else if (!isset($arr['mac_dinh']) && !isset($arr['tat_lich'])) {
            $a = 0;
            foreach ($arr['t'] as $t) {
                $day = date('d', strtotime(($t) . 'day', strtotime(sprintf("%4dW%02d", $arr['year'], $arr['w']))));
                $month = date('m', strtotime(($t) . 'day', strtotime(sprintf("%4dW%02d", $arr['year'], $arr['w']))));
                $year = date('Y', strtotime(($t) . 'day', strtotime(sprintf("%4dW%02d", $arr['year'], $arr['w']))));
                $bs = new CDbCriteria();
                $bs->compare('id_bs', $arr['id']);
                $bs->compare('weekday', $t);
                $bs->compare('black_day', 1);
                $bs->compare('day', $day);
                $bs->compare('session_day', $arr['session']);
                $bs->compare('month', $month);
                $bs->compare('year', $year);
                TbSettime::model()->deleteAll($bs);
                if (isset($arr['gio'])) {
                    $i = 0;
                    foreach ($arr['gio'] as $item) {
                        $a = $arr['today'] - $arr['thu'][$i];
                        if ($a < 0) {
                            $a = $a + 7;
                        }
                        $han = $t - $a;
                        if ($han < 0) {
                            $han = $han + 7;
                        }
                        $time = new TbSettime();
                        $a = MyUtil::count_time($arr['gio'][$i], $arr['phut'][$i], $arr['time'][$i]);
                        $time->id_bs = $arr['id'];
                        $time->hour = $arr['gio'][$i];
                        $time->minute = $arr['phut'][$i];
                        $time->time = $arr['time'][$i];
                        $time->name = $a;
                        $time->weekday = $t;
                        $time->weekyear = $arr['w'];
                        $time->day = $day;
                        $time->month = $month;
                        $time->year = $year;
                        $time->timedate = $arr['cho'];
                        $time->session_day = $arr['session'];
                        $time->han_thu = $han;
                        $time->han_gio = $arr['han_gio'][$i];
                        $time->han_phut = $arr['han_phut'][$i];
                        $time->black_day = 1;
                        $time->trang_thai = 0;
                        $time->save();
                        $i++;
                    }
                }
            }
        }
    }

}
