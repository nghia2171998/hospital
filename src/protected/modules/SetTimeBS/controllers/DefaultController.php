<?php
/**
 * Admin Controller of SetTimeBS module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since 10/02/2022 03:00:17
 * @version 1.0
 */
class DefaultController extends FrontController 
{
    
    function init()
    {
        parent::init();
        $this->pageTitle = '';
    }

    function actionIndex()
    {
        $this->render("index");
    }
    
}