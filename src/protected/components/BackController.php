<?php

session_start();

/**
 * Controller FrontController
 *
 * @author Nguyen Manh Luu <luu.nguyen@voithan.com>
 * @since 13 thg 5, 2020 10:48:38
 * @version 1.0
 */
class BackController extends CController {

    public $breadcrumbs = array();
    public $leng = 1;

    function init() {
        parent::init();

        $this->breadcrumbs = array(
            'Admin' => Yii::app()->baseUrl . "/admin"
        );
        Yii::app()->theme = "2005_admin";
        // if (isset($_SESSION['User']) {
        //     Yii::app()->theme = "2005_admin";
        // } else {
        //     $this->redirect(Yii::app()->baseUrl . "/taikhoan");
        // }
    }

    public $code = 200;
    public $message = "";
    public $data = array();
    public $response = array();

    function _response() {
        $this->response['code'] = $this->code;
        $this->response['message'] = $this->message;
        if (!empty($this->data)) {
            $this->response['data'] = $this->data;
        }

        header('Content-Type: application/json');
        echo json_encode($this->response);
        Yii::app()->end();
    }

    function renderJson($code, $message, $data = array()) {
        $this->code = $code;
        $this->message = $message;
        $this->data = $data;
        $this->_response();
    }
    function actionIndex() {
        $this->render('//layouts/crud');
    }

}