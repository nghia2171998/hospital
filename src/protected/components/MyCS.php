<?php

/**
 * My Client Script
 *
 * @author Nguyen Manh Luu <luu.nguyen@voithan.com>
 * @since Oct 14, 2016 5:20:03 PM
 * @version 1.0
 */
class MyCS
{

    static $css = array();
    static $css_string = "";
    static $js_files = array();
    static $js_string = "";

    static function js($string, $p = 4, $htmlOptions = array())
    {
        $id = crc32($string);
        Yii::app()->clientScript->registerScript($id, $string, $p, $htmlOptions);
    }

    static function jsFunction($string)
    {
        $id = crc32($string);
        Yii::app()->clientScript->registerScript($id, $string, CClientScript::POS_END);
    }

    static function jsFile($file, $p = 2)
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->controller->assetDir . "$file", $p);
    }

    static function jsFileRemote($file, $p = 2)
    {
        Yii::app()->clientScript->registerScriptFile($file, $p);
    }

    static function jsThuvien($file, $p = 2)
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . "/assets/thuvien/$file", $p);
    }

    static function css($string)
    {
        $id = crc32($string);
        Yii::app()->clientScript->registerCss($id, $string);
    }

    static function cssFile($file, $main = FALSE, $version = 0)
    {
        if ($main)
        {
            $path = Yii::app()->basePath . "/.." . Yii::app()->controller->assetDir;
            $id = crc32($path . $file . $version);
            $css = Yii::app()->cache->get($id);
            if ($css === false)
            {
                $string = file_get_contents($path . $file);
                $css = Minify_CSS_Compressor::process($string);
                Yii::app()->cache->set($id, $css, 2592000); // Lưu 1 tháng
            }

            Yii::app()->clientScript->registerCss($id, $css);
        }
        else
        {
            Yii::app()->clientScript->registerCssFile(Yii::app()->controller->assetDir . "$file");
        }
    }

    static function cssThuvien($file, $main = TRUE, $version = 0)
    {
        if ($main)
        {
            $path = Yii::app()->basePath . "/../assets/thuvien/";
            $id = crc32($path . $file . $version);
            $css = Yii::app()->cache->get($id);
            if ($css === false)
            {
                $string = file_get_contents($path . $file);
                $css = Minify_CSS_Compressor::process($string);
                Yii::app()->cache->set($id, $css, 2592000); // Lưu 1 tháng
            }

            Yii::app()->clientScript->registerCss($id, $css);
        }
        else
        {
            self::$css[] = Yii::app()->baseUrl . "/assets/thuvien/$file";
        }
    }

    static function cssFileRemote($file)
    {
        self::$css[] = $file;
    }

    static function getCssFiles()
    {
        return self::$css;
    }

    /**
     * Registers a meta tag that will be inserted in the head section (right before the title element) of the resulting page.
     *
     * <b>Note:</b>
     * Each call of this method will cause a rendering of new meta tag, even if their attributes are equal.
     *
     * <b>Example:</b>
     * <pre>
     *    $cs->registerMetaTag('example', 'description', null, array('lang' => 'en'));
     *    $cs->registerMetaTag('beispiel', 'description', null, array('lang' => 'de'));
     * </pre>
     * @param string $content content attribute of the meta tag
     * @param string $name name attribute of the meta tag. If null, the attribute will not be generated
     * @param string $httpEquiv http-equiv attribute of the meta tag. If null, the attribute will not be generated
     * @param array $options other options in name-value pairs (e.g. 'scheme', 'lang')
     * @param string $id Optional id of the meta tag to avoid duplicates
     * @return static the CClientScript object itself (to support method chaining, available since version 1.1.5).
     */
    static function metaTag($content, $name = NULL, $httpEquiv = NULL, $options = array(), $id = NULL)
    {
        Yii::app()->clientScript->registerMetaTag($content, $name, $httpEquiv, $options, $id);
    }

    /**
     * Registers a link tag that will be inserted in the head section (right before the title element) of the resulting page.
     * @param string $relation rel attribute of the link tag. If null, the attribute will not be generated.
     * @param string $type type attribute of the link tag. If null, the attribute will not be generated.
     * @param string $href href attribute of the link tag. If null, the attribute will not be generated.
     * @param string $media media attribute of the link tag. If null, the attribute will not be generated.
     * @param array $options other options in name-value pairs
     * @return static the CClientScript object itself (to support method chaining, available since version 1.1.5).
     */
    public static function linkTag($relation = NULL, $type = NULL, $href = NULL, $media = NULL, $options = array())
    {
        Yii::app()->clientScript->registerLinkTag($relation, $type, $href, $media, $options);
    }

}
