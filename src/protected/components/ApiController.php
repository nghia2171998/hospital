<?php

/**
 * Controller MyController
 *
 * @author Nguyen Manh Luu <luu.nguyen@voithan.com>
 * @since 24-10-2017 09:58:11
 * @version 1.0
 */
class ApiController extends CController
{

    function init()
    {
        parent::init();
        $this->validIp();
    }

    function validIp()
    {
        $white_list = array(
            '127.0.0.1', '::1', '112.213.87.197', '103.1.239.212'
        );

        if (!in_array(Yii::app()->request->userHostAddress, $white_list))
        {
            throw new CHttpException(401, 'Your IP address is invalid');
        }
    }

    public $code = 200;
    public $message = "";
    public $data = array();
    public $response = array();
    function _response()
    {
        $this->response['code'] = $this->code;
        $this->response['message'] = $this->message;
        if (!empty($this->data))
        {
            $this->response['data'] = $this->data;
        }

        header('Content-Type: application/json');
        echo json_encode($this->response);
        Yii::app()->end();
    }

    function renderJson($code, $message, $data = array())
    {
        $this->code = $code;
        $this->message = $message;
        $this->data = $data;

        $this->_response();
    }

}
