<?php

/**
 * Controller FrontController
 *
 * @author Nguyen Manh Luu <luu.nguyen@voithan.com>
 * @since 13 thg 5, 2020 10:48:38
 * @version 1.0
 */
class FrontController extends CController
{
    public $pageTitle = "";

    function init()
    {
        parent::init();

        Yii::app()->theme = "2005";

    }
    public $breadcrumbs=array();
    public $breadcrumbs1= array();  
    public $code = 200;
    public $message = "";
    public $data = array();
    public $response = array();

    function _response() {
        $this->response['code'] = $this->code;
        $this->response['message'] = $this->message;
        if (!empty($this->data)) {
            $this->response['data'] = $this->data;
        }

        header('Content-Type: application/json');
        echo json_encode($this->response);
        Yii::app()->end();
    }

    function renderJson($code, $message, $data = array()) {
        $this->code = $code;
        $this->message = $message;
        $this->data = $data;
        $this->_response();
    }

}