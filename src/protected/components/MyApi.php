<?php

/**
 * Description of MyApi
 *
 * @author Nguyen Manh Luu <luu.nguyen@voithan.com>
 */
class MyApi
{

    static function post($url, $fields, $header = array())
    {
        return self::makeRequest($url, "POST", $fields, $header);
    }

    static function get($url, $fields = array(), $header = array())
    {
        return self::makeRequest($url, "GET", $fields, $header);
    }

    static function makeRequest($url, $method, $fields = array(), $header = array())
    {
        if ($method == 'GET')
        {
            if (!empty($fields))
            {
                $fieldsString = http_build_query($fields);
                $url .= ('?' . $fieldsString);
            }
        }

        $curl = curl_init($url);
        if ($method == 'POST')
        {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
            curl_setopt($curl, CURLOPT_POST, count($fields));
            curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($fields)));
        }
        elseif ($method != 'GET')
        {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
            if (!empty($fields))
            {
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($fields));
            }
        }

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        $output = curl_exec($curl);

        curl_close($curl);

        return $output;
    }

}
