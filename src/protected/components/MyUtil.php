<?php

class MyUtil {

    static function to_slug($str) {
        $str = preg_replace('/(á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ,)/', 'a', $str);
        $str = preg_replace('/(é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ)/', 'e', $str);
        $str = preg_replace('/(í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị)/', 'i', $str);
        $str = preg_replace('/(ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ)/', 'o', $str);
        $str = preg_replace('/(ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự)/', 'u', $str);
        $str = preg_replace('/(ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ)/', 'y', $str);
        $str = preg_replace('/(đ|Đ)/', 'd', $str);
        $str = preg_replace('/[^a-zA-Z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        return $str;
    }

    static function UploadFile($inputName, $Folder) {
        require_once Yii::app()->basePath . '/extension/EPhpThumb/EPhpThumb/lib/phpThumb/src/ThumbLib.inc.php';
        if ($_FILES[$inputName]["error"] != 0)
            return "";
        $filename = "";
        $tmp_name = "";
        $filename = $_FILES[$inputName]["name"];
        $tmp_name = $_FILES[$inputName]["tmp_name"];
        $arr = explode(".", $filename);
        $file_ext = strtolower(end($arr));
        $thumb = PhpThumbFactory::create($tmp_name);
        $thumb->save("$Folder/$filename");
        return $filename;
    }

    static function SaveImage($inputName, $folder, $size = "") {
        MyUtil::CheckFolder("f$size/");
        require_once Yii::app()->basePath . '/extension/EPhpThumb/EPhpThumb/lib/phpThumb/src/ThumbLib.inc.php';
        if ($_FILES[$inputName]["error"] != 0)
            return "";
        $filename = "";
        $tmp_name = "";
        $filename = $_FILES[$inputName]["name"];
        $tmp_name = $_FILES[$inputName]["tmp_name"];
        $arr = explode(".", $filename);
        $file_ext = strtolower(end($arr));
        $thumb = PhpThumbFactory::create($tmp_name);
        $thumb->resize($size, $size);
        $thumb->save("$folder/$filename");
    }

    static function CheckFolder($file = "") {
        $target = '';
        $folder = "file/";
        $year = date("Y");
        $month = date("m");
        $day = date("d");
        ;
        $f_folder = $target . $file;
        $year_folder = $file . $year;
        $month_folder = $year_folder . '/' . $month;
        $day_folder = $month_folder . '/' . $day;
        !file_exists($folder) && mkdir($folder, 0777);
        !file_exists("file/" . $f_folder) && mkdir("file/" . $f_folder, 0777);
        !file_exists("file/" . $year_folder) && mkdir("file/" . $year_folder, 0777);
        !file_exists("file/" . $month_folder) && mkdir("file/" . $month_folder, 0777);
        !file_exists("file/" . $day_folder) && mkdir("file/" . $day_folder, 0777);
        return $target = $day_folder;
    }

   function senmail($ten, $email, $mk1) {

        // require "PHPMailer-5.2-stable/class.phpmailer.php";
        require 'PHPMailer/PHPMailerAutoload.php';
        require 'PHPMailer/class.phpmailer.php';

        // Khai báo tạo PHPMailer
        $mail = new PHPMailer();

        //Khai báo gửi mail bằng SMTP
        $mail->IsSMTP();

        //Tắt mở kiểm tra lỗi trả về, chấp nhận các giá trị 0 1 2
        // 0 = off không thông báo bất kì gì, tốt nhất nên dùng khi đã hoàn thành.
        // 1 = Thông báo lỗi ở client
        // 2 = Thông báo lỗi cả client và lỗi ở server

        $mail->SMTPDebug = 2;
        $mail->CharSet = 'UTF-8';
        $mail->Debugoutput = "html"; // Lỗi trả về hiển thị với cấu trúc HTML
        $mail->Host = "smtp.gmail.com"; //host smtp để gửi mail
        $mail->Port = 587; // cổng để gửi mail
        $mail->SMTPSecure = "tls"; //Phương thức mã hóa thư - ssl hoặc tls
        $mail->SMTPAuth = true; //Xác thực SMTP
        $mail->Username = "longhero10x@gmail.com"; // Tên đăng nhập tài khoản Gmail
        $mail->Password = "CTi4QePKVruc6ik"; //Mật khẩu của gmail
        $mail->SetFrom("longhero10x@gmail.com", "Hospital"); // Thông tin người gửi
        $mail->AddReplyTo("longhero10x@gmail.com", "Hospital Reply"); // Ấn định email sẽ nhận khi người dùng reply lại.
        $mail->AddAddress("$email", "$ten"); //Email của người nhận
        $mail->Subject = "Cập nhật tài khoản thành công"; //Tiêu đề của thư
        $mail->MsgHTML("<b>Tên đăng nhập là:</b> $ten <br /> <b>Mật khẩu là:</b> $mk1 <br /> <p>Vui lòng đăng nhập tại https://medkee.com/taikhoan </p>"); //Nội dung của bức thư.
// $mail->MsgHTML(file_get_contents("email-template.html"), dirname(__FILE__));
// Gửi thư với tập tin html
// $mail->AltBody = "nội dung rút gọn thư ";//Nội dung rút gọn hiển thị bên ngoài thư mục thư.
// $mail->AddAttachment("./../1.php");//Tập tin cần attach
//Tiến hành gửi email và kiểm tra lỗi
        if (!$mail->Send()) {
            echo "Có lỗi khi gửi mail: " . $mail->ErrorInfo;
        } else {
            echo "Đã gửi thư thành công!";
        }
    }
    
    public function rules() {
        return [
            ['email', 'checkMail']
        ];
    }

    public function mahoa($mk1) {
        $slat = '@#$qwert@#$';
        $sl = '*123#';
        $bm = md5($sl . $mk1 . $slat);
        return $bm;
    }

    public function kiemtra($tk, $a, $b) {
        foreach ($tk as $row) {
            if ($row['username'] == $a || $row['email'] == $b) {
                return 0;
                break;
            }
        }
        return 1;
    }

    public function checkDM($dm) {
        $DM = [];
        $a = TbBaiviet::model()->findAll();
        foreach ($a as $b) {
            $h = json_decode($b['danhmuc']);
            foreach ($h as $c) {
                if ($c == $dm) {
                    $DM[] = $b['id'];
                    break;
                }
            }
        }
        return $DM;
    }

    public function checkBS($id) {
        $BS = [];
        $a = TbBacsi::model()->findAll();
        foreach ($a as $b) {
            $h = json_decode($b['chuc_vu']);
            foreach ($h as $c) {
                if ($c == $id) {
                    $BS[] = $b['id'];
                    break;
                }
            }
        }
        return $BS;
    }

    public function checkArr($id) {
        $Arr = [];
        $arr = TbBaiviet::model()->findByPk($id);
        $ar = TbBaiviet::model()->findAll();
        $b = json_decode($arr['danhmuc']);
        foreach ($b as $k) {
            foreach ($ar as $h) {
                $d = json_decode($h['danhmuc']);
                foreach ($d as $l) {
                    if ($k == $l) {
                        $Arr[] = $h['id'];
                    }
                }
            }
        }
        return $Arr;
    }

    public function KiemTraDanhMuc($id) {
        $DM = [];
        $bs = TbBacsi::model()->findAll();
        foreach ($bs as $b) {
            $dm = json_decode($b['chuc_vu']);
            foreach ($dm as $d) {
                $DM[] = $d;
            }
        }
        if (in_array($id, $DM)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function KiemTraChuyenKhoa($id) {
        $DM = [];
        $bs = TbBaiviet::model()->findAll();
        foreach ($bs as $b) {
            $dm = json_decode($b['danhmuc']);
            foreach ($dm as $d) {
                $DM[] = $d;
            }
        }
        if (in_array($id, $DM)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function checkLogin() {
        if (!isset($_SESSION['login'])) {
            return 0;
        } elseif (time() - $_SESSION['Time'] > 60 * 60 * 24) {
            session_unset();
            return 0;
        } else {
            return 1;
        }
    }

    function get_current_weekday() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $weekday = date("l");
        $weekday = strtolower($weekday);
        switch ($weekday) {
            case 'monday':
                $weekday = 0;
                break;
            case 'tuesday':
                $weekday = 1;
                break;
            case 'wednesday':
                $weekday = 2;
                break;
            case 'thursday':
                $weekday = 3;
                break;
            case 'friday':
                $weekday = 4;
                break;
            case 'saturday':
                $weekday = 5;
                break;
            default:
                $weekday = 6;
                break;
        }
        return $weekday;
    }

    function get_current_day($num) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $a = array();
        for ($i = 0; $i <= 6; $i++) {
            if ($i <= $num) {
                $t = $num - $i;
                //print (date('l-Y-m-d', strtotime(' - '.$t.' days')));
                $a[] = date('l', strtotime(' - ' . $t . ' days'));
            } else {
                $t = $i - $num;
                //print (date('l-Y-m-d', strtotime(' + '.$t.' days')));
                $a[] = date('l', strtotime(' + ' . $t . ' days'));
            }
        }
        return $a;
    }

    function count_time($gio, $phut, $time) {
        $g = $gio;
        $p = $phut;
        $t = $phut + $time;
        while ($t > 59) {
            $gio++;
            $t = $t - 60;
        }
        if ($p < 10) {
            $p = "0" . $p;
        }
        if ($g < 10) {
            $g = '0' . $g;
        }
        if ($gio < 10) {
            $gio = '0' . $gio;
        }
        if ($t < 10) {
            $t = '0' . $t;
        }
        return $g . ":" . $p . "-" . $gio . ":" . $t;
    }

    function GetDay($week, $year, $w, $h, $hour, $minute) {
        if ($w >= $h) {
            $week = $week;
        } else {
            $week = $week - 1;
        }
        $day = date('m/d/Y', strtotime(($h) . 'day', strtotime(sprintf("%4dW%02d", $year, $week))));
        $d = date('d', strtotime($day));
        $m = date('m', strtotime($day));
        $y = date('Y', strtotime($day));
        return strtotime($y . '-' . $m . '-' . $d . ' ' . $hour . ':' . $minute);
    }

}
