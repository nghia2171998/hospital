<?php
$this->pageTitle = 'Lỗi ' . $code;
$this->breadcrumbs = array(
    'Error',
);
?>


<div class="container">
    <div class="jumbotron">
        <h1 class="display-4" style="font-weight: bold;">Rất tiếc! Lỗi <?php echo $code; ?></h1>
        <?php if ($code == 404): ?>
            <p class="lead " style="font-weight: 600;">Trang bạn tìm kiếm không tồn tại.</p>
        <?php elseif ($code == 500): ?>
            <p class="lead " style="font-weight: 600;">Lỗi truy vấn xin thử lại.</p>
        <?php endif; ?>
        <hr class="my-4">
        <p class="lead mb-2">
            <a class="btn btn-primary btn-lg" href="<?php echo Yii::app()->baseUrl ?>/" role="button">Quay về Trang Chủ</a>
        </p>
        <p style="font-weight: 600; " class="p-bot" >Nếu cần hỗ trợ, vui lòng liên hệ hotline: 0988092250</p>
        <pre><?php echo $message  ?></pre>
        <pre><?php echo "File: $file : dòng $line";   ?></pre>
        <pre><?php echo $trace ?></pre>
    </div>
</div>
