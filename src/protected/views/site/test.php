<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<input type="hidden" id="upload_url" value="<?php echo Yii::app()->baseUrl . "/site/upload"; ?>" />
<input id="profileupload" type="file" name="profile" accept="image/png, image/jpg, image/gif, image/jpeg"/>


<script>
    $('#profileupload').on('change', function () {
        let photo = document.getElementById("profileupload").files[0];
        let formData = new FormData();

        formData.append("profile", photo);
        fetch($('#upload_url').val(), {method: "POST", body: formData});
    });
</script>