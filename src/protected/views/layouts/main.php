<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/assets/css/bootstrap.min.css" />
        <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/jquery-3.3.1.min.js"></script>
        <title><?php echo $this->pageTitle ?></title>
    </head>
    <body>
        <?php echo $content ?>
        <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/jquery-3.3.1.min.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/bootstrap.min.js"></script>
    </body>
</html>