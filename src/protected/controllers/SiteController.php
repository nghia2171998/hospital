<?php

/**
 * Controller DefaultController
 *
 * @author Nguyen Manh Luu <luu.nguyen@voithan.com>
 * @since 01-11-2017 16:26:23
 * @version 1.0
 */
class SiteController extends FrontController {
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }



}
