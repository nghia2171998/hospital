<?php

/**
 * File config modules
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since Jan 7, 2015 - 12:06:07 AM
 * @version 1.0
 */
return array(
    'trangchu',
    'trangtinh',
    'chuyenkhoa',
    'bacsi',
    'truyenthong',
    'hoidap',
    'taikhoan',
    'ykienkhachhang',
    'baiviet',
    'danhmuc',
    'anh',
    'time',
    'lichkham',
    'SetTimeBS',
    'Comment',
);