<?php

return array(
    // Other controllers
    '/' => 'trangchu/default/index',
    '<url>-p<id>' => '/trangtinh/default',
    '<url>-p<id>' => '/chuyenkhoa/default',
    '<url>-p<id>' => '/bacsi/default',
    '<url>/<id:\d+>' => '/danhmuc/default',
    'Truyen-thong' => 'truyenthong/default',
    'hoi-dap' => 'hoidap/default',
    'hoi-dap/add' => 'hoidap/default/add',
    '<url>-p<id>' => '/baiviet/default',
     // phải để dưới cùng   
     '<controller:\w+>/<id:\d+>' => '<controller>/view',
     '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
     '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
     'Chuyen-khoa'=>'chuyenkhoa/default/chuyenkhoa',
     'Bac-si'=>'bacsi/default/bacsi',
     'taikhoan/admin'=>'taikhoan/admin/index',
);



/**
 * End of url
 */