<?php

/**
 * File config modules
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since Jan 7, 2015 - 12:06:07 AM
 * @version 1.0
 */
$conf = require(dirname(__FILE__) . '/../modules.php');
//Tùy chỉnh module cho dev
$conf['gii'] = array(
    'class' => 'system.gii.GiiModule',
    'password' => '123456',
    'ipFilters' => array($_SERVER['REMOTE_ADDR'])
);
return $conf;
