<?php
$config = require(dirname(__FILE__) . '/../main.php');
$config['components']['db'] = require(dirname(__FILE__) . '/db.php');
$config['components']['urlManager']['rules'] = require(dirname(__FILE__) . '/url.php');
$config['params'] = require(dirname(__FILE__) . '/params.php');
$config['modules'] = require(dirname(__FILE__) . '/modules.php');

// Lấy cấu hình config của các module tương ứng
$modules_dir = dirname(dirname(__FILE__)) . '/../modules/';
foreach ($config['modules'] as $m => $conf)
{
    $module = (is_array($conf)) ? $m : $conf;
    
    if(in_array($module, $config['import']))
    {
        continue;
    }
    
    // Import model từ module
    $config['import'][] = 'application.modules.' . $module . '.models.*';

//    // Lấy config từ module
//    $module_config = $modules_dir . $module . '/config/main.php';
//    if (file_exists($module_config))
//    {
//        $config = CMap::mergeArray($config, require($module_config));
//    }
}
return $config;