<?php

/**
 * @author Nguyen Manh Luu <luu.nguyen@voithan.com>
 * @since 31-10-2017 15:59:53
 * @version 1.0
 */
$conf = require(dirname(__FILE__) . '/../params.php');
// Tùy chỉnh params cho dev
return $conf;