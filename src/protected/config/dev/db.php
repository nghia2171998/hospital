<?php
/**
 * Description of db
 * 
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since Jan 7, 2015 - 12:06:07 AM
 * @version 1.0
 */
return array(
    'connectionString' => 'mysql:host=localhost;dbname=phongkham',
    'emulatePrepare' => true,
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    'tablePrefix' => 'tbl_',
);
/**
 * End of db
 */