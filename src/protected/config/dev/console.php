<?php
$conf = require(dirname(__FILE__) . '/../console.php');
$conf['components']['db'] = require(dirname(__FILE__) . '/db.php');
$conf['params'] = require(dirname(__FILE__) . '/../params.php');

return $conf;