<?php

$config = array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    'components' => array(
        'db' => require(dirname(__FILE__) . '/db.php'),
        'cache' => array(
            'class' => 'system.caching.CFileCache',
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => require(dirname(__FILE__) . '/url.php'),
        ),
        'request' => array(
            'hostInfo' => 'http://localhost',
            'baseUrl' => '/medkee',
//            'scriptUrl' => '',
        ),
    ),
    'modules' => require(dirname(__FILE__) . '/modules.php'),
    'params' => require (dirname(__FILE__) . DIRECTORY_SEPARATOR . 'params.php'),
);
// Lấy cấu hình config của các module tương ứng
$modules_dir = dirname(dirname(__FILE__)) . '/modules/';
foreach ($config['modules'] as $m => $conf) {
    $module = (is_array($conf)) ? $m : $conf;

    if (in_array($module, $config['import'])) {
        continue;
    }

    // Import model từ module
    $config['import'][] = 'application.modules.' . $module . '.models.*';
}
return $config;

