<?php
/**
 * This is the template for generating the model class of a specified table.
 * - $this: the ModelCode object
 * - $tableName: the table name for this class (prefix is already removed if necessary)
 * - $modelClass: the model class name
 * - $columns: list of table columns (name=>CDbColumnSchema)
 * - $labels: list of attribute labels (name=>label)
 * - $rules: list of validation rules
 * - $relations: list of relations (name=>relation declaration)
 */
?>
<?php echo "<?php\n"; ?>
<?php $messageName = lcfirst($modelClass); ?>

/**
 * Đây là lớp model của bảng "<?php echo $tableName; ?>".
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since <?php echo date('d/m/Y H:i:s')."\n" ?>
 * @version 1.0
 *
 * Dưới đây là các cột của bảng '<?php echo $tableName; ?>':
<?php foreach($columns as $column): ?>
 * @property <?php echo $column->type.' $'.$column->name."\n"; ?>
<?php endforeach; ?>
<?php if(!empty($relations)): ?>
 *
 * Dưới đây là các quan hệ có sẵn trong model:
<?php foreach($relations as $name=>$relation): ?>
 * @property <?php
	if (preg_match("~^array\(self::([^,]+), '([^']+)', '([^']+)'\)$~", $relation, $matches))
    {
        $relationType = $matches[1];
        $relationModel = $matches[2];

        switch($relationType){
            case 'HAS_ONE':
                echo $relationModel.' $'.$name."\n";
            break;
            case 'BELONGS_TO':
                echo $relationModel.' $'.$name."\n";
            break;
            case 'HAS_MANY':
                echo $relationModel.'[] $'.$name."\n";
            break;
            case 'MANY_MANY':
                echo $relationModel.'[] $'.$name."\n";
            break;
            default:
                echo 'mixed $'.$name."\n";
        }
	}
    ?>
<?php endforeach; ?>
<?php endif; ?>
 */
class <?php echo $modelClass; ?>Base extends MyModel
{

    public $modelName = '<?php echo $modelClass; ?>';

    /**
     * @return string trả về tên của bảng
     */
    public function tableName()
    {
        return '<?php echo $tableName; ?>';
    }

    /**
     * @return array Các quy định về thuộc tính của lớp.
     */
    public function rules()
    {
        // LƯU Ý: bạn chỉ nên định nghĩa các quy định cho các trường
        // mà người dùng sẽ thể nhập liệu	
        return array(
<?php foreach($rules as $rule): ?>
            <?php echo $rule.",\n"; ?>
<?php endforeach; ?>
            // Những thuộc tính dưới đây để phục vụ cho hàm search().			
            // @todo Hãy xóa đi những trường mà bạn không muốn cho người dùng tìm kiếm
            array('<?php echo implode(', ', array_keys($columns)); ?>', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array mối quan hệ với các bảng khác.
     */
    public function relations()
    {
        // LƯU Ý: Bạn có thể cần phải điều chỉnh tên các quan hệ 
        // và các lớp liên quan được tự động sinh ra bên dưới đây		
        $rtn = parent::relations();
<?php foreach($relations as $name=>$relation): ?>
        <?php echo '$rtn[' . "'$name'] = $relation;\n"; ?>
<?php endforeach; ?>
        return $rtn;
    }

    /**
     * @return array Tùy chỉnh tên các thuộc tính (name=>label)
     */
    public function attributeLabels()
    {
        $rtn = parent::attributeLabels();
<?php foreach($labels as $name=>$label): ?>
        <?php echo '$rtn[' . "'$name'] = '$label';\n"; ?>
<?php endforeach; ?>
        return $rtn;
    }

<?php if($connectionId!='db'):?>
    /**
     * @return CDbConnection the database connection used for this class
     */
    public function getDbConnection()
    {
	return Yii::app()-><?php echo $connectionId ?>;
    }

<?php endif?>
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return <?php echo $modelClass; ?> the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
