<?php echo "<?php\n"; ?>
/**
 * <?php echo $this->moduleID; ?> module
 *
 * @author Nguyễn Mạnh Lưu <luu.nguyen@voithan.com>
 * @since <?php echo date('d/m/Y H:i:s')."\n" ?>
 * @version 1.0
 */
class <?php echo $this->moduleClass; ?> extends MyModule
{

    public function init()
    {        
        /**
        * $this->setImport(array(
        *    '<?php echo $this->moduleID; ?>.models.*',
        *    '<?php echo $this->moduleID; ?>.components.*',
        * ));
        */
    }
    
    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {     
            return true;
        }
        else
        {
            return false;
        }    
    }

}
