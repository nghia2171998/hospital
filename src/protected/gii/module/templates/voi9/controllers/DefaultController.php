<?php echo "<?php\n"; ?>
/**
 * Admin Controller of <?php echo $this->moduleID; ?> module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since <?php echo date('d/m/Y H:i:s')."\n" ?>
 * @version 1.0
 */
class DefaultController extends FrontController 
{
    
    function init()
    {
        parent::init();
        $this->pageTitle = '';
    }

    function actionIndex()
    {
        $this->render("index");
    }
    
}