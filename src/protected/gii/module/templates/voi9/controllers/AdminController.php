<?php echo "<?php\n"; ?>
/**
 * Admin Controller of <?php echo $this->moduleID; ?> module
 *
 * @author BookingCare Team <dev@bookingcare.vn>
 * @since <?php echo date('d/m/Y H:i:s')."\n" ?>
 * @version 1.0
 */
class AdminController extends BackController 
{

    public $modelName = '<?php echo ucfirst($this->moduleID); ?>';
    
    function init()
    {
        parent::init();
        $this->pageTitle = $this->modelName;
    }

    function getFormAttr()
    {
        return array(
            '' => array(
                array(
                    'field' => 'name'
                ),
                array(
                    'field' => 'status',
                    'type' => 'select'
                )
            ),
        );
    }

    /**
     * Danh sách các trường dùng cho danh sách
     * @return array
     */
    public function getListAttr()
    {
        return array(
            'name',
            'status'
        );
    }

    /**
     * Danh sách các trường dùng cho lọc
     * @return array
     */
    public function getFilterAttr()
    {
        return array(
            array(
                'field' => 'name',
                'type' => 'search'
            ),
            array(
                'field' => 'status',
                'type' => 'select'
            ),
        );
    }

    public function getAttrData()
    {
        $rtn = parent::getAttrData();
        return $rtn;
    }
}