<!DOCTYPE html>
<html lang="vi">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Homepage</title>
        <link rel="shortcut icon" type="image/x-icon" href="https://bookingcare.vn/bookingcare.png">
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/assets/css/style1.css" />
        <link type="text/css" rel="stylesheet"
              href="<?php echo Yii::app()->baseUrl ?>/assets/css/jquery-dynamic-content-menu.css" />
        <link
            href="<?php echo Yii::app()->baseUrl ?>/assets/css/https _cdnjs.cloudflare.com_ajax_libs_slick-carousel_1.9.0_slick-theme.min.css"
            rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->baseUrl ?>/assets/js/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link
            href="<?php echo Yii::app()->baseUrl ?>/assets/css/https _cdn.jsdelivr.net_npm_bootstrap@5.1.3_dist_css_bootstrap.min.css"
            rel="stylesheet" type="text/css" />
        <link
            href="<?php echo Yii::app()->baseUrl ?>/assets/css/https _bootstrapmade.com_demo_templates_Reveal_assets_vendor_swiper_swiper-bundle.min.css"
            rel="stylesheet" type="text/css" />
        <link
            href="<?php echo Yii::app()->baseUrl ?>/assets/css/https _bootstrapmade.com_demo_templates_Reveal_assets_css_style.css"
            rel="stylesheet" type="text/css" />

        <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/vue_index.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/vue.js"></script>
        <script
            src="<?php echo Yii::app()->baseUrl ?>/assets/js/https _cdn.jsdelivr.net_npm_bootstrap@5.1.3_dist_js_bootstrap.bundle.min.js">
        </script>
        <script
            src="<?php echo Yii::app()->baseUrl ?>/assets/js/https _ajax.googleapis.com_ajax_libs_jquery_3.5.1_jquery.min.js">
        </script>
        <script
            src="<?php echo Yii::app()->baseUrl ?>/assets/js/https _bootstrapmade.com_demo_templates_Reveal_assets_vendor_glightbox_js_glightbox.min.js">
        </script>
        <script
            src="<?php echo Yii::app()->baseUrl ?>/assets/js/https _bootstrapmade.com_demo_templates_Reveal_assets_vendor_swiper_swiper-bundle.min.js">
        </script>
        <script
            src="<?php echo Yii::app()->baseUrl ?>/assets/js/https _bootstrapmade.com_demo_templates_Reveal_assets_js_main.js">
        </script>
        <style type='text/css'>
            .slick-prev:before,
            .slick-next:before {
                font-family: fontawesome;
                font-size: 30px;
                color: black;
            }

            .slick-prev:before {
                content: '\f100';
            }

            .slick-next:before {
                content: '\f101';
            }

            .slick-track {
                margin: 0;
            }

            .slick-prev,
            .slick-next {
                display: none;
            }

            .offcanvas-backdrop.show {
                opacity: 0.5;
            }

            .accordion-button::after {
                display: contents;
            }

            @media only screen and (max-width: 1024px) {

                .slick-prev:before,
                .slick-next:before {
                    display: none;
                }
            }

            .offcanvas-backdrop.show {
                opacity: 0.5;
            }
        </style>
    </head>
    <header>
        <div class='container'>
            <nav class="navbar navbar-expand-lg navbar-light" style='height: 100%;'>
                <div class='icon full_icon'>
                    <a href="<?php echo Yii::app()->baseUrl ?>/">
                        <img width='auto' height="60px" src='https://dev.bookingcare.vn/assets/icon/bookingcare-2020.svg'/>
                    </a>
                </div>
                <div class="container-fluid show_menu">
                    <div class='icon short_icon'>
                        <a href="<?php echo Yii::app()->baseUrl ?>/">
                            <img width='auto' height="60px" src='https://dev.bookingcare.vn/bookingcare.png'/>
                        </a>
                    </div>
                    <div class="thanh_search">
                        <div class='tim_kiem' id='tim_kiem'>
                            <div class="input-group mb-3 input-tk">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                         class="bi bi-search" viewBox="0 0 16 16">
                                    <path
                                        d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                                    </svg>
                                </button>
                                <input id='tukhoa' name='tukhoa' type="search" class="form-control" placeholder="Tìm Kiếm"
                                       aria-label="Example text with button addon" aria-describedby="button-addon1" autocomplete="off">
                            </div>
                            <div class='search' id='Ketqua'></div>
                        </div>
                        <button class="btn" style="margin-right: -15px;" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">
                            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
                            </svg>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse border_menu" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo Yii::app()->baseUrl ?>/">Trang chủ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo Yii::app()->baseUrl ?>/Chuyen-khoa">Dịch vụ và chuyên khoa</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="<?php echo Yii::app()->baseUrl ?>/danhmuc">Danh mục</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Trang tĩnh
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <?php
                                    $pages = TrangTinh::model()->findAll();
                                    foreach ($pages as $page) :
                                        if ($page->trang_thai == 1) :
                                            ?>
                                            <li><a class="dropdown-item" href="<?php echo Yii::app()->createUrl('trangtinh/default', array('id' => $page->id, 'url' => $page->url)) ?>"><?= $page['name'] ?></a></li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>

                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo Yii::app()->baseUrl ?>/hoi-dap">Hỏi đáp</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
                <div class="offcanvas-header">
                    <h5 id="offcanvasRightLabel">Menu</h5>
                    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button" type="button">
                            <a href="<?php echo Yii::app()->baseUrl ?>/">Trang Chủ </a>
                        </button>
                    </h2>
                </div>

                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseOne" aria-expanded="false"
                                aria-controls="flush-collapseOne">
                            Giới Thiệu
                        </button>
                    </h2>

                    <?php
                    $pages = TrangTinh::model()->findAll();
                    foreach ($pages as $page) {
                        if ($page->trang_thai == 1) {
                            ?>

                            <div id="flush-collapseOne" class="accordion-collapse collapse"
                                 aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                <a style='color: #0d6efd; margin-left: 35px'
                                   href="
                                   <?php echo Yii::app()->createUrl('trangtinh/default', array('id' => $page->id, 'url' => $page->url)) ?>">
                                    <?php echo $page->name ?> </a>

                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button collapsed" type="button">
                            <a href="<?php echo Yii::app()->baseUrl ?>/Chuyen-khoa">Dịch Vụ và Chuyên Khoa</a>
                        </button>
                    </h2>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button collapsed" type="button">
                            <a href="<?php echo Yii::app()->baseUrl ?>/danhmuc">Cẩm Nang</a>
                        </button>
                    </h2>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button collapsed" type="button">
                            <a href="<?php echo Yii::app()->baseUrl ?>/hoi-dap"> Hỏi Đáp</a>
                        </button>
                    </h2>
                </div>
            </div>
        </div>
    </header>
    <?php echo $content ?>

    <!-- FOOTER CONTACT -->

    <footer style="padding: 20px 0 0; height: 100%; background-color:#efefef">

        <!-- Messenger Plugin chat Code -->
        <div id="fb-root"></div>

        <!-- Your Plugin chat code -->
        <div id="fb-customer-chat" class="fb-customerchat">
        </div>

        <script>
            var chatbox = document.getElementById('fb-customer-chat');
            chatbox.setAttribute("page_id", "110329668169910");
            chatbox.setAttribute("attribution", "biz_inbox");
        </script>

        <!-- Your SDK code -->
        <script>
            window.fbAsyncInit = function () {
                FB.init({
                    xfbml: true,
                    version: 'v12.0'
                });
            };

            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <a href="<?php echo Yii::app()->baseUrl ?>/">
                        <img width='auto' height="40px" src='https://bookingcare.vn/assets/icon/bookingcare-2020.svg'/>
                    </a>
                    <h5 style="font-weight: bold; font-size: 14px;margin-top: 10px">Công ty cổ phần công nghệ Bookingcare</h5>
                    <p style="font-size: 14px;margin: -15px 0 15px 0">28 Thành Thái, Dịch Vọng, Cầu Giấy, Hà Nội</p>
                    <p style="font-size: 14px;margin: -15px 0 15px 0">ĐKKD số: 0106790291. Sở KHĐT Hà Nội cấp ngày 16/03/2015</p>
                    <div style="display:flex">
                        <img src="https://bookingcare.vn/assets/icon/bo-cong-thuong.svg"/>
                        <img src="https://bookingcare.vn/assets/icon/bo-cong-thuong.svg"/>
                    </div>
                </div>

                <div class="col-md-3">
                    <a style='color: var(--mau-bc-16);' href="#">Liên hệ hợp tác</a><br>
                    <a style='color: var(--mau-bc-16);' href="#">Câu hỏi thường gặp</a><br>
                    <a style='color: var(--mau-bc-16);' href="#">Điều khoản sử dụng</a><br>
                    <a style='color: var(--mau-bc-16);' href="#">Chính sách bảo mật</a><br>
                    <a style='color: var(--mau-bc-16);' href="#">Quy trình giải quyết khiếu nại</a><br>
                    <a style='color: var(--mau-bc-16);' href="#">Quy chế hoạt động</a><br>
                </div>

                <div class="col-md-4">
                    <h5 style="font-weight: bold; font-size: 14px;margin-top: 10px">Trụ sở tại Hà Nội</h5>
                    <p style="font-size: 14px;margin: -15px 0 15px 0">28 Thành Thái, Dịch Vọng, Cầu Giấy, Hà Nội</p>
                    <h5 style="font-weight: bold; font-size: 14px;margin-top: 10px">Trụ sở tại Hà </h5>
                    <p style="font-size: 14px;margin: -15px 0 15px 0">28 Thành Thái, Dịch Vọng, Cầu Giấy, Hà Nội</p>
                    <h5 style="font-weight: bold; font-size: 14px;margin-top: 10px">Trụ sở tại Hà Nội</h5>
                    <p style="font-size: 14px;margin: -15px 0 15px 0">28 Thành Thái, Dịch Vọng, Cầu Giấy, Hà Nội</p>
                </div>
                <div>
                    <div class="col-md-12 text-center">
                        <p> &copy <a href="https://medkee.com/">medkee.com</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script language="javascript">
        $(document).ready(function (e) {
            $("#tukhoa").keyup(function (e) {
                tk = $("#tukhoa").val();
                $.post("<?php echo Yii::app()->baseUrl ?>/trangchu/default/ketqua", {
                    tukhoa: tk
                },
                        function (responseData, status) {
                            if (status == "success")
                                $("#Ketqua").html(responseData);
                            else
                                $("#Ketqua").html("<h3>CÓ LỖI</h3>");
                        }
                );
            });
        });

        $("#tukhoa").focus(function () {
            document.getElementById("Ketqua").style.display = "block";
        });

        $(document).click(function (e) {
            var container = $("#tim_kiem");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                document.getElementById("Ketqua").style.display = "none";
            }
        });
    </script>

</html>