<div id='Ketqua1' style='width:100%;hieght:auto;display:block'>
    <div class='title' id='camnang' v-cloak>
        <nav>
            <p style="text-align: center">{{title}}</p>
        </nav>
        <a v-if='in_trash==1' class="navbar-brand"></a>
        <div v-if='FormTitle.select=="1"' style="float: right">
            <select v-if='in_trash==0' class="form-select " style='width: 200px' aria-label=".form-select-sm example"
                v-model='select1' @change="fetchAllData()">
                <option value=''>All câu hỏi</option>
                <option value="0">Câu hỏi đã trả lời</option>
                <option value="1">Câu hỏi chưa trả lời</option>
            </select>
        </div>
        <div v-else>
        </div>
        <div class='act' v-if='in_trash==0'>
            <input v-model='tukhoa' v-on:keyup.enter="fetchAllData()" type="search" placeholder="Tìm kiếm" />
            <select v-model='select' @change="fetchAllData()">
                <option value='' disabled>Chọn trạng thái</option>
                <option value="0">Nháp</option>
                <option value="1">Xuất bản</option>
            </select>
            <button type="button" class="btn btn-secondary" onclick="reload()">Trở
                lại</button>
        </div>

        <div class='bt'>
            <div v-if='FormTitle.addbutton=="1"' style="width:95%">
                <input v-if='in_trash==0' type="button" class="btn btn-success btn-xs" @click="openModel"
                    value="Thêm" />
            </div>
            <div>
                <button v-if='in_trash==0' type="button" class="btn btn-warning float-end"
                    @click='fetchAllHiddenData()'>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-trash" viewBox="0 0 16 16">
                        <path
                            d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                        <path fill-rule="evenodd"
                            d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                    </svg>
                </button>
                <button v-if='in_trash==1' type="button" class="btn btn-secondary float-end" @click='fetchAllData()'>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-recycle" viewBox="0 0 16 16">
                        <path
                            d="M9.302 1.256a1.5 1.5 0 0 0-2.604 0l-1.704 2.98a.5.5 0 0 0 .869.497l1.703-2.981a.5.5 0 0 1 .868 0l2.54 4.444-1.256-.337a.5.5 0 1 0-.26.966l2.415.647a.5.5 0 0 0 .613-.353l.647-2.415a.5.5 0 1 0-.966-.259l-.333 1.242-2.532-4.431zM2.973 7.773l-1.255.337a.5.5 0 1 1-.26-.966l2.416-.647a.5.5 0 0 1 .612.353l.647 2.415a.5.5 0 0 1-.966.259l-.333-1.242-2.545 4.454a.5.5 0 0 0 .434.748H5a.5.5 0 0 1 0 1H1.723A1.5 1.5 0 0 1 .421 12.24l2.552-4.467zm10.89 1.463a.5.5 0 1 0-.868.496l1.716 3.004a.5.5 0 0 1-.434.748h-5.57l.647-.646a.5.5 0 1 0-.708-.707l-1.5 1.5a.498.498 0 0 0 0 .707l1.5 1.5a.5.5 0 1 0 .708-.707l-.647-.647h5.57a1.5 1.5 0 0 0 1.302-2.244l-1.716-3.004z" />
                    </svg>
                </button>
            </div>
        </div>
        <div class='add'>
            <div v-if='myModel'>
                <div class="modal-mask">
                    <div class="modal-wrappe">
                        <div class="modal-dialog modal-xl">

                            <form method='POST' class="modal-content" id='myForm' enctype="multipart/form-data">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{dynamicTitle}} {{title}}</h5>
                                    <button type="button" class="btn-close" @click="myModel=false"></button>
                                </div>
                                <div class="modal-body">
                                    <div v-for="form in FormAttr">
                                        <div v-if="form.type=='text'">
                                            <div class="mb-3">
                                                <label for="f1" class="form-label">{{form.name}}</label>
                                                <input type="text" class="form-control" :v-model='form.field'
                                                    :name='form.field' :value='form.value' required>
                                            </div>
                                        </div>
                                        <div v-if="form.type=='date'">
                                            <div class="mb-3">
                                                <label for="f1" class="form-label">{{form.name}}</label>
                                                <input type="date" class="form-control" v-model="date" name='date'>
                                            </div>
                                        </div>
                                        <div v-if='form.type=="user"'>
                                            <div class="mb-3">
                                                <label for="f1" class="form-label">Tên tài khoản</label>
                                                <input type="text" class="form-control" v-model="ten"
                                                    v-on:keyup="checkuser()" name='ten' id="ten" required>
                                                <div class='dropdown-search' id="checkname"
                                                    style='font-size:14px;color:red'></div>
                                            </div>
                                        </div>
                                        <div v-if='form.type=="email"'>
                                            <div class="mb-3">
                                                <label for="f1" class="form-label">Email</label>
                                                <input type="email" class="form-control" v-on:keyup="checkemail()"
                                                    v-model="email" name='email' id="email" required>
                                                <div class='dropdown-search' id="checkemail"
                                                    style='font-size:14px;color:red'></div>
                                            </div>
                                        </div>
                                        <div v-if='form.type=="password"'>
                                            <div class="mb-3">
                                                <label class="form-label">Mật khẩu</label>
                                                <input type="password" class="form-control" v-model="mk1" name='mk1'
                                                    autocomplete="on">
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label">Nhập lại mật khẩu</label>
                                                <input type="password" class="form-control" v-model="mk2" name='mk2'
                                                    autocomplete="on">
                                            </div>
                                        </div>
                                        <div v-if="form.type=='img'">
                                            <div id="display_image"
                                                style=' max-width: 300px;max-height: 200px;background-position: center;background-size: cover;overflow:hidden'>
                                                <img id='idimg' :src="image" />
                                            </div>
                                            <input type="hidden" v-model='anh' id="anh">
                                            <a class="btn btn-primary" href="#" role="button" id="myBtn"
                                                @click='openImage(img=1)'>{{form.name}}</a>
                                        </div>
                                        <div v-if="form.type=='imgs'">
                                            <div id="display_image"
                                                style=' max-width: 300px;max-height: 200px;background-position: center;background-size: cover;overflow:hidden'>
                                                <img id='idimg_kq' :src="image_kq" />
                                            </div>
                                            <input type="hidden" v-model='anh_kq' id="anh_kq">
                                            <a class="btn btn-primary" href="#" role="button" id="myBtn"
                                                @click='openImage(img=2)'>{{form.name}}</a>
                                        </div>
                                        <div v-if="form.type=='textarea'">
                                            <div class="mb-3">
                                                <label for="exampleFormControlTextarea1"
                                                    class="form-label">{{form.name}}</label>
                                                <textarea :name='form.field' :v-model="form.field" rows="5" cols="50"
                                                    required>{{form.value}}</textarea>
                                            </div>
                                        </div>
                                        <div v-if="form.type=='ckeditor'">
                                            <div class="mb-3">
                                                <label for="exampleFormControlTextarea1"
                                                    class="form-label">{{form.name}}</label>
                                                <textarea :name='form.field' :v-model="form.field" rows="5" cols="50"
                                                    required>{{form.value}}</textarea>
                                            </div>
                                        </div>
                                        <div v-if="form.type=='select2'">
                                            <div class="mb-3">
                                                <label for="f1" class="form-label">{{form.name}}</label>
                                                <select class="form-select mul-select"
                                                    aria-label="Default select example" id="danhmuc" name="danhmuc[]"
                                                    multiple="multiple" style='width:100%;height:30px'>
                                                </select>
                                            </div>
                                        </div>
                                        <div v-if='form.type=="radio"'>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="rd1" v-model='rd1'
                                                    value='0' checked>
                                                <label class="form-check-label" for="rd1">
                                                    Nháp
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="rd1" v-model="rd1"
                                                    value='1'>
                                                <label class="form-check-label" for="rd1">
                                                    Xuất bản
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" v-model="hiddenId" name='hiddenId' />
                                    <button type="button" class="btn btn-secondary"
                                        @click='myModel=false'>Thoát</button>
                                    <button type="button" class="btn btn-primary" id='luu'
                                        @click='submitData'>Lưu</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr class="table-primary">
                    <th scope="col">STT</th>
                    <th v-for='col in listAtttr' scope="col">{{col.name}}</th>
                    <th v-if='in_trash==0' scope="col">Hành động</th>
                    <th v-if='in_trash==1' scope="col">Khôi phục</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for='row,index in allData'>
                    <td scope="row">{{index+1}}</td>
                    <td v-for='col in listAtttr' scope="col">
                        <p v-if='col.field=="trang_thai"'>{{row[col.field] | trangthai}}</p>
                        <p v-else-if='col.field=="image"'><img width='100' height='100'
                            :src="'<?php echo Yii::app()->baseUrl?>/file/f100/' + row[col.field]" alt='' /></p>
                        <p v-else-if='col.field=="images"'><img width='100' height='100'
                            :src="'<?php echo Yii::app()->baseUrl?>/file/f100/' + row[col.field]" alt='' /></p>
                        <p v-else>{{row[col.field]}}</p>
                    </td>
                    <td v-if='in_trash==0'>
                        <button type="button" class="btn btn-primary" @click="fetchData(row.id)">Sửa</button>
                        <button type="button" class="btn btn-danger btn-xs delete"
                            @click="hiddenData(row.id)">Xoá</button>
                    </td>
                    <td v-if='in_trash==1'>
                        <button type="button" class="btn btn-primary" @click="unHiddenData(row.id)">Khôi phục</button>
                    </td>
                </tr>
            </tbody>
        </table>
        <div v-if='ModelAnh'>
            <div class="modal-mask">
                <div class="modal-wrappe">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Ảnh</h5>
                                <button type="button" class="btn-close" @click="ModelAnh=false"></button>
                            </div>
                            <div class="modal-body">
                                <iframe src="<?php echo yii::app()->createUrl('/anh/admin/add1') ?>" width="100%"
                                    height="850" frameborder="0"></iframe>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" @click='ModelAnh=false'>Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center" v-if="pageList.length > 1">
            <button v-if='in_trash==0' class="btn btn-info mr-2" v-bind:class="page ==  current_page ? 'btn-dark' : ''"
                v-for="page in pageList" v-on:click="fetchAllData(page)">{{page}}</button>
            <button v-if='in_trash==1' class="btn btn-info mr-2" v-bind:class="page ==  current_page ? 'btn-dark' : ''"
                v-for="page in pageList" v-on:click="fetchAllHiddenData(page)">{{page}}</button>
        </div>
    </div>
</div>
<input type='hidden' name="getListAttribues" value='<?php echo json_encode($this->getListAttr1()) ?>' />
<input type='hidden' name="getFormAttribues" value='<?php echo json_encode($this->getFormAttr1()) ?>' />
<input type='hidden' name="getTitleAttribues" value='<?php echo json_encode($this->getTitle()) ?>' />
<script>
var application = new Vue({
    el: "#camnang",
    data: {
        allData: '',
        allDanhMuc: '',
        user: 1,
        passw: 1,
        gmail: 1,
        title: '',
        hoi: '',
        Addbutton: '',
        myModel: false,
        ModelAnh: false,
        actionButton: 'Insert',
        dynamicTitle: 'Thêm cẩm nang',
        select: '',
        select1: '',
        tukhoa: '',
        anh:'',
        anh_kq:'',
        date:'',
        in_trash: 0,
        pageList: [],
        limit: 10,
        current_page: 1,
        listAtttr: JSON.parse($('[name="getListAttribues"]').val()),
        FormAttr: JSON.parse($('[name="getFormAttribues"]').val()),
        FormTitle: JSON.parse($('[name="getTitleAttribues"]').val()),
    },
    methods: {
        fetchAllData: function(page = 1) {
            application.title = application.FormTitle.Title;
            application.in_trash = 0;
            if (page) {
                application.current_page = page;
            }
            $.ajax({
                url: "<?php echo $this->createUrl('admin/GetList') ?>",
                type: 'GET',
                data: {
                    in_trash: 0,
                    limit: 10,
                    page: application.current_page,
                    tukhoa: application.tukhoa,
                    select: application.select,
                    select1: application.select1,
                },
                success: function(res) {
                    application.allData = res.data.data;
                    var page_number = Math.ceil(res.data.total / application.limit);
                    application.pageList = [];
                    for (var i = 1; i <= page_number; i++) {
                        application.pageList.push(i);
                    }
                },
            });
        },
        fetchAllHiddenData: function(page = 1) {
            application.in_trash = 1;
            if (page) {
                application.current_page = page;
            }
            $.ajax({
                url: "<?php echo $this->createUrl('admin/Getlist') ?>",
                type: 'GET',
                data: {
                    in_trash: 1,
                    limit: 10,
                    page: application.current_page,
                    tukhoa: application.tukhoa,
                    select: application.select,
                },
                success: function(res) {
                    application.allData = res.data.data;
                    var page_number = Math.ceil(res.data.total / application.limit);
                    application.pageList = [];
                    for (var i = 1; i <= page_number; i++) {
                        application.pageList.push(i);
                    }
                },
            });
        },
        fetchData: function(id) {
            $.ajax({
                url: '<?php echo $this->createUrl('admin/getOneList') ?>',
                type: 'GET',
                data: {
                    id: id,
                },
            }).then(function(res) {
                application.hiddenId = res.data.id;
                for (var i = 0; i < application.FormAttr.length; i++) {
                    if (application.FormAttr[i].type == "textarea") {
                        $(document).ready(function() {
                            CKEDITOR.replace('title');
                        });
                    } else if (application.FormAttr[i].type == "ckeditor") {
                        $(document).ready(function() {
                            CKEDITOR.replace('tomtat');
                        });
                    } else if (application.FormAttr[i].type == "user") {
                        application.ten = res.data.ten;
                    } else if (application.FormAttr[i].type == "email") {
                        application.email = res.data.email;
                    } else if (application.FormAttr[i].type == "password") {
                        application.mk1 = '';
                        application.mk2 = '';
                    } else if (application.FormAttr[i].type == "img") {
                        application.image = '<?php echo Yii::app()->baseUrl?>/file/' +res.data.anh;
                        application.anh = res.data.anh;
                    } else if (application.FormAttr[i].type == "imgs") {
                        application.image_kq = '<?php echo Yii::app()->baseUrl?>/file/' +res.data.anh_kq;
                        application.anh_kq = res.data.anh_kq;
                    } else if (application.FormAttr[i].type == 'radio') {
                        application.rd1 = res.data.trang_thai;
                    } else if (application.FormAttr[i].type == 'date') {
                        application.date = res.data.date;
                    } else if (application.FormAttr[i].type == 'select2') {
                        application.allDanhMuc = application.FormAttr[i].data;
                        $("#danhmuc").val(res.data.danhmuc);
                        $(document).ready(function() {
                            $('.mul-select').select2({
                                data: application.allDanhMuc
                            });
                            $(".mul-select").val(res.data.danhmuc).trigger('change');
                        });
                    }
                    application.FormAttr[i].value = res.data[application.FormAttr[i].field];
                }
                application.myModel = true;
                application.dynamicTitle = 'Sửa';
                application.actionButton = 'Update';
            });
        },
        submitData: function() {
            if (application.mk1 == application.mk2) {
                if (application.user == 1 && application.gmail == 1) {
                    if (application.actionButton == 'Insert') {
                        var form_data = new FormData(document.getElementById("myForm"));
                        application.FormAttr.forEach(function(item) {
                            if (item.type == 'textarea') {
                                form_data.append('title', CKEDITOR.instances['title'].getData());
                            }
                            if (item.type == 'ckeditor') {
                                form_data.append('tomtat', CKEDITOR.instances['tomtat'].getData());
                            }
                            if (item.type == 'img') {
                                form_data.append('anh', application.anh);
                            }
                            if (item.type == 'imgs') {
                                form_data.append('anh_kq', application.anh_kq);
                            }
                            if (item.type == 'select2') {
                                form_data.append('danhmuc', JSON.stringify($('#danhmuc').val()));
                            }
                            if (item.type == 'password') {
                                form_data.append('pass1', application.mk1);
                            }

                        });
                        $.ajax({
                            url: '<?php echo $this->createUrl('admin/AddList') ?>',
                            type: 'POST',
                            contentType: false,
                            processData: false,
                            data: form_data,
                        }).then(function(res) {
                            loader();
                            application.myModel = false;
                            application.fetchAllData();
                            application.rd1 = 0;
                            application.hiddenId = '';
                        });
                    }
                    if (application.actionButton == 'Update') {
                        var form_data = new FormData(document.getElementById("myForm"));
                        application.FormAttr.forEach(function(item) {
                            if (item.type == 'textarea') {
                                form_data.append('title', CKEDITOR.instances['title'].getData());
                            }
                            if (item.type == 'ckeditor') {
                                form_data.append('tomtat', CKEDITOR.instances['tomtat'].getData());
                            }
                            if (item.type == 'img') {
                                form_data.append('anh', application.anh);
                            }
                            if (item.type == 'imgs') {
                                form_data.append('anh_kq', application.anh_kq);
                            }
                            if (item.type == 'select2') {
                                form_data.append('danhmuc', JSON.stringify($('#danhmuc').val()));
                            }

                        });
                        $.ajax({
                            url: "<?php echo $this->createUrl('admin/UpList') ?>",
                            type: 'POST',
                            contentType: false,
                            processData: false,
                            data: form_data,
                        }).then(function(res) {
                            loader();
                            application.myModel = false;
                            application.fetchAllData();
                            application.rd1 = 0;
                            application.hiddenId = '';
                        });
                    }
                }
            } else if (application.mk1 != application.mk2) {
                alert('Mật khẩu nhập lại không đúng');
            }
        },
        openModel: function() {
            for (var i = 0; i < application.FormAttr.length; i++) {
                if (application.FormAttr[i].type == "textarea") {
                    $(document).ready(function() {
                        CKEDITOR.replace('title');
                    });
                }
                if (application.FormAttr[i].type == "ckeditor") {
                    $(document).ready(function() {
                        CKEDITOR.replace('tomtat');
                    });
                }
                if (application.FormAttr[i].type == "img") {
                    application.image = '';
                    application.anh = '';
                }
                if (application.FormAttr[i].type == "imgs") {
                    application.image_kq = '';
                    application.anh_kq = '';
                }
                if (application.FormAttr[i].type == "password") {
                    application.mk1 = '';
                    application.mk2 = '';
                }
                if (application.FormAttr[i].type == 'select2') {
                    application.allDanhMuc = application.FormAttr[i].data;
                    application.danhmuc = [];
                    $(document).ready(function() {
                        $('.mul-select').select2({
                            data: application.allDanhMuc
                        });
                    });
                }
                application.FormAttr[i].value = '';
            }
            application.rd1 = 0;
            application.ten = '';
            application.email = '';
            application.hiddenId = '';
            application.actionButton = 'Insert';
            application.dynamicTitle = 'Thêm';
            application.myModel = true;
        },
        openImage: function() {
            application.ModelAnh = true;
        },
        hiddenData: function(id) {
            Swal.fire({
                title: 'Bạn muốn xoá?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Đồng ý',
                cancelButtonText: 'Không'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                            'Đã xoá!',
                        ),
                        $.ajax({
                            url: "<?php echo $this->createUrl('admin/Hidden') ?>",
                            type: 'POST',
                            data: {
                                id: id,
                            },
                        }).then(function(res) {
                            application.fetchAllData();
                        });
                }
            });
        },
        unHiddenData: function(id) {
            $.ajax({
                url: "<?php echo $this->createUrl('admin/UnHidden') ?>",
                type: 'POST',
                data: {
                    id: id,
                },
            }).then(function(res) {
                application.fetchAllHiddenData();
            });
        },
        checkuser: function() {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('taikhoan/admin/checkuser') ?>',
                type: 'GET',
                data: {
                    ten: application.ten,
                },
            }).then(function(res) {
                if (application.ten != '') {
                    if (res.data != 1) {
                        document.getElementById("checkname").innerHTML = res.data;
                        application.user = 0;
                    } else {
                        document.getElementById("checkname").innerHTML = '';
                        application.user = 1;
                    }
                } else {
                    application.gmail = 0;
                }
            });
        },
        checkemail: function() {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('taikhoan/admin/checkemail') ?>',
                type: 'GET',
                data: {
                    email: application.email,
                },
            }).then(function(res) {
                if (application.email != '') {
                    if (res.data != 1) {
                        document.getElementById("checkemail").innerHTML = res.data;
                        application.gmail = 0;
                    } else {
                        document.getElementById("checkemail").innerHTML = '';
                        application.gmail = 1;
                    }
                } else {
                    appliaction.gmail = 0;
                }
            });
        },
    },
    filters: {
        trangthai: function(id) {
            return id == "1" ? "Xuất bản" : "Nháp";
        }
    },
});
application.fetchAllData();

function reload() {
    application.select = '';
    application.select1 = '';
    application.tukhoa = '';
    application.fetchAllData();
}

function hello(string) {
    if (application.img == 1) {
        var name = string;
        application.anh = name;
        document.images['idimg'].src = '<?php echo Yii::app()->baseUrl?>/file/' + name;
        application.ModelAnh = false;
    } else {
        var name = string;
        application.anh_kq = name;
        document.images['idimg_kq'].src = '<?php echo Yii::app()->baseUrl?>/file/' + name;
        application.ModelAnh = false;
    }
}

function loader() {
    $('.loader').css('display', 'block');
    $('.loader').delay(1000).fadeOut('fast');
}
</script>