<?php
$Login = MyUtil::checkLogin();
if ($Login == 0) {
    header('Location:' . Yii::app()->baseUrl . '/taikhoan');
} else {
    ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/assets/css/style_admin.css" />
    <link type="text/css" rel="stylesheet"
        href="<?php echo Yii::app()->baseUrl ?>/assets/css/https _cdn.jsdelivr.net_npm_bootstrap@5.1.3_dist_css_bootstrap.min.css" />
    <link type="text/css" rel="stylesheet"
        href="<?php echo Yii::app()->baseUrl ?>/assets/css/https _cdn.jsdelivr.net_npm_select2@4.1.0-rc.0_dist_css_select2.min.css" />
    <link href="<?php echo Yii::app()->baseUrl ?>/assets/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo Yii::app()->baseUrl ?>/assets/css/sb-admin-2.min.css" rel="stylesheet">
    <script
        src="<?php echo Yii::app()->baseUrl ?>/assets/js/https _ajax.googleapis.com_ajax_libs_jquery_3.6.0_jquery.min.js">
    </script>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/vue.js"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/sweetalert2@11.js"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/vue_index.js"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/ckeditor/ckeditor.js"></script>

    <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/assets/css/fontawesome.css" />
</head>

<body id="page-top">
    <div class="loader">
        <span class="fas fa-spinner xoay icon"></span>
    </div>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor"
                    class="bi bi-emoji-wink" viewBox="0 0 16 16">
                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                    <path
                        d="M4.285 9.567a.5.5 0 0 1 .683.183A3.498 3.498 0 0 0 8 11.5a3.498 3.498 0 0 0 3.032-1.75.5.5 0 1 1 .866.5A4.498 4.498 0 0 1 8 12.5a4.498 4.498 0 0 1-3.898-2.25.5.5 0 0 1 .183-.683zM7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm1.757-.437a.5.5 0 0 1 .68.194.934.934 0 0 0 .813.493c.339 0 .645-.19.813-.493a.5.5 0 1 1 .874.486A1.934 1.934 0 0 1 10.25 7.75c-.73 0-1.356-.412-1.687-1.007a.5.5 0 0 1 .194-.68z" />
                </svg>
                <div class="sidebar-brand-text mx-3">Hospitals</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo Yii::app()->baseUrl ?>/chuyenkhoa/admin">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-clipboard-data" viewBox="0 0 16 16">
                        <path
                            d="M4 11a1 1 0 1 1 2 0v1a1 1 0 1 1-2 0v-1zm6-4a1 1 0 1 1 2 0v5a1 1 0 1 1-2 0V7zM7 9a1 1 0 0 1 2 0v3a1 1 0 1 1-2 0V9z" />
                        <path
                            d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z" />
                        <path
                            d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z" />
                    </svg>
                    <span>Chuyên Khoa</span>
                </a>
            </li>
            <hr class="sidebar-divider">

            <li class="nav-item active">
                <a class="nav-link" href="<?php echo Yii::app()->baseUrl ?>/danhmuc/admin">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-grid-3x3-gap" viewBox="0 0 16 16">
                        <path
                            d="M4 2v2H2V2h2zm1 12v-2a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1zm0-5V7a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1zm0-5V2a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1zm5 10v-2a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1zm0-5V7a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1zm0-5V2a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1zM9 2v2H7V2h2zm5 0v2h-2V2h2zM4 7v2H2V7h2zm5 0v2H7V7h2zm5 0h-2v2h2V7zM4 12v2H2v-2h2zm5 0v2H7v-2h2zm5 0v2h-2v-2h2zM12 1a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1h-2zm-1 6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1h-2a1 1 0 0 1-1-1V7zm1 4a1 1 0 0 0-1 1v2a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1v-2a1 1 0 0 0-1-1h-2z" />
                    </svg>
                    <span>Danh Mục</span></a>
            </li>
            <hr class="sidebar-divider">

            <li class="nav-item active">
                <a class="nav-link" href="<?php echo Yii::app()->baseUrl ?>/baiviet/admin">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-book" viewBox="0 0 16 16">
                        <path
                            d="M1 2.828c.885-.37 2.154-.769 3.388-.893 1.33-.134 2.458.063 3.112.752v9.746c-.935-.53-2.12-.603-3.213-.493-1.18.12-2.37.461-3.287.811V2.828zm7.5-.141c.654-.689 1.782-.886 3.112-.752 1.234.124 2.503.523 3.388.893v9.923c-.918-.35-2.107-.692-3.287-.81-1.094-.111-2.278-.039-3.213.492V2.687zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                    </svg>
                    <span>Bài Viết</span></a>
            </li>
            <hr class="sidebar-divider">

            <li class="nav-item active">
                <a class="nav-link" href="<?php echo Yii::app()->baseUrl ?>/bacsi/admin">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-person-circle" viewBox="0 0 16 16">
                        <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                        <path fill-rule="evenodd"
                            d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                    </svg>
                    <span>Bác Sĩ</span></a>
            </li>
            <hr class="sidebar-divider">

            <li class="nav-item active">
                <a class="nav-link" href="<?php echo Yii::app()->baseUrl ?>/truyenthong/admin">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-cast" viewBox="0 0 16 16">
                        <path
                            d="m7.646 9.354-3.792 3.792a.5.5 0 0 0 .353.854h7.586a.5.5 0 0 0 .354-.854L8.354 9.354a.5.5 0 0 0-.708 0z" />
                        <path
                            d="M11.414 11H14.5a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.5-.5h-13a.5.5 0 0 0-.5.5v7a.5.5 0 0 0 .5.5h3.086l-1 1H1.5A1.5 1.5 0 0 1 0 10.5v-7A1.5 1.5 0 0 1 1.5 2h13A1.5 1.5 0 0 1 16 3.5v7a1.5 1.5 0 0 1-1.5 1.5h-2.086l-1-1z" />
                    </svg>
                    <span>Truyền Thông</span></a>
            </li>
            <hr class="sidebar-divider">

            <li class="nav-item active">
                <a class="nav-link" href="<?php echo Yii::app()->baseUrl ?>/ykienkhachhang/admin">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-chat-dots" viewBox="0 0 16 16">
                        <path
                            d="M5 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                        <path
                            d="m2.165 15.803.02-.004c1.83-.363 2.948-.842 3.468-1.105A9.06 9.06 0 0 0 8 15c4.418 0 8-3.134 8-7s-3.582-7-8-7-8 3.134-8 7c0 1.76.743 3.37 1.97 4.6a10.437 10.437 0 0 1-.524 2.318l-.003.011a10.722 10.722 0 0 1-.244.637c-.079.186.074.394.273.362a21.673 21.673 0 0 0 .693-.125zm.8-3.108a1 1 0 0 0-.287-.801C1.618 10.83 1 9.468 1 8c0-3.192 3.004-6 7-6s7 2.808 7 6c0 3.193-3.004 6-7 6a8.06 8.06 0 0 1-2.088-.272 1 1 0 0 0-.711.074c-.387.196-1.24.57-2.634.893a10.97 10.97 0 0 0 .398-2z" />
                    </svg>
                    <span>Ý Kiến Khách Hàng</span></a>
            </li>
            <hr class="sidebar-divider">

            <li class="nav-item active">
                <a class="nav-link" href="<?php echo Yii::app()->baseUrl ?>/trangtinh/admin">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-activity" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                            d="M6 2a.5.5 0 0 1 .47.33L10 12.036l1.53-4.208A.5.5 0 0 1 12 7.5h3.5a.5.5 0 0 1 0 1h-3.15l-1.88 5.17a.5.5 0 0 1-.94 0L6 3.964 4.47 8.171A.5.5 0 0 1 4 8.5H.5a.5.5 0 0 1 0-1h3.15l1.88-5.17A.5.5 0 0 1 6 2Z" />
                    </svg>
                    <span>Trang Tĩnh</span></a>
            </li>
            <hr class="sidebar-divider">

            <li class="nav-item active">
                <a class="nav-link" href="<?php echo Yii::app()->baseUrl ?>/hoidap/admin">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-question-circle" viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                        <path
                            d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z" />
                    </svg>
                    <span>Hỏi Đáp</span></a>
            </li>
            <hr class="sidebar-divider">

            <li class="nav-item active">
                <a class="nav-link" href="<?php echo Yii::app()->baseUrl ?>/anh/admin">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-image" viewBox="0 0 16 16">
                        <path d="M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                        <path
                            d="M2.002 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2h-12zm12 1a1 1 0 0 1 1 1v6.5l-3.777-1.947a.5.5 0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12V3a1 1 0 0 1 1-1h12z" />
                    </svg>
                    <span>Ảnh</span></a>
            </li>
            <hr class="sidebar-divider">

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-alarm" viewBox="0 0 16 16">
                        <path
                            d="M8.5 5.5a.5.5 0 0 0-1 0v3.362l-1.429 2.38a.5.5 0 1 0 .858.515l1.5-2.5A.5.5 0 0 0 8.5 9V5.5z" />
                        <path
                            d="M6.5 0a.5.5 0 0 0 0 1H7v1.07a7.001 7.001 0 0 0-3.273 12.474l-.602.602a.5.5 0 0 0 .707.708l.746-.746A6.97 6.97 0 0 0 8 16a6.97 6.97 0 0 0 3.422-.892l.746.746a.5.5 0 0 0 .707-.708l-.601-.602A7.001 7.001 0 0 0 9 2.07V1h.5a.5.5 0 0 0 0-1h-3zm1.038 3.018a6.093 6.093 0 0 1 .924 0 6 6 0 1 1-.924 0zM0 3.5c0 .753.333 1.429.86 1.887A8.035 8.035 0 0 1 4.387 1.86 2.5 2.5 0 0 0 0 3.5zM13.5 1c-.753 0-1.429.333-1.887.86a8.035 8.035 0 0 1 3.527 3.527A2.5 2.5 0 0 0 13.5 1z" />
                    </svg>
                    <span>Đặt Lịch</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Đặt Lịch:</h6>
                        <a class="collapse-item" href="<?php echo Yii::app()->baseUrl ?>/lichkham/admin">Đặt Khám</a>
                        <a class="collapse-item" href="<?php echo Yii::app()->baseUrl ?>/SetTimeBS/admin">Set Bác sĩ</a>
                    </div>
                </div>
            </li>
            <hr class="sidebar-divider">

            <li class="nav-item active">
                <a class="nav-link" href="<?php echo Yii::app()->baseUrl ?>/taikhoan/admin">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-person-plus" viewBox="0 0 16 16">
                        <path
                            d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
                        <path fill-rule="evenodd"
                            d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z" />
                    </svg>
                    <span>User</span>
                </a>
            </li>
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Expand
            </div>
            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-arrow-left-right" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                            d="M1 11.5a.5.5 0 0 0 .5.5h11.793l-3.147 3.146a.5.5 0 0 0 .708.708l4-4a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 11H1.5a.5.5 0 0 0-.5.5zm14-7a.5.5 0 0 1-.5.5H2.707l3.147 3.146a.5.5 0 1 1-.708.708l-4-4a.5.5 0 0 1 0-.708l4-4a.5.5 0 1 1 .708.708L2.707 4H14.5a.5.5 0 0 1 .5.5z" />
                    </svg>
                </button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </button>

                    <!-- Topbar Search -->
                    <form
                        class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                        <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                                aria-label="Search" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button" style='z-index:0'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-search" viewBox="0 0 16 16">
                                        <path
                                            d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </form>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <div class="topbar-divider d-none d-sm-block"></div>
                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span
                                    class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $_SESSION['User'] ?></span>
                                <img class="img-profile rounded-circle"
                                    src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYWFRgWFRUZGRgZGBwdHBwcGhkaHBwaGhoaHBwaHBkcIS4lHh4rHxkaJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHhISHjQkISs0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0PTQ0NDQ0MTQ0NP/AABEIAOEA4QMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAEAAIDBQYBB//EAD0QAAEDAQMKBQMCBQQCAwAAAAEAAhEDBCExBRJBUWFxgZGh8AYiscHREzLhQlJygpKy8RQjYqIVwmNz4v/EABoBAAIDAQEAAAAAAAAAAAAAAAECAAMEBQb/xAAkEQADAAICAwACAgMAAAAAAAAAAQIDESExBBJBIlETYTJCcf/aAAwDAQACEQMRAD8AhoWZ7/taTt0c1a2bIc/e7gPlXtOzQp201zr8mq64OpHixPfIHZsnsZ9rQO9aMZTGpSNYnQszbb2y/aXCE1oUgUecuF6gGmySV3OQ7qoUTrUNahPUNzl0vVa+1akLVyhGlMlsnqXDqwQ1a3AaVn7VlhoxKzeU/EYFwM96E8w2I3Mmtt+WmsF7gsTlrxYTLWXnoN6oLVaqlU3zB/SMTvU1gyaCRn4TgI9ytE41PNFNZqfEle6o95lxLt6eypC24ydTLYa0Dy4AibtYVHlDIxAzmggbU6yJ8FVRXZWU64NxUwZqQbqZbiERZ6+jDjgnKiYEi/SNKsKWV3gAi84TfIu6i9DOAMEHG6/Xv1G7ioavlM8/nlKDlPsaaqemGi3PcCDUdLjMyJnUJw1I2w2x1MtLDgIIN8jTPzoVMypcYgwdUTjqU7aswRcRcRq/Cnql0B067Z6TYLU2owOaY9QdXfBGALBZByj9N8OuabjqI1xs/K3tN0iReERRwaiqTblC1FURcgyHM1INUmauZqmyDIST4SQ2QiCWeonPUT6kLmbOsp2TuqKJ1VCVK6r7TbgNKKTY2pRaVLUBpQNoyoG4lZq3ZYvht52KjtNpe8wTtN9wAxJ+FfGB0VXnmTVWrxA0GAUJZ8tNc/zPiFg7baTJAJQEq9YFozvyX8R6RafELQ6AZEYqmtviTGHe6yTKZcYAkq0sdgAguMmOA2960VimRXnqjtW2VKl4lo1lOpWQi9wMnSbzuCsGUwXQNHRE07NJnVv9kW0hUnXYFSs0DUNJjpKKp2ZzsLhGuLtp0DZ0VvZMml8SLhgOl+1WlLJ1+a0Sem9VVZpnFwZ+jYQNN/eAI29VbU7LLC2NGnTyw4K8s+SQMcUY2yAXKt2OsSMLlPJDThpHfos3XsrmOhwu0HfgvU7Zk4OBjuFnrXk1r2vabobdu0cAQQnjLrsrvAu0ZCz1M3RLTjpCKcGubdhpnHHTr18CoX0S1xadx3zf1ngVNQEXdOfstKZjc6BGXAjdwxPsp6RMDTh6gx6c1yoyHXYEjlFya3DfHCOwiKENaIkGfUajv0LaeEMqZ7TSefM0XbR2e4WCLyx0/pOMcidl/RHZNtZY9r2mCDdu27NB2E61GiHrDGouiLkBk+1NqMa9uDhI1jWDtBuVnRbclohyFxPcEoSkGJJ8LihCkqVggq9qAxKqLZlgDAyqx9WpUw8o1m7kss4XR06yzPbLO3ZVDdKpnvfVv+1uvXuCIpWNovPmO32Clqm6FsjAp5ZiyeU3xIAaYYCG46Tp1ATokqkt9pAaWjbxzT+VcWh2N+gOJ1aPlZO3VZOyPYSrXx0Zlt8sFeZSa2Smqak1KOkF2cxcN3yUfTfp0DD5QDMY0Ds97FaWangYxwHe30VdMumdhllp3RgXY78Y4BXtmskADTpOrXB9/hQ5KssmTwOybzvJk8Gq+slDOOHlEcdiz3RrxwkiSx2a4QIGrCfgK2s9nDdCVFiJAVXZa2NDV3MTwuoaF2QPYqjKNnaHB0Yy0xqcPWQFePVdb2ywjdzBn2RGXKMDlyzZrycdO+Wi/nKBpgEmdfUGPYq/y5TDtxaBzH4VCww911xB63+61Q+DFlnVEVb0Bne2D6+qGb33wU9Q506Jk8/8KFl5btmd2IVqM7JqrM4deI0+iAki7aeBB/wrHNOnQYO7D3KDtzb2u1jq2L+IPRMhTe+DMpyAw/qEjeLndRJ/iW8oYLxrwzbSx+OBzgNF8B09D/KvYLBVDmSMCq6WgoIITYUiaUhDiS7CSJNHl1GyNbfEnWURCdCULWkl0VVTrljCoal8936AiCoi3mowIosoPAY86zG2MB0M8CsrXMlX+V3+QnW4Aevss8/EpGWI4xEUvyh2oin3zSMdBlkp5zgFo7BZs5xOgGBvunvYqvJFCQTpMAepPpzWpyZSEAaSbhrnH3vWe6NeKSys1nuDRiYnYIiOnJXdnpACBgOpUNjoXT2Ue1sLOzWOaFKAmNT5USEYl1clOCgBrghKwmO9CLeUJVdF+oH2PsoxpMrlKli06MOcDp3cstapDhoJzp5D3C2WW6fl3tjlestlhkPA/wCZHP8Ayr8bM+dcFazoCOR0qNjfkcCkzTOg+kfKfH28uX+VpMRNUxOo/wDsMeEnkh6jM5h1zI2ax1KntN4EabvaeMlDtdOcOPO+OqiIxuT3jPbqm+NGgr1nwpas5kHEQDq2EbJBG4BeQsdDs4a+ox5rfeFrUGuzZxgjVmmADzjkhSCj0Ial0hRU6ktB7w+FKCqiMjzTrXU9JQB51CRCdCULaUERChrGA46mn0RRCFto8ro/aUGFGNy2/wC1uyeg0c+apSVYZXfL9gAHRV0Ktl0rge1EUQhmo+wMlzRGJS1wh5XJqMmUPK0DTdxIl13EDgtBStDacZwl37RoGon1XMiWPA6A27+YzPRXLLOwfpHqsdUtnQiHoqK2X3jARsj5SoeJXi5wB6eiuXNb+1sbghKmTaT7w1s7PgJfZfob0r9naHiFrri2CrGz29rsCs0+wBpuU9laQ5BpfAzv6apr0/PQVmdcu1q0IEconqVgMSqy25UptmXdlV+ULWcBeqP/AMe+oToBOm/kmmV9YlPXCDcq5aY4QBOOrGCFnsoWwVHZ0RePYKyqZA8pl94MYbR8qoq2UMumZdHOfhXx674M+T21yC2a/P8A5zxGCc/Rv/KZZD92v5OCcwksBHcCArzMPe0FronA8Iw9YQ2dDp2kHRfM396kWwiCdUHgRB9OoQT5gg9x2FEBjSIcRy77wWlyNaIaxwMFocDoux06YLjvAWZeZg8+CuMhVYcMb3N2YnNcJ4o0gI9WyPas9oPMajiT16qxZ3zWX8PVoe6nOERta4SInE6P5QtMy8bvZUsLHZx1LqWcNa6poB5/CULq6thQMIUNWIvw0qYhBZSqQw7vm7vUg+gowVsMv4k8zIQ+biirQ3SOwIjpCZRGjWqtlwMRernItGXtOgObPE/gqqe2CtF4ep+V5jANM6jKXI9SXY1uj0fJ9PyA645aOkIPKdr+nMTOi4noFYWA+Qbk+vQDgblz988nVnoylnp17S+4ljf3OvPBouBWer2+rTLgKrrvtGaCCc6IN/luk4G8RditrZnPpugQ5s4G5w3IW25Io1XF/nYSZIAEE6SM4XcFfLnRmy473tbBMlW99RkvbEENJF7SSJGN43K3pU7wlRszWs+mxoiZJJJJOs7VYUqPlvF6StfCyPbWqCbGyWoS3vhWlhb6Kryi2XQkQW+WioFMuJJQtqyuyldBLp+0QNP6nm5uI2q/rWcinDID3RedAm8jbGCqcsZKZUpta0fTcwECfMHA3uBcL5kTJVspPsrt0l+KM0/K9Sq9zWsiSbhULr26nAwftN4QTa5c452IcJBxBBhXlgySyh53vbLR5WiTebpJIGvVpVPUaPqOIB+4mdclujmrZ9d8Gevb1/IGoeVzxqI/uePdS2a5kf8AIjqT8Ljx5n7Y6ElKzt8h1508IVpm+jWugA7IjZt4eijrsjTII5a+nqnPfHAmeM+wXHtIu4b9XSEUBgY0hGWJ8Hf0MSPhDVMZ71qWym8djX6JgHoWT68GjU05pY8YXAnrBA/mC2lN1/fegrzjINSWNB0Ojf8AUbmx/wBTG9bvJdSWAG8gQTrLXFs9J4qpogfmBcToXECGESKSRWsoGlUmXasMMaT079SrmoblnsuVLwDg1ucfQA8weaV9DSuTPVWXcB6H2AQ7BeTqCItDbm94D8KFpx338AYVZaJlOYO6eZWj8NMnPGgxI2Kisv2m/R8H3U9lyi+zva9kG6C04FJSdLRbjpTSbPVMlTmAHEXKwDVmfCeVzaGOc5oa4PIIG4EHl7rTsKw1LVNM6c0nKaIKtla7EKMZPbt5o9oUjWqSF20B0rKBoUj2IohDvKLFl7ZJZjEqsto86saGlVtvueFET/ZhDWSELVsZ0FF2V0hEZqjYU9GctWT3EEXXqjyjYcxs4uLx7e49VualMLOeJKYDRvnqE0U/bQuVJwzG1mw93eLXKOzvudsk9T8dVNah5p/5N5R8Hqg7O+ZGwjr+VsXRy2OtIhx0X+5j1XQJ4jqP8J2ULyTsHTBMY/l38IgZG9l2oiD7HqmU7rxhIPAmDyMDiiajed+zj6clDESI2gb8RxuPJEBf5DtOYXX4Okcxjuzj2F6HYDmveNbgRxnO6ieK8syc+Cb/ANBn+kweeaeK9OsL87MOmOsSQeLikohbQe5+UkvqDWV1KQxKRSXHYFaygieZ3fKy2VqmcHnW5rf6RPuAtHaX5rCTqJO7GOQjisllF5hgOJlx3udP4SUWSgKsLuA9lAHRO4+6lc6XDZdvnT1lCvf7pByeg6AQdfSG/Ce9udnax8fKhpmRu+Pj1XWVIdJ0zO4/kHmpohpfA1qzajmfuE/zNu6g9F6TQdIXkOTa30q9N8+XOE7j5T0Mr1myOuWTPP5bOh4tbloPYpgoWKUKpFlCchKhRhCrbXamMMOcG36TCjGhh1mpkqtyo3BT0bfAVXbbe0uEuA33IonO22G5OOI1KzhVGSK4e94GDc3mZ9oVsSo+wdkNRZfxPUuHfeC0ld6x/iOqSRs90IX5Et6gzNsd929o5tcfZD0wGvP8R5Td0T7eQOYngPhygq/dpPvIhbl0cyuyeqbnA/pH9sO9gmUyLuI5zf0KYal/Q7jEnnCjs74IHdxm7vSiKFVARBGIHocPQKI6CNEdL/Z3IKRz/KDq+Y+OaGbpGrv1URAljs13HmDeO9q9QyLUJAnRmDRg4Z4POBxXljHSOHoZ+V6F4SrZ1JuM9JYfK31/6oUQ1d2tJNzm6nriQhkUx5m5PKgqvidQ7gLWUFblZ+dDB+o334AX+gPJZfKVaajowHl2XYfKurZXIz3nQDA34eg/rhZuTdrN55mOMydwKSi2eDpMDpxw9kI5153Ii0vi7Vjv7KBnE96kB9hNkdo3fldqiJUdjxO35/KJYM4cPdB9gFZzNxw+RivUfC9v+pSaT9zfK7eNPEQeK8nDocOa0/hXKop1QCYa+GnVP6T1jjsVeWfaS3BfrR6qxynaUDQqIj6ixNG/eycqCtTaTJAlD1rexn3OA3mFW2nxBTBgPBO9DTHiG3wF2nJ4JGa7N4Ajkq5mQmZ5e8l7tZi4amjQpqWWm4m/cUOfEVPOhxA4opvoasNIu7HZ2MGaxoAxu16zrKncVW0cpMd9rweKLFSVGiveiK1uWK8QVruM9f8AC0+Va8NK868Q23PfmNOGJ9Arsc8lGa+NEWVBm36//wAg+iGL79segv6p+Uny0bzymUIDcDrELQjE+yWq6+46+l/ueSjz75Gwpr3998E1rugPfVMgBbKsiMJB54jqExj7xOnv3Q7HJx+D3xPREgUDBjX7rY+C7eINM4h+eOOaCN32ngsQxxMa1aZDtGZVYZgFwBvi49jFClwQ9Y/1XchJU31H/vb/AE/lJJogCT+EDbHgMJO1x3YRxwRb3Xa1QZVteI0YnURfG8T0WhlUoqsp2iTmzic528y719BrVa58EnTgNl0LtWpeSeXX4UNbVp2aPylZakRPdo7vUbx19FNmZov727lz6ZPHBAg+zC7DuR3wRVmbPI84n5UeZF2weoU9I5oHPvogw7A7Qb4jZ6+6dSdMjYI4BQvxnaev4UtmbLu9V6j6JPZ6d4Xyg59FmeZI8s6417YWjY9ZXwjT/wBpzT+72HuFo6Di05ruB1rDT/JnRlPSFbaZIuVG+JvC1TWIe0ZNY/G46xcUjRqw5vTsy1ahTdcWnHGY9OCG/wBHTEwxt+sA9StA/wAP6qzxwZPouMyE0GXPc7YYA5BTX9l9eTOuEV+SbK2ZDG3bAr91QNC4yiGi4QqDxFlhtJpE3p0t8GDLe+WVfifLIaDF5mANZ+FiWOl0kySb96fbbQXuz3ncNQ+VDQMuv77la5n1Rz6r2YfbnXN+NLs4+iBe7yt49YRFtM8C0cYcUG83cTG64eyZCMdnYJs3cT7JNddx79lxpu49+iIGOab1OcENCnpmbkWgDmOuIU7X4d94hCDFT03Xd4oERc/+cr/vPRJVX1Bs6LiGkE1NvyowAhp8xu1QTdJnVqhZy1Vi908sBcLgtTlqzMqsp1swYtzoEZzXEYkb+Cpcq5Dis9lKC1rQTMiCZEa9HqtN4akz48sv+inFMC8uA2k4btqY57W/aJ/5HXu0o85IfOaKZJAugmOE3qFlhdeQw3EiQDcRjOpUuX+i9Un9Bm0HOMnTpOH54IltIA3STrj20KQ0HgZ2bcbpkCdW1RPY/AiNn+JKGg7RE8idx4Tq2n5TKrzMC8998E6pSvBBN2y5T2VsEuAm6BICZTsDYCyyuukQNunbCuLBYLwTpnkMepAUtgsheS518Ya+GoD9xWhs1hhoLoAzmztm4NA0Ae8oXOpYYpey2W3hilmtIIxM9FonUQ4QeB0g61XWCnAG29WzVy6e62dWVqdA4eWXO4HX+VO2qCnOYHCCJCqrVYarL6Ts4ftcb+BRTT7ByizLlFUqBZi1ZYrsudSdyWbyp4mrmQ1uZtN/RWTDfQlX6rk1PiDxAygwyRnHAaSV5jbLW+s8vebvQfMIilZn1HF7yXHWVDa/LMK+JU8fTLkp1/wFcwnAcfgKSztg4z3+UNnTiVPQMBXMpXZLaX3b3E8gAPdDOOA1fKdUdhuCjUA2OCQw4j3TUgbuXuikAeCnUnXjVIlMC6wosgRUbBSa7uAnOaC2ZmNN+zvio0oCXiko0lCG9sHms1Ma3NEb6gA9lJXYC2o/9T6zwNzDmAc2k/zFFMsn0rNZgT5iWHcWj6h/tPJMq0SH0WHEDOdtcfM4/wBRXVa2cxPTJ6diILQTmydI0aV2y0A19SMLjvcJEnyzeI5KR9Qm/Vhen2ctaxz3XkzdoHZ9Ev8AH9G9yntb/q1mU80BrHfUeBhmsIIkmJl2aOay2W25z31BdLp1eURAjhPFX9ief92q4xnw1v8AC2ZP9RdyVdlCnNO7FzgBxIAS/wAS7HWVppFXb7K9rM7NaNom/miLHYM8B03Rhv4K2yzQ/wBpxugDVsOBUGS7L/sscHEEtBIEG44XETgg8ST6GWVtb2W2RrI1rCLi7PuxM3Nv2Aa8b4BvR1Wxuex7m4tALAf1EEEk6pi5MyJTDHlrjnZwBG0jQddxKviAJi+UHj7TIsj4aH2GoHsa9uBEhWDVUZNbmuezROc3c7EcDPMK2Y5cDLDi2mehx2rhUvo8LpTmtSISDbK/KNQNY4nQ0kry+0zUqm7htN54CY5r0vLrf9p/8JPKD8rCZEaC+dpO6CfdW4npNiWttIebLmMzf1YndoHE6Niy2WBAEm91/DEdCFurQyM92skDYGtBJ5yOCxHiF4Lxsu9PZXYnuijNOpKdgvRDfY+ke6hojHd7j881I5aWZUdJv4BRuuT2NPTBce1QAxIJQkmQDrSuhNCcEGQJsj7419xyJHEJPbBjQoGmIO1Fu8wEY4t27O9iBCJJdv1FJQh6plOpn2mnT0MY57thdDG/9XP5JWpuc8E6jHRVvhitnPq1nX574F8+RgIA5lxRttygA/DTsHuur6s5Gyf6biDddgocpgfSzG3EwJ3nFWtOqAwkFhBFwBkjRKz1qfnZ0YAzv2KxLa0DeiG0sb5WM+0AAfwiO+SAbTz6oEeSnedryPKOAJPFqsmDE7EDkx8OqN/+TO4FrQP7UfUHsFW2n/tkGLxhvQmSYfRpYfZmztbd7KwtLC5jtipvC786iW6WPcOfm9SUtf5Dz/iW9Alt7bs28bwtA20h7GuGkctYVE1uO5dyVas0QZLdI1RdIQqdgmtF5SfgQL9fqi2WwRJugXoU1BAzSCDpGkakPb35jL2znA8D8rHk8WMtbaNuLy7xTpPgu7JbmPHlcDs0jgiC5Ypj8zNew+YAk7tS1dktLXtBadGGkcFy/J8V4nxyjq+N5M5Z54f6Icp3scNYPosFkFsVHsOjO4jOn09Fd+I8qVKYLgfKHhrmwLmuJEgjTesZaXvp186ZO39QnCeXJWY/EpT+T7Fry5b1K5WzS2usA10nOgHDUbrjhKwOVKpc+dc9CQD0Wmq2oOZ5ccCDiLyb+iydqvfuAHST1JV/8MwuOTM89ZH+XA2hj3rTn9981ygL0nlDQRA3zri72UrzPDslRuPe73+VOyC29BkB3BcClqs/KZBGhTYBpEFPATs2UzNRbIdace+8U+k7Rtu3prXbEg0aZQAFfUf2UkPJ/eef5SQDs9CyEzMZDcB+PyeKflISAYUuR2eSIxPSSEa+y3HO0Qeq7aOLsGoyxvC6epjULuiaKcDuVLUfnvvuw4DUlVaOWxMhdg5KAojNrE6wByVi8hB1KZzgQj9I2GVvtcBp9IWV8NuLbRVpm6ZMbWn4ctZUbjuHosdbXfStbHjBxE8fK7oQVXXeyyHtNGpm4lC0XZriNt3FFQIx24Iau3zTCYTYbSeQfKSPfgpXWkk+Yccen+UPTJICc8Tfp1oNDJhdoYzMuIcSRhokwQdWKbZnwyRjh1hCtdOmDLfVEUgcwhsXHTv0DE4hK5XOxlT+FL4mqZ1Gq0H7CzO3S107cUPbrKHgEYwCOAVvb7PnNdcPMwNM3i4yN+JQWS4dRE3PbLXDTI8pE6sOCpc74ZdNa5Rnq5IgObfjOBjD2VDVxN+nWtbbxOcYu27hELKVWQefqsuSfU1469hURek51/GVymbye+708CblUXIQCeDCjm7YuOfcNiXRNhDqg0prXcvTmhc5cL1PUm0HMjV0CkIadPe3Wq9rwuh42qaF9g00d2wprWRj3wUDa6kbWnAKaJsn/wBOdTV1QZ38SSGibPVcg/Yz+H3KLtWP9X9rlxJdr6cb4V7PvCkdh3rSSTiAztKY3FJJQhO3E96Asd4q+5n8R9kklXZbj7NO3Abh6Ia0Y8AkkmQoRZvtO4J7cOa6kiwkQ+7i31RVmxH8R/tcuJJK6GkhqfaNzfUKtyXjV/8Atd/6JJJBwW0fa/c3+4rHv+fUpJLJnNmATMOBXW98ikkszNJ04H+FDpJIis4k5JJQAm4hdKSSgGdap2YcR7JJKEHpJJIAP//Z">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-person-fill" viewBox="0 0 16 16">
                                        <path
                                            d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                                    </svg>
                                    Profile
                                </a>
                                <!-- <a class="dropdown-item" href="#">
                                            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Settings
                                        </a> -->
                                <a class="dropdown-item" href="<?php echo Yii::app()->baseUrl?>/" target="_blank">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-house" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                                        <path fill-rule="evenodd"
                                            d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
                                    </svg>
                                    Trang Chính
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item"
                                    href="<?php echo yii::app()->createUrl('taikhoan/admin/Logout') ?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-box-arrow-right" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z" />
                                        <path fill-rule="evenodd"
                                            d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z" />
                                    </svg>
                                    Logout
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div style='padding: 0 30px 0 30px;min-height: 795px'>
                    <?php echo $content; ?>
                </div>
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Your Website 2021</span>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/select2.min.js"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/css/bootstrap.bundle.min.js"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/sb-admin-2.min.js"></script>

</html>
<?php
}
?>