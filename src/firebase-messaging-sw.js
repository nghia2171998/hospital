/* global firebase */
importScripts('https://www.gstatic.com/firebasejs/4.11.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.11.0/firebase-messaging.js');

var config = {
    apiKey: "AIzaSyCS4L8BDlgiPIOqVtkgBhSSGAy2aIFAsFE",
    authDomain: "bookingcareadmin.firebaseapp.com",
    databaseURL: "https://bookingcareadmin.firebaseio.com",
    projectId: "bookingcareadmin",
    storageBucket: "bookingcareadmin.appspot.com",
    messagingSenderId: "522060580065"
};
firebase.initializeApp(config);

const messaging = firebase.messaging();
